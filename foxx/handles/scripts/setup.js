/*jslint node: true */

'use strict';
const db = require('@arangodb').db;
const collectionName = 'names';

if (!db._collection(collectionName)) {
  db._createDocumentCollection(collectionName);
}

