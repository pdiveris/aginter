import json from 'rollup-plugin-json';
import vue from 'rollup-plugin-vue';
import buble from 'rollup-plugin-buble';
import resolve from 'rollup-plugin-node-resolve';
import includePaths from 'rollup-plugin-includepaths';
import sass from 'rollup-plugin-sass';
import scss from 'rollup-plugin-scss';

let includePathOptions = {
  include: {
    'vue': 'node_modules/vue/dist/vue.common.js',
    'vue-router': 'node_modules/vue-router/dist/vue-router.js'
  },
  external: ['vue', 'vue-router']
};

export default {
  format: 'iife',
  entry: 'resources/assets/js/rollapp.js',
  dest: 'bundle.js',
  globals: {
    'vue': 'Vue',
    'vue-router': 'VueRouter'
  },
  plugins: [
    json(),
    vue({compileTemplate: true}),
    scss(),
    includePaths(includePathOptions),
    resolve({
      browser: true,
      main: true
    }),
    buble({
        transforms: { forOf: false }
      }
    )
  ]
};
