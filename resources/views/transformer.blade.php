<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Transformer</title>
  <style>
    html, body {
      height: 100%;
      width: 100%;
      margin: 0;
      padding: 0;
    }

    .instructions {
      display: none;
    }

    .instructions, .events, .output {
      pointer-events: none;
    }

    .clear-events {
      -webkit-appearance: none;
      -moz-appearance: none;
      cursor: pointer;
      display: inline-block;
      font-size: 1rem;
      border: 2px solid #ccc;
      background: white;
      border-radius: 3px;
      outline: none;
      pointer-events: all;
    }

    .droptarget {
      position: absolute;
      right: 20px;
      top: 20px;
      width: 200px;
      height: 200px;
      background: #eee;
    }

    .status, .events {
      font-family: monospace;
      white-space: pre-wrap;
    }

  </style>
</head>
<body>
<div class="container">
  <h3>τρανσφόρμερ</h3>
  <form
    method="post"
    action="http://www.aginter.eu/factory/thumbnail?colorspace=bw&type=jpeg&height=800"
    enctype="multipart/form-data">
    <p>
      <input type="file" name="file"/>
    </p>
    <p>
      <input type="submit" value="Post it(TM)">
    </p>

  </form>
</div>
</body>
</html>
