<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <title>aginter</title>
  <script>
    window.fd = {logging: false};
  </script>
  <script type="text/javascript" src="/js/filedrop.js"></script>


  <link href="https://cdnjs.cloudflare.com/ajax/libs/leaflet-contextmenu/1.4.0/leaflet.contextmenu.min.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="{{mix('css/app.css')}}">

</head>
<body>
<div id="root"></div>
</body>
<script src="{{ mix('js/app.js') }}"></script>

</html>
