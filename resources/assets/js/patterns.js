let instance = null;
function User(name, age) {
  if(instance) {
    return instance;
  }
  instance = this;
  this.name = name;
  this.age = age;

  return instance;
}
const user1 = new User('Peter', 25);
const user2 = new User('Mark', 24);
// prints true
console.log(user1 === user2);
console.log(user1 == user2);
console.log(user1);
console.log(user2);

function Car(name) {
  this.name = name;
  // Default values
  this.color = 'White';
}
// Creating a new Object to decorate
const tesla= new Car('Tesla Model 3');
// Decorating the object with new functionality
tesla.setColor = function(color) {
  this.color = color;
}
tesla.setPrice = function(price) {
  this.price = price;
}
tesla.setColor('black');
tesla.setPrice(49000);
// prints black
console.log(tesla);
