// import Vue from 'vue';

window.Vue = require('vue');

import App from './App.vue';
import router from './router';

import Bulma from 'bulma';
import Buefy from 'buefy';

import 'buefy/dist/buefy.css';
import 'mdi/css/materialdesignicons.css';

import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import axios from 'axios';

axios.interceptors.request.use(function (config) {
    // Do something *before* request is  sent
    NProgress.start();
    return config;
 }, function(error) {
    // Do something with request error
    console.error(error)
    return Promise.reject(error);

});

axios.interceptors.response.use(function (response) {
    // Do something with response data
    NProgress.done();
    return response;

}, function (error) {
    // Do something with response error
    return Promise.reject(error);
});

Vue.config.warnHandler = function(x, msg, vm, trace) {
  return true;
};

Vue.use(Bulma);
Vue.use(Buefy);

const app = new Vue({
    el: '#root',
    template: `<app></app>`,
    components: {App},
    router
});
