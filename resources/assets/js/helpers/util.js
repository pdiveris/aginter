import lodash from 'lodash';

export default {
    notificationActive: false,
    notificationMessage: '',
    notificationType: 'is-success',

    /**
     *
     * @param msg
     * @param notificationType (is-info: blue, is-success: green, is-warning: amber, is-danger: red)
     * @param timeout
     */
    notifikation(msg = '', notificationType = '', timeout) {

        if (!_.startsWith(notificationType, 'is-')) {
            notificationType = 'is-'.concat(notificationType);
        }

        this.notificationMessage = msg;
        this.notificationType = notificationType;
        this.notificationActive = true;

        let me = this;

        setTimeout(function () {
            me.notificationActive = false;
        }, timeout);
    },


};
