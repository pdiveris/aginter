import { Mark } from 'tiptap';
import { wrappingInputRule, wrapIn } from 'tiptap-commands';

export default class StrongMark extends Mark {

    // choose a unique name
    get name() {
        return 'strong';
    }

    // the prosemirror schema object
    // take a look at https://prosemirror.net/docs/guide/#schema for a detailed explanation
    get schema() {
        return {
            content: 'block+',
            group: 'block',
            defining: true,
            draggable: false,
            // define how the editor will detect your node from pasted HTML
            // every blockquote tag will be converted to this blockquote node
            parseDOM: [
              {tag: "strong", preserveWhitespace: "full"},
              // This works around a Google Docs misbehavior where
              // pasted content will be inexplicably wrapped in `<b>`
              // tags with a font-weight normal.
              {tag: "b", getAttrs: node => node.style.fontWeight != "normal" && null, preserveWhitespace: "full"},
              {style: "font-weight", getAttrs: value => /^(bold(er)?|[5-9]\d{2,})$/.test(value) && null},


            ],
            // this is how this node will be rendered
            // in this case a blockquote tag with a class called `awesome-blockquote` will be rendered
            // the '0' stands for its text content inside
            toDOM: () => ['strong', 0],
        };
    }

    // this command will be called from menus to add a blockquote
    // `type` is the prosemirror schema object for this blockquote
    // `schema` is a collection of all registered nodes and marks
    command({ type, schema }) {
        return wrapIn(type);
    }

    // here you can register some shortcuts
    // in this case you can create a blockquote with `ctrl` + `>`
    keys({ type }) {
        return {
            'Ctrl->': wrapIn(type),
        };
    }

    // a blockquote will be created when you are on a new line and type `>` followed by a space
    inputRules({ type }) {
        return [
            wrappingInputRule(/^\s*>\s$/, type),
        ];
    }

}
