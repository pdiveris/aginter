/**
 * Transformer
 *
 * Tags: TextOps, formats, Quill, ProseMirror
 *
 * Author Petros Diveris
 */

import Api from './api';
export default {
  components: {
    Api
  },
  quillDeltaToRenderableFormat(dlt, format) {
    let dt = {
      delta: {
        ops: dlt.ops
      },
      to: format
    };
    console.log('Deep in the quillDeltaToRenderableFormat function');
    console.log(dt);

    let resp = Api.post(
      `/api/transformer/quill/deltas`, dt)
      .then(response => {
        console.log(response);
        return response;
    })
    .catch(e => {
        //  this.errors.push(e)
        console.log(e);
    });
    return resp;
  }

};

