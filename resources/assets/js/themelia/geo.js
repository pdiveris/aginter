/**
 * Geo helper
 * Tags: "geo"
 *
 * Author Petros Diveris
 */

var map;

export default {
  signature: 'Geo',
  /**
   *
   * @param mapVar
   */
  setMap(mapVar) {
    map = mapVar;
  },
  /**
   *
   * @returns {*}
   */
  getMap() {
    return map;
  },
  stravo(url) {
    return 'patshuko';
  },
  /**
   * Get a map centre suggestion either from the data directly (if specified)
   * or best guess based on country etc.. Fall back to Europe if all's empty
   * Read some Camus, drink red wine, smoke..
   *
   * @param data
   * @returns {LatLng|*|M}
   */
  getSuggestedMapCentreFromData(data) {
    if (undefined !== data.event.map_center) {
      let mapCentre = data.event.map_center;

      if ( (mapCentre.latitude_deg !== undefined) && (mapCentre.longitude_deg !== undefined)) {
        return new L.LatLng(mapCentre.latitude_deg, mapCentre.longitude_deg);
      }
    }
    if (data.country.geo !== undefined) {
      let centre = data.country.geo.centre;
      if (centre.latitude != null && centre.longitude != null) {
        return new L.LatLng(centre.latitude, centre.longitude);
      }
    }
    return new L.LatLng(45.6523748, 9.3011539);
  },
  /**
   * Get the rendered map's centre
   * Remove event from function name as it is important  for other resources too..
   *
   * @returns {Array}
   */
  getRenderedEventMarkers() {
    console.log("Get.getRenderedEventMarkers()");
    let ret = [];

    // check whether Bit's actually getting the Europa zentrum for a default value (it's no
    if (typeof map !== 'undefined') {
      map.eachLayer(function (layer) {
        if (layer.options.hasOwnProperty('aginter')) {
          ret.push({latitude_deg: layer._latlng.lat, longitude_deg: layer._latlng.lng});
        }
      });

    }
    return ret;
  },

  getRenderedMapCentre() {
    console.log("Get.getRenderedMapCentre()");
    let ret = {};

    if (typeof map !== 'undefined') {
      let latlong = map.getCenter();
      ret = {latitude_deg: latlong.lat, longitude_deg: latlong.lng, zoom: map.getZoom()};
    }

    return ret;
  },
  /**
   * Return true if aginter type of markers (layers) exist on the map..
   *
   * @returns {boolean}
   */
  gotMarkers() {
    if (typeof map !== 'undefined') {
      map.eachLayer(function (layer) {
        if (layer.options.hasOwnProperty('aginter')) {
          return true;
        }
      });

    }
    return false;
  },
  /**
   * Add a new marker
   * To be implemented
   *
   * @param options
   */
  addNewMarker(options = {}) {

  },
  /**
   * Remove all our markers from the map
   * Typically invoked by the 'Remove all markers' context menu
   *
   * @param e
   */
  removeAllMarkers(e) {
    if (null !== e) {}

    map.eachLayer(function (layer) {
      if (layer.options.hasOwnProperty('aginter')) {
        map.removeLayer(layer);
      }
    });
  },
  /**
   *
   * @param e
   */
  showCoordinates(e) {
    alert(e.latlng);
  },
  /**
   *
   * @param e
   */
  centerMap(e) {
    map.panTo(e.latlng);
  },
  /**
   *
   * @param e
   */
  zoomIn(e) {
    if (null !== e) {}
    map.zoomIn();
  },
  /**
   *
   * @param e
   */
  zoomOut(e) {
    if (null !== e) {}
    map.zoomOut();
  },
  /**
   *
   * @param e
   */
  getMapInfo(e) {
    console.log('Geo.getInfo');
    // info...
  }


};

