/**
 * API helper
 * Tags: "axios wrapper", API, promise, async
 *
 * Author Petros Diveris
 */
import axios from 'axios';
import https from 'axios';

import {convertReplace} from "../helpers/api";

let meta = document.head.querySelector('meta[name="csrf-token"]');
let token = meta.content;
if (!token) {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/*
const agent = new https.Agent({
  rejectUnauthorized: false
});
*/

let api_token = localStorage.getItem('api_token');

export default {
  token: api_token,
  config: {
    headers: {
      'Accept': 'application/x.phobos.v1+json',
      'Authorization': 'Bearer '+api_token,
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-TOKEN': token,
      'Content-Type': 'application/json'
    },
/*
    httpsAgent: agent,
*/
    // `transformResponse` allows changes to the response data to be made before
    // it is passed to then/catch

    transformResponse: [function (data) {
      // Do whatever you want to transform the data
      let ret = JSON.parse(data);

      ret = convertReplace(ret, '␠', ' ');
      console.log("RET", ret);

      return ret;
    }],

    transformRequest: [function (data, headers) {
      if (undefined !== data && data.hasOwnProperty('introduction')) {
        if (undefined !== data.introduction.node ) {
          for (let i=0; i < data.introduction.node.content.length; i++) {
            for (let j=0; j < data.introduction.node.content[i].content.length; j++) {
              if (data.introduction.node.content[i].content[j].hasOwnProperty('text')) {
                data.introduction.node.content[i].content[j].text =
                    data.introduction.node.content[i].content[j].text.replace(/\s/g, '␠');
              }
            }
          }
        }
      }
      return JSON.stringify(data);
    }],
  },
//
  get(url) {
    console.log("Api.GET: " + url);

    let resp = axios.get(url, this.config)
      .then(response => {
        return response;
      })
      .catch(e => {
        //  this.errors.push(e)
        // console.log(e);
      });

    return resp;
  },
  patch(url, data) {
    console.log("Api.PATCH: " + url);
    console.log(this.config);

    let resp = axios.patch(url, data, this.config)
      .then(response => {
        return response;
      })
      .catch(e => {
        console.log("PAKO PAKOP");
        console.log(e);
        return e.response;
      });
    return resp;
  },
  post(url, data) {
    console.log("Api.POST: " + url);

    console.log(data);
    console.log(this.config);

    let resp = axios.post(url, data, this.config)
      .then(response => {
        return response;

      })
      .catch(e => {
        return e.response;
      });
    return resp;
  },
  delete(url) {
    console.log("Api.DELETE: " + url);
    let resp = axios.delete(url, this.config)
      .then(response => {
        return response;
      })
      .catch(e => {
        console.log("DELETE: feck feck");
        return e.response;
      });
    return resp;
  },
  get_token() {
    return this.token;
  }

};

