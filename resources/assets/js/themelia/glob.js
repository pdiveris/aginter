/**
 * Globals
 * Tags:
 *
 * Author Petros Diveris
 */

export default {
  quillDefaults: {
    placeholder: 'Add bubble...',
    readOnly: false,
    theme: '',
    modules: {
      toolbar: [
        ['bold', 'italic'],
        [{'size': ['small', 'large', 'huge']}],
        [{'font': []}],
        [{'align': []}],
        [{ header: [1, 2, 3, 4] }],

      ]
    }
  },
  quillTheme(theme) {
    let ret = this.quillDefaults;
    ret.theme = theme;
    return ret;
  },
  mapDefaults: {

  }

};

