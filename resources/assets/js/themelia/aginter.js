/**
 * Aginter Endpoints helper
 * Tags: "aginter wrapper", API, promise, async
 *
 * Author Petros Diveris
 */
import api from './api';

export default {
  brum(url, data) {
    console.log("Api.PATCH: " + url);
    let resp = axios.patch(url, data, this.config)
      .then(response => {
        return response;
      })
      .catch(e => {
        return e.response;
      });
    return resp;
  },
  mediaUpdateListPositions(data) {   // people, []
    console.log('I am about to PATCH..');

    let endPoint = '/api/media/tiles/listpos?token='+api.token;

    api.patch(endPoint, {edges: data})
      .then(response => {
        console.log(response);
      })
      .catch(e => {

      });
  }
};

