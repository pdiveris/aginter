import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from '../views/Auth/Login.vue';
import Register from '../views/Auth/Register.vue';

import PeopleIndex from '../views/People/Index.vue';
import PeopleEdit from '../views/People/Edit.vue';

import OrganisationIndex from '../views/Organisation/Index.vue';
import OrganisationEdit from '../views/Organisation/Edit.vue';

import EventIndex from '../views/Event/Index.vue';
import EventEdit from '../views/Event/Edit.vue';

import TagIndex from '../views/Tag/Index.vue';
import TagEdit from '../views/Tag/Edit.vue';

import SourceIndex from '../views/Source/Index.vue';
import SourceEdit from '../views/Source/Edit.vue';

import TextumIndex from '../views/Textum/Index.vue';
import TextumEdit from '../views/Textum/Edit.vue';

import LinktypeIndex from '../views/Linktype/Index.vue';
import LinktypeEdit from '../views/Linktype/Edit.vue';

import CountryIndex from '../views/Country/Index.vue';
import CountryEdit from '../views/Country/Edit.vue';

import MediaManager from '../views/Media/Index.vue';

import Test from '../views/Test/Test.vue';
import Dragula from '../views/Dragula/Dragula.vue';
import Quill from '../views/Quill/Quill.vue';
import PM from '../views/PM/PM.vue';
import PDF from '../views/pdf/PDF.vue';

import NotFound from '../views/NotFound.vue';

import MapIndex from '../views/Map/Index.vue';

import Auth from '../store/auth';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/manage/people', component: PeopleIndex, meta: {requiresAuth: true}},
    {path: '/manage/people/:id/edit', component: PeopleEdit, meta: {requiresAuth: true, mode: 'edit'} },
    {path: '/manage/people/add', component: PeopleEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/events', component: EventIndex, meta: {requiresAuth: true}},
    {path: '/manage/events/:id/edit', component: EventEdit, meta: {requiresAuth: true, mode: 'edit' } },
    {path: '/manage/events/add', component: EventEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/organisations', component: OrganisationIndex, meta: {requiresAuth: true } },
    {path: '/manage/organisations/:id/edit', component: OrganisationEdit, meta: {requiresAuth: true, mode: 'edit'}},
    {path: '/manage/organisations/add', component: OrganisationEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/tags', component: TagIndex, meta: {requiresAuth: true}},
    {path: '/manage/tags/:id/edit', component: TagEdit, meta: {requiresAuth: true, mode: 'edit' } },
    {path: '/manage/tags/add', component: TagEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/sources', component: SourceIndex, meta: {requiresAuth: true}},
    {path: '/manage/sources/:id/edit', component: SourceEdit, meta: {requiresAuth: true, mode: 'edit' } },
    {path: '/manage/sources/add', component: SourceEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/linktypes', component: LinktypeIndex, meta: {requiresAuth: true}},
    {path: '/manage/linktypes/:id/edit', component: LinktypeEdit, meta: {requiresAuth: true, mode: 'edit'} },
    {path: '/manage/linktypes/add', component: LinktypeEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/texta', component: TextumIndex, meta: {requiresAuth: false}},
    {path: '/manage/texta/:id/edit', component: TextumEdit, meta: {requiresAuth: true, mode: 'edit'} },
    {path: '/manage/texta/add', component: TextumEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/manage/countries', component: CountryIndex, meta: {requiresAuth: true} },
    {path: '/manage/countries/:id/edit', component: CountryEdit, meta: {requiresAuth: true, mode: 'edit'} },
    {path: '/manage/countries/add', component: CountryEdit, meta: {requiresAuth: true, mode: 'create'} },

    {path: '/mediasink', component: MediaManager, meta: {requiresAuth: true}},
    {path: '/map', component: MapIndex},
    {path: '/quill', component: Quill},
    {path: '/dragula', component: Dragula},
    {path: '/pm', component: PM},
    {path: '/pdf', component: PDF},

    {path: '/register', component: Register},
    {path: '/test', component: Test},
    {path: '/login', component: Login},
    {path: '/not-found', component: NotFound},
    {path: '*', component: NotFound}
  ]
});

export default router;

function auth() {
  let api_token = Auth.state.api_token;
  return api_token != null;
}

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!auth()) {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath,
        },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

