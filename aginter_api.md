FORMAT: 1A

# Aginter Api

# Organisations [/organisations]
Class OrganisationController

## Query Organisations [GET /organisations{?query,page,limit,sort_by}]
Get a JSON representation of all or some actors.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Banda'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Create a new Organisation resource and return the newly created document in JSON [POST /organisations]


## Return a JSON representation of the Organisation resource with the specified ID [GET /organisations{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Organisation resource and return the newly created document in JSON [POST /organisations]


## Update the specified resource in storage. [PUT /organisations{?id}]
Return the updated fields in JSON along with _key, _revision and _id

+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /organisations{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Actors [/actors]
Class ActorController

## Query Actors [GET /actors{?query,page,limit,sort_by}]
Get a JSON representation of all or some actors.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Borghese'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Actor resource with the specified ID [GET /actors{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Actor resource and return the newly created document in JSON [POST /actors]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /actors{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /actors{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Events [/events]
Class EventController

## Query Events [GET /events{?query,page,limit,sort_by}]
Get a JSON representation of all or some events.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'massacre'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Event resource with the specified ID [GET /events{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Event resource and return the newly created document in JSON [POST /events]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /events{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /events{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Tags [/tags]
Class TagController

## Query Tags [GET /tags{?query,page,limit,sort_by}]
Get a JSON representation of all or some actors.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'παπαδοπαίδι'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Tag resource with the specified ID [GET /tags{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Tag resource and return the newly created document in JSON` [POST /tags]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /tags{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /tags{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Countries [/countries]
Class CountryController

## Query Countries [GET /countries{?query,page,limit,sort_by}]
Get a JSON representation of all or some countries.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Greece'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Country resource with the specified ID [GET /countries{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Country resource and return the newly created document in JSON [POST /countries]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /countries{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /countries{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Logs [/logs]
Class LogController

## Query Log entries [GET /logs{?query,page,limit,sort_by}]
Get a JSON representation of all or some Log entries.

+ Parameters
    + query: (string, optional) - A term to search for e.g. alert
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by, and the direction e.g. name.asc.

## Return a JSON representation of the Log resource with the specified ID [GET /logs{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Log resource and return the newly created document in JSON [POST /logs]


## Update the specified resource in storage. [PUT /logs{?id}]
Return the updated fields in JSON along with _key, _revision and _id

+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /logs{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Media [/media]
Class MediaController

## Query Media [GET /media{?query,page,limit,sort_by}]
Get a JSON representation of all or some media.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Borghese'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Media resource with the specified ID [GET /media{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Media resource and return the newly created document in JSON [POST /media]


## Update the specified resource in storage. [PUT /media{?id}]
Return the updated fields in JSON along with _key, _revision and _id

+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /media{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Return some demo data for MediaManager (tiles) in JSON [GET /media/tiles]
