FORMAT: 1A

# aginter

# Organisations [/organisations]
Class OrganisationControllerOrganisation resource representation

## Query Organisations [GET /organisations{?query,page,limit,sort_by}]
Get a JSON representation of all or some organisations.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Banda'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

+ Request (application/json)
    + Headers

            X-Custom: aginter
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "_key": "16461758",
                        "_id": "organisations/16461758",
                        "_rev": "_WzNbSjy--_",
                        "name": "ABBA",
                        "initials": "ABBA",
                        "aliases": "",
                        "profile": "N",
                        "country": "SE",
                        "from": "",
                        "to": "",
                        "category": "",
                        "source": "",
                        "introduction": {
                            "html": "",
                            "node": []
                        },
                        "tags": [],
                        "library": []
                    }
                ],
                "count": 175,
                "totalCount": 175,
                "aql": "FOR o IN organisations FILTER LIKE(o.name, '%%', true) SORT o.name ASC LIMIT 0,5 RETURN o",
                "fields": []
            }

## Return a JSON representation of the Organisation resource with the specified ID [GET /organisations{?id}]


+ Parameters
    + id: (string, required) - The ID of the resource to show..

+ Request (application/json)
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "_key": "16461758",
                "_id": "organisations/16461758",
                "_rev": "_WzNbSjy--_",
                "name": "ABBA",
                "initials": "ABBA",
                "aliases": null,
                "profile": "N",
                "country": [],
                "from": "",
                "to": "",
                "category": "",
                "source": "",
                "introduction": [],
                "media": {
                    "images": [],
                    "videos": [],
                    "texts": [],
                    "sources": []
                },
                "relations": {
                    "actors": []
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Get a suitable structure for editing an existing or adding a new organisation resource
Essentially it presents a new or existing resource with sane default values where they do not exist
and with the associated structures (media, other reosurces) which might be linked to it, also with sane defaults [GET /organisations/edit/{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to get an edit structure for.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            []

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

+ Response 200 (application/json)
    + Body

            {
                "organisation": {
                    "name": "",
                    "initials": "",
                    "aliases": "",
                    "profile": "",
                    "country": "",
                    "from": "",
                    "to": "",
                    "category": "",
                    "source": "",
                    "introduction": {
                        "html": "",
                        "markdown": "",
                        "deltas": "",
                        "node": ""
                    },
                    "tags": [],
                    "library": ""
                },
                "media": {
                    "images": [],
                    "videos": [],
                    "texts": [],
                    "sources": []
                },
                "country": [],
                "relations": {
                    "actors": []
                }
            }

## Add new Organisation document [POST /organisations]
Create a new Organisation resource and return _id, _key and _rev

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "name": "Test test",
                "country": "MV",
                "category": "TER",
                "initials": "TT",
                "source": "Book",
                "tags": "[]",
                "introduction": "{}"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "organisations/17502424",
                "_key": "17502424",
                "_rev": "_W0AAF5m--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

## Update the specified resource in storage. Return _id, _key, _rev and _oldRev in JSON [PUT /organisations/{id}]


+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "name": "17 Νοέμβρη",
                "profile": "L"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "organisations/IT_00001",
                "_key": "IT_00001",
                "_rev": "_W0_3n1O--_",
                "_oldRev": "_W0_3Iyq--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Remove the specified resource from storage. [DELETE /organisations{?id}]


+ Request (application/javascript)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body



+ Response 200 (application/json)
    + Body

            {
                "_id": "organisations/17502424",
                "_key": "17502424",
                "_rev": "_W0AAF5m--_"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

# People [/people]
Class PeopleController

## Query People [GET /people{?query,page,limit,sort_by}]
Get a JSON representation of all or some people.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Borghese'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "_key": "ALFR",
                        "_id": "people/ALFR",
                        "_rev": "_WxcbrDC--b",
                        "name": "Alberto Franceschini",
                        "country": "IT",
                        "orgs": "BR",
                        "tags": [
                            {
                                "_key": "10500807",
                                "tag": "convicted"
                            }
                        ],
                        "introduction": {
                            "html": "",
                            "node": []
                        },
                        "profile": "L",
                        "aliases": "",
                        "title": ""
                    }
                ],
                "count": 133,
                "totalCount": 133,
                "aql": "FOR a IN people FILTER LIKE(a.name, '%%', true) SORT a.name ASC LIMIT 1,1 RETURN a ",
                "fields": []
            }

## Return a JSON representation of the People resource with the specified ID [GET /people{?id}]


+ Parameters
    + id: (string, required) - The ID of the resource to show..

+ Request (application/json)
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "person": {
                    "_key": "ALGI",
                    "_id": "people/ALGI",
                    "_rev": "_WxcbrC2--J",
                    "name": "Αλέξανδρος Γιωτόπουλος",
                    "country": "GR",
                    "orgs": "17N",
                    "aliases": null,
                    "profile": "L",
                    "tags": [],
                    "introduction": {
                        "html": "<p><strong>Alexandros Giotopoulos</strong></p>",
                        "node": []
                    },
                    "title": ""
                },
                "media": {
                    "images": [
                        {
                            "_id": "people_media/13813798",
                            "_key": "13813798",
                            "_rev": "_WzpJf5u--J",
                            "addedBy": "Petros Diveris",
                            "description": {
                                "html": "",
                                "node": {
                                    "content": [],
                                    "type": "doc"
                                }
                            },
                            "file": "0f7b183a5da5e0a90bdd12c9eb1f73e4a244800f.jpg",
                            "license": "",
                            "mime": "image/jpeg",
                            "mountPoint": "people/ALGI",
                            "size": 3637,
                            "source": "http://www.bbc.co.uk/greek/local/030328_17nspecial3.shtml",
                            "_to": "media/13813796",
                            "listPos": 0,
                            "_from": "people/ALGI",
                            "type": "image"
                        }
                    ],
                    "videos": [],
                    "texts": [],
                    "sources": []
                },
                "country": {
                    "_key": "GR",
                    "_id": "countries/GR",
                    "_rev": "_WlS28SC--_",
                    "name": "Greece",
                    "alpha3": "GRC",
                    "countryCode": "300",
                    "iso_3166_2": "ISO 3166-2:GR",
                    "region": "Europe",
                    "subRegion": "Southern Europe",
                    "regionCode": "150",
                    "subRegionCode": "039",
                    "alpha2": "GR",
                    "geo": {
                        "centre": {
                            "latitude": 39.169082,
                            "longitude": 21.897994,
                            "zoom": 6
                        }
                    }
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Get a suitable structure for editing an existing or adding a new People resource [GET /people{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to get an edit structure for.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            []

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

+ Response 200 (application/json)
    + Body

            {
                "person": {
                    "name": "",
                    "title": "",
                    "aliases": "",
                    "profile": "",
                    "country": "",
                    "from": "",
                    "to": "",
                    "category": "",
                    "source": "",
                    "introduction": {
                        "html": "",
                        "markdown": "",
                        "deltas": "",
                        "node": ""
                    },
                    "tags": [],
                    "library": ""
                },
                "media": {
                    "images": [],
                    "videos": [],
                    "texts": [],
                    "sources": []
                },
                "country": [],
                "relations": {
                    "actors": []
                }
            }

## Create document in People collection and return _id, _key and _rev [POST /people]


+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "name": "Petros Diveris",
                "country": "IT",
                "profile": "L",
                "tags": "[]",
                "introduction": "{}"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "people/PEDI",
                "_key": "17502424",
                "_rev": "_W0AAF5m--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /people{?id}]


+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "name": "Petros Diveris",
                "title": "Chief Dev"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "people/PEDI",
                "_key": "PEDI",
                "_rev": "_W0_3n1O--_",
                "_oldRev": "_W0_3Iyq--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Remove the specified resource from storage. [DELETE /people{?id}]


+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Request (application/javascript)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body



+ Response 200 (application/json)
    + Body

            {
                "_id": "people/PEDI",
                "_key": "PEDI",
                "_rev": "_W0AAF5m--_"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

# Events [/events]
Class EventController

## Query Events [GET /events{?query,page,limit,sort_by}]
Get a JSON representation of all or some events.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'massacre'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "_key": "13692140",
                    "_id": "events/13692140",
                    "_rev": "_WzNbnV6--_",
                    "when": "1969-03-30T23:00:00.000Z",
                    "what": "Birth of Petros Diveris",
                    "who": "Toula Sieti, Kostas Diveris",
                    "country": "DE",
                    "location": "Weimar",
                    "tags": [
                        {
                            "_key": "14395610",
                            "tag": "DDR"
                        }
                    ],
                    "eventDate": "",
                    "labels": "",
                    "library": "",
                    "latitude_deg": 50.9712734,
                    "longitude_deg": 11.3291517,
                    "map_center": {
                        "latitude_deg": "50.9712734",
                        "longitude_deg": "11.3291517",
                        "zoom": "13"
                    },
                    "introduction": {
                        "html": "The <strong>most</strong> important event that took place on March 31, 1969 in Weimar, DDR. ",
                        "node": {
                            "content": [
                                {
                                    "type": "paragraph",
                                    "content": [
                                        {
                                            "type": "text",
                                            "text": "The important event that took place on March 31, 1969 in Weimar, DDR. "
                                        }
                                    ]
                                }
                            ],
                            "type": "doc"
                        }
                    },
                    "count": 131,
                    "totalCount": 131,
                    "aql": "FOR e IN events FILTER LIKE(e.what, '%%', true)  SORT e.when ASC LIMIT 0,5 RETURN e",
                    "fields": []
                }
            }

## Return JSON representation of Event resource with the identified by ID [GET /events{?id}]


+ Parameters
    + id: (string, required) - The ID of the resource to show..

+ Request (application/json)
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "event": {
                    "_key": "800802-00009",
                    "_id": "events/800802-00009",
                    "_rev": "_WxoUvVS--H",
                    "when": "1980-08-02T00:00:00.000Z",
                    "what": "Bologna Massacre",
                    "who": "NAR",
                    "labels": "fascist",
                    "country": "IT",
                    "location": "Bologna",
                    "persons": "Francesca Mambro, Valerio Fioravanti",
                    "comments": "85 dead, 200 injured, Terza Posizione",
                    "tags": [],
                    "latitude_deg": 44.5160084,
                    "longitude_deg": 11.2820739,
                    "map_center": {
                        "latitude_deg": "44.5160084",
                        "longitude_deg": "11.2820739",
                        "zoom": "15"
                    },
                    "introduction": {
                        "html": "<p><br></p>",
                        "markdown": "",
                        "node": ""
                    }
                },
                "media": {
                    "images": [
                        {
                            "_id": "events_media/15277095",
                            "_key": "15277095",
                            "_rev": "_WzpKDb6--_",
                            "addedBy": "Petros Diveris",
                            "author": "Beppe Briguglio, Patrizia Pulga, Medardo Pedrini, Marco Vaccari",
                            "copyright": "www.stragi.it",
                            "description": {
                                "deltas": [],
                                "html": "By Beppe Briguglio, Patrizia Pulga, Medardo Pedrini, Marco Vaccari - www.stragi.it/, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=301978",
                                "markdown": "",
                                "node": {
                                    "content": [
                                        {
                                            "type": "paragraph",
                                            "content": [
                                                {
                                                    "type": "text",
                                                    "text": "By Beppe Briguglio, Patrizia Pulga, Medardo Pedrini, Marco Vaccari - www.stragi.it/, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=301978"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "doc"
                                }
                            },
                            "file": "Stragedibologna-2.jpg",
                            "license": "CC BY-SA 3.0",
                            "mime": "image/jpeg",
                            "mountPoint": "events/800802-00009",
                            "size": 32396,
                            "source": "Wikipedia - www.stragi.it",
                            "title": "Rescue teams making their way through the rubble.",
                            "_to": "media/15277093",
                            "listPos": 0,
                            "_from": "events/800802-00009",
                            "type": "image"
                        },
                        {
                            "_id": "events_media/15277418",
                            "_key": "15277418",
                            "_rev": "_WzpKDbu--I",
                            "addedBy": "Petros Diveris",
                            "author": "Zweifel",
                            "copyright": "Zweifel",
                            "description": {
                                "deltas": [],
                                "html": "A picture of the plaque at Bologna Centrale. I took this picture this past summer.",
                                "markdown": "",
                                "node": {
                                    "content": [
                                        {
                                            "type": "paragraph",
                                            "content": [
                                                {
                                                    "type": "text",
                                                    "text": "A picture of the plaque at Bologna Centrale. I took this picture this past summer."
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "doc"
                                }
                            },
                            "file": "Bologna_massacre_memorial.jpg",
                            "license": "Public domain",
                            "mime": "image/jpeg",
                            "mountPoint": "events/800802-00009",
                            "size": 72461,
                            "source": "Wikipedia",
                            "title": "Memorial",
                            "_to": "media/15277416",
                            "listPos": 0,
                            "_from": "events/800802-00009",
                            "type": "image"
                        }
                    ],
                    "videos": [],
                    "texts": [],
                    "sources": []
                },
                "relations": {
                    "people": [],
                    "organisations": [
                        {
                            "_id": "organisations_events/15277374",
                            "_key": "15277374",
                            "_rev": "_Woal4PS--_",
                            "category": "TER",
                            "comments": "Strage di Bologna",
                            "country": "IT",
                            "from": 1977,
                            "initials": "NAR",
                            "introduction": {
                                "html": "<p>The <strong>Nuclei Armati Rivoluzionari..",
                                "node": {
                                    "content": [
                                        {
                                            "type": "paragraph",
                                            "content": [
                                                {
                                                    "type": "text",
                                                    "text": "The Nuclei Armati Rivoluzionari"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "doc"
                                }
                            },
                            "name": "Nuclei Armati Rivoluzionari",
                            "profile": "F",
                            "tags": [],
                            "to": 1981,
                            "victims": "33,85",
                            "_to": "events/800802-00009",
                            "_from": "organisations/IT_00079"
                        }
                    ]
                },
                "country": {
                    "_key": "IT",
                    "_id": "countries/IT",
                    "_rev": "_WlSsK9K--_",
                    "name": "Italy",
                    "alpha3": "ITA",
                    "countryCode": "380",
                    "iso_3166_2": "ISO 3166-2:IT",
                    "region": "Europe",
                    "subRegion": "Southern Europe",
                    "regionCode": "150",
                    "subRegionCode": "039",
                    "alpha2": "IT",
                    "geo": {
                        "centre": {
                            "latitude": 41.71944,
                            "longitude": 13.629067,
                            "zoom": 6
                        }
                    }
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Get a suitable structure for editing an existing or adding a new event resource [GET /events{?id}]


+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            []

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

+ Response 200 (application/json)
    + Body

            {
                "event": {
                    "_key": "13692140",
                    "_id": "events/13692140",
                    "_rev": "_WzNbnV6--_",
                    "when": "1969-03-30T23:00:00.000Z",
                    "what": "Birth of Petros Diveris",
                    "who": "Toula Sieti, Kostas Diveris",
                    "country": "DE",
                    "location": "Weimar",
                    "tags": [
                        {
                            "_key": "14395610",
                            "tag": "DDR"
                        }
                    ],
                    "eventDate": null,
                    "labels": null,
                    "library": null,
                    "latitude_deg": 50.9712734,
                    "longitude_deg": 11.3291517,
                    "map_center": {
                        "latitude_deg": "50.9712734",
                        "longitude_deg": "11.3291517",
                        "zoom": "13"
                    },
                    "introduction": {
                        "html": "The <strong>most</strong> important event that took place on March 31, 1969 in Weimar, DDR. ",
                        "markdown": null,
                        "node": {
                            "content": [
                                {
                                    "type": "paragraph",
                                    "content": [
                                        {
                                            "type": "text",
                                            "text": "The "
                                        },
                                        {
                                            "type": "text",
                                            "marks": [
                                                {
                                                    "type": "strong"
                                                }
                                            ],
                                            "text": "most "
                                        },
                                        {
                                            "type": "text",
                                            "text": "important event that took place on March 31, 1969 in Weimar, DDR. "
                                        }
                                    ]
                                }
                            ],
                            "type": "doc"
                        }
                    }
                },
                "media": {
                    "images": [
                        {
                            "_id": "events_media/13731988"
                        }
                    ],
                    "videos": [],
                    "texts": [],
                    "sources": []
                },
                "relations": {
                    "people": [
                        {
                            "_id": "people_events/14228315",
                            "_to": "events/13692140",
                            "_from": "people/KODI"
                        },
                        {
                            "_id": "people_events/14228290",
                            "_to": "events/13692140",
                            "_from": "people/TUSI"
                        }
                    ],
                    "organisations": [
                        {
                            "_id": "organisations_events/14462667",
                            "_to": "events/13692140",
                            "_from": "organisations/14395628"
                        }
                    ]
                },
                "country": {
                    "_key": "DE",
                    "name": "Germany",
                    "geo": {
                        "centre": {
                            "latitude": 50.048132,
                            "longitude": 10.619596
                        }
                    }
                }
            }

## Create a new Event resource and return _id, _key and _rev [POST /events]


+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "what": "Επίθεση με ρουκέτες κατά της τράπεζας Barclays",
                "country": "GR",
                "when": "1992-04-12"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "events/17617990",
                "_key": "17617990",
                "_rev": "_W0mqZDi--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

## Update the specified resource in storage. Return _id, _key, _rev and _oldRev in JSON [PUT /events{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "what": "Επίθεση στα ματ με πολυβόλο"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "events/17617990",
                "_key": "17617990",
                "_rev": "_W0mtUG--_",
                "_oldRev": "_W0mqZDi--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Remove the specified resource from storage. [DELETE /events{?id}]


+ Request (application/javascript)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body



+ Response 200 (application/json)
    + Body

            {
                "_id": "events/17617990",
                "_key": "17617990",
                "_rev": "_W0mtUG---_"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

# Tags [/tags]
Class TagController

## Query Tags [GET /tags{?query,page,limit,sort_by}]
Get a JSON representation of all or some tags.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'παπαδοπαίδι'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "_key": "17036398",
                        "_id": "tags/17036398",
                        "_rev": "_WxlnePe--_",
                        "tag": "17N",
                        "class": null,
                        "parent": null
                    }
                ],
                "count": 42
            }

## Return a JSON representation of the Tag resource with the specified ID [GET /tags{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

+ Request (application/json)
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "_key": "17036398",
                "_id": "tags/17036398",
                "_rev": "_WxlnePe--_",
                "tag": "17N",
                "class": "",
                "parent": ""
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Get a suitable structure for editing an existing or adding a new organisation resource
Essentially it presents a new or existing resource with sane default values where they do not exist
and with the associated structures (media, other reosurces) which might be linked to it, also with sane defaults [GET /tags/edit/{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to get an edit structure for.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            []

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

+ Response 200 (application/json)
    + Body

            {
                "_key": "17036398",
                "_id": "tags/17036398",
                "_rev": "_WxlnePe--_",
                "tag": "17N",
                "class": "",
                "parent": ""
            }

## Create a new Tag resource and return _id, _key and _rev [POST /tags]


+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "name": "DIGI",
                "class": "State",
                "parent": "IT"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "tags/18037398",
                "_key": "18037398",
                "_rev": "_W0ABF5m--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

## Update the specified Tag resource in the collection.<br/>Return _id, _key, _rev and _oldRev in JSON [PUT /tags/{id}]


+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "tag": "DIGI",
                "class": "Secret Service",
                "parent": "P2"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "tags/18037398",
                "_key": "18037398",
                "_rev": "_W0ABF5n--_",
                "_oldRev": "_W0ABF5m--_"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Remove the specified Tag resource from storage. [DELETE /tags{?id}]


+ Request (application/javascript)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body



+ Response 200 (application/json)
    + Body

            {
                "_id": "tags/18037398",
                "_key": "18037398",
                "_rev": "_W0ABF5m--_"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

# Texta [/texta]
Class TextusController

## Query Texta [GET /texta{?query,page,limit,sort_by}]
Get a JSON representation of all or some actors.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'παπαδοπαίδι'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Textus resource with the specified ID [GET /texta{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Texta resource and return the newly created document in JSON [POST /texta]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /texta{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /texta{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# LinkTypes [/linktypes]
Class Controller

## Query LinkTypes [GET /linktypes{?query,page,limit,sort_by}]
Get a JSON representation of all or some actors.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'παπαδοπαίδι'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the LinkType resource with the specified ID [GET /linktypes{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new LinkType resource and return the newly created document in JSON` [POST /linktypes]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /linktypes{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /linktypes{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Countries [/countries]
Class CountryController

## Query Countries [GET /countries{?query,page,limit,sort_by}]
Get a JSON representation of all or some countries.

+ Parameters
    + query: (string, optional) - A term to search for e.g. 'Greece'
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by , and the direction e.g. name.asc.

## Return a JSON representation of the Country resource with the specified ID [GET /countries{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

+ Response 200 (application/json)
    + Body

            {
                "_id": "AF",
                "name": "Afghanistan",
                "alpha3": "AFG",
                "countryCode": "004",
                "iso_3166_2": "ISO 3166-2:AF",
                "region": "Asia",
                "subRegion": "Southern Asia",
                "regionCode": "142",
                "subRegionCode": "034",
                "alpha2": "AF"
            }

## Get a suitable structure for editing an existing or adding a new country resource [GET /countries{?id}]


+ Response 200 (application/json)
    + Body

            {
                "_id": "AF",
                "name": "Afghanistan",
                "alpha3": "AFG",
                "countryCode": "004",
                "iso_3166_2": "ISO 3166-2:AF",
                "region": "Asia",
                "subRegion": "Southern Asia",
                "regionCode": "142",
                "subRegionCode": "034",
                "alpha2": "AF"
            }

## Create a new Country resource and return the newly created document in JSON [POST /countries]


## Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id [PUT /countries{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /countries{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Logs [/logs]
Class LogController

## Query Log entries [GET /logs{?query,page,limit,sort_by}]
Get a JSON representation of all or some Log entries.

+ Parameters
    + query: (string, optional) - A term to search for e.g. alert
        + Default: 
    + page: (string, optional) - The page of results to view.
        + Default: 1
    + limit: (string, optional) - The amount of results per page.
        + Default: 25
    + sort_by: (string, optional) - The field to sort by, and the direction e.g. name.asc.

## Return a JSON representation of the Log resource with the specified ID [GET /logs{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Create a new Log resource and return the newly created document in JSON [POST /logs]


## Update the specified resource in storage. [PUT /logs{?id}]
Return the updated fields in JSON along with _key, _revision and _id

+ Parameters
    + id: (string, optional) - The ID of the resource to update.

## Remove the specified resource from storage. [DELETE /logs{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# Media [/media]
Class MediaController

## Create a new Media resource and return _id, _key and _rev [POST /media]


+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "title": " Βόλος",
                "author": "Petros Diveris",
                "copyright": "Petros Diveris",
                "file": "DSC_7342-1dla.jpg",
                "mime": "image/jpeg",
                "description": {
                    "html": "",
                    "node": {
                        "type": "doc",
                        "content": [
                            {
                                "type": "paragraph",
                                "content": [
                                    {
                                        "type": "text",
                                        "text": "Βόλος"
                                    }
                                ]
                            }
                        ]
                    }
                },
                "size": 3654583,
                "mountPoint": "people/AAAA",
                "addedBy": "Petros Diveris",
                "source": "Nikon",
                "license": "CC"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "media/17421467",
                "_key": "17421467",
                "_rev": "_WzlHcNS---"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

## Update the specified resource in storage. [PUT /media{?id}]
Return _key, _revision and _id in JSON

+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            {
                "title": " Βόλος",
                "author": "Petros Diveris",
                "copyright": "Petros Diveris",
                "file": "DSC_7342-1dla.jpg",
                "mime": "image/jpeg",
                "description": {
                    "html": "",
                    "node": {
                        "type": "doc",
                        "content": [
                            {
                                "type": "paragraph",
                                "content": [
                                    {
                                        "type": "text",
                                        "text": "Βόλος"
                                    }
                                ]
                            }
                        ]
                    }
                },
                "size": 3654583,
                "mountPoint": "people/AAAA",
                "addedBy": "Petros Diveris",
                "source": "Nikon",
                "license": "CC"
            }

+ Response 200 (application/json)
    + Body

            {
                "_id": "media/17421467",
                "_key": "17421467",
                "_rev": "_WzlHcNS---"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "422 Validation failed"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Not found"
            }

## Update the listPost of the resources posted to reflect the position within the array [PUT /media/tiles/listpos]


+ Parameters
    + token: (string, required) - API auth token

+ Request (application/json)
    + Body

            "edges={}"

+ Request (application/json)
    + Headers

            Authorization: Bearer AbCdEf123456
    + Body

            [
                "people_media/17422118",
                "people_media/17421857",
                "people_media/11530782",
                "people_media/17421469",
                "people_media/17422932",
                "people_media/17422818"
            ]

+ Response 200 (application/json)
    + Body

            {
                "updated": "6 edges"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "404 Edge not found"
            }

+ Response 404 (application/json)
    + Body

            {
                "status_code": "414 Collection not found"
            }

+ Response 422 (application/json)
    + Body

            {
                "status_code": "415 Some other error (to be expanded)"
            }

+ Response 401 (application/json)
    + Body

            {
                "status_code": "401 Unauthorized"
            }

## Perform image imaginaryTransformFit and return a file structure [POST /media/transformer]


## Perform image transformations and return a file structure [POST /media/transformer]


## Upload a file from filedropjs [POST /media/filedrop]


## Remove the specified resource from storage. [DELETE /media{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

## Return some demo data for MediaManager (tiles) in JSON [GET /media/tiles]


# Edges [/edges]
Class EventController

## Query Edges [GET /edges{?query,page,limit,sort_by}]
Get a JSON representation of all or some edges.

## Create a new Edge resource and return the newly created document in JSON [POST /edges]


+ Request (application/json)
    + Body

            "_key=foo&listPos=bar"

+ Response 200 (application/json)
    + Body

            {
                "_key": 10,
                "listPos": "1"
            }

## Update the specified resource in storage. Return _id, _key, _rev and _oldRev in JSON [PUT /edges/{id}]


+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Request (application/json)
    + Body

            "listPos=1"

+ Response 200 (application/json)
    + Body

            {
                "_key": 17045706,
                "_rev": "_WxoiF7G--_",
                "_from": "people/LIGE",
                "_to": "events/17045702",
                "listPos": "1"
            }

## Remove the specified resource from storage. [DELETE /edges{?id}]


+ Parameters
    + id: (string, required) - The ID of the resource to update.

+ Response 200 (application/json)
    + Body

            {
                "response": "Response text"
            }

# Fake [/fakes]
Class FakeControllerUsed as debugging bench. Nothing to see here.

## Faker! [GET /fakes{?id}]


## Faker! [POST /fakes{?id}]


# Fake [/fakes]
Class TransformationsController

## Quill Ops ("Delta") to "renderable" stream transformer [POST /fakes{?id}]
In this instance we are looking at old, limited, very limited, HTML
Stepping stone for some grand ideas which might never be.

# Docs [/docs]
Class PapyrusControllerThis is the Documentation Controller.

My ambition is to have it respond to a a series of endpoints with
 - Detailed description of services available
 - The various data formats expected and returned
 - Anything else in lieu of **live** supporting documentation

## Query Docs [GET /docs{?what}]
Get a JSON representation of all or some actors.

+ Parameters
    + what: (string, optional) - Which set of documents
        + Default: Table of contents

## Return a JSON representation of the Tag resource with the specified ID [GET /docs{?id}]


+ Parameters
    + id: (string, optional) - The ID of the resource to show..

# AppApiV1ControllersMediaImageImaginaryImaginaryController
Class ImaginaryControllerBridge to the Imaginary golang microservice

## Imaginary service API root [GET /]


## Get health status [GET /health]


## Get service info [GET /info]


## Crop an image [GET /crop]
