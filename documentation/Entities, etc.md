##Entities
###Main data
* People
	* people_media
* Organisations
	* organisations_media
* Events
	* events_media

###Functional edges
* Linktypes

###Media

* Images
* Videos
* Sources
* Texta

##Application level
* Users
* Persistence
* Log

##Auxiliary collections
* Countries
* Languages
