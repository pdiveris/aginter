# Atrocities Management System 

### Frameworks and Libraries:

- Laravel 5.4+
- Vue.js 2.2+
- Vue Router
- Axios
- Bulma
- Buefy
- ProseMirror

Please see the [architecture document](https://gitlab.com/pdiveris/aginter/blob/master/docs/architecture.md) for a discussion of the stack and the problems I tried to address with it.
Particularly the discussion on the choice of  Graph and Document database, the problems of current 
interfaces to such databases and the use of ProseMirror  

### Provides:

- Built in token authentication
- Image preview and upload
- CRUD


![Agitation International](s1.png)

### References
*  [ArangoDB](https://docs.arangodb.com/3.3/Manual/index.html)
*  [FOXX extensibility for ArangoDB](https://www.arangodb.com/why-arangodb/foxx/)
*  [Filedrop.js](http://filedropjs.org/)
*  [Leaflet.js maps](http://leafletjs.com/reference-1.3.0.html)
*  [ProseMirror](https://prosemirror.net/docs/)
*  [Bulma CSS](https://bulma.io/documentation/overview/start/)
*  [Buefy](https://buefy.github.io/#/)
*  [Greek stemmer in javascript](https://github.com/Apmats/greekstemmerjs)
*  [Vega History timeline](https://vega.github.io/vega/examples/timelines/)
*  [Frappé Charts ](https://frappe.github.io/charts/)
*  [Best of JS](https://bestof.js.org/)
*  [Laravel Image](https://github.com/Folkloreatelier/laravel-image)

#### Aginter makse use of the imaginary image operation microservice
The [imaginary](https://github.com/h2non/imaginary#usage) server itself in written and implemented in Go 
and it makes use of the superfast and memory efficient [libvips](http://jcupitt.github.io/libvips/) by John Cupitt.
Notes on the use of the service can be found in the [architecture document](docs/architecure.md).

#### Other beautiful suppotring softwares used
[**Lodash.** A modern JavaScript utility library delivering modularity, performance & extras](https://www.lodash.com)

[Pluralize](https://github.com/blakeembrey/pluralize)

### Installation
`git clone https://github.com/codekerala/laravel-and-vue.js-spa-Recipe-Box.git`

`composer install`

`npm install`


### On Youtube

