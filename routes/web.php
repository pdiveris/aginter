<?php

use App\Api\V1\Controllers\TextumController;
use Illuminate\Support\Str;

Route::get('/i', function() {
  dump(app()->version());
  return '';
});

Route::post('register', 'AuthController@register');

Route::get('/finder/{model}/{id}', function($model, $id) {
  $model = '\\App\Api\\V1\\Models\\'.ucfirst(Str::singular($model));
  $concreteModel = $model::find($id);
  var_dump($concreteModel);
  
});

Route::get('/save/{model}/{id}', function($model, $id) {
  $model = '\\App\Api\\V1\\Models\\'.ucfirst(Str::singular($model));
  $concreteModel = $model::find($id);
  $concreteModel->who = 'BR';
  $concreteModel->what = 'Assassination of policemen';

  var_dump($concreteModel);
  $concreteModel->save();
  dd($concreteModel);
  
});

Route::get('/where/{model}', function($model) {
  $model = '\\App\Api\\V1\\Models\\' . ucfirst(Str::singular($model));
  $concreteModel = new $model();
  
  $concreteModel->where();
});


Route::get('/insert/{model}', function($model) {
  $model = '\\App\Api\\V1\\Models\\'.ucfirst(Str::singular($model));

  $concreteModel = new $model();
  
  $concreteModel->who = 'PAKO';
  $concreteModel->what = 'Ice Cream';
  $concreteModel->country = 'GR';
  $concreteModel->when = '2018-12-28T00:00:00.000Z';

  var_dump($concreteModel);
  
  $concreteModel->save();
  
  dd($concreteModel);
  
});

Route::get('/transformer', function () {
  return view('transformer');
});

Route::get('/koufo', '\App\Api\V1\Controllers\TextumController@addTexts');
Route::get('/tokens', '\App\Api\V1\Controllers\TextumController@addTokens');
// Route::get('/tokens', [TextumController::class, 'addTokens']);

Route::get('/{any}', function () {
    return view('welcome');
})->where(['any' => '.*'])->fallback();

