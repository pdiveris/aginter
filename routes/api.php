<?php

use Dingo\Api\Routing\Router;

Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');

$api = app(Router::class);

$api->version('v1', function ($api) {
  $api->get('test', function() {
    return ['v1'];
  });
  $api->get('versions', function() {
    return ['v1', 'v2'];
  });

  $api->get('organisations/edit', 'App\Api\V1\Controllers\OrganisationController@edit');
  $api->get('organisations/edit/{id}', 'App\Api\V1\Controllers\OrganisationController@edit');
  $api->get('organisations', 'App\Api\V1\Controllers\OrganisationController@index');
  $api->get('organisations/{id}', 'App\Api\V1\Controllers\OrganisationController@show');
  $api->post('organisations', 'App\Api\V1\Controllers\OrganisationController@store');
  $api->patch('organisations/{id}', 'App\Api\V1\Controllers\OrganisationController@update');
  $api->delete('organisations/{id}', 'App\Api\V1\Controllers\OrganisationController@destroy');

  $api->get('people/edit/{id}', 'App\Api\V1\Controllers\PeopleController@edit');
  $api->get('people/edit/', 'App\Api\V1\Controllers\PeopleController@edit');
  $api->get('people', 'App\Api\V1\Controllers\PeopleController@index');
  $api->get('people/{id}', 'App\Api\V1\Controllers\PeopleController@show');
  $api->post('people', 'App\Api\V1\Controllers\PeopleController@store');
  $api->patch('people/{id}', 'App\Api\V1\Controllers\PeopleController@update');
  $api->delete('people/{id}', 'App\Api\V1\Controllers\PeopleController@destroy');

  $api->patch('pako', function () {
    // dd(Request::all());
    dump(Request::all());
    return Response::json(['request'=>Request::all()]);
  });
  
  
  $api->get('events/edit', 'App\Api\V1\Controllers\EventController@edit');
  $api->get('events/edit/{id}', 'App\Api\V1\Controllers\EventController@edit');
  $api->get('events', 'App\Api\V1\Controllers\EventController@index');
  $api->get('events/{id}', 'App\Api\V1\Controllers\EventController@show');
  $api->post('events', 'App\Api\V1\Controllers\EventController@store');
  $api->patch('events/{id}', 'App\Api\V1\Controllers\EventController@update');
  $api->delete('events/{id}', 'App\Api\V1\Controllers\EventController@destroy');

  $api->get('tags/edit', 'App\Api\V1\Controllers\TagController@edit');
  $api->get('tags/edit/{id}', 'App\Api\V1\Controllers\TagController@edit');
  $api->get('tags', 'App\Api\V1\Controllers\TagController@index');
  $api->get('tags/{id}', 'App\Api\V1\Controllers\TagController@show');
  $api->post('tags', 'App\Api\V1\Controllers\TagController@store');
  $api->patch('tags/{id}', 'App\Api\V1\Controllers\TagController@update');
  $api->delete('tags/{id}', 'App\Api\V1\Controllers\TagController@destroy');

  $api->get('sources/edit', 'App\Api\V1\Controllers\SourceController@edit');
  $api->get('sources/edit/{id}', 'App\Api\V1\Controllers\SourceController@edit');
  $api->get('sources', 'App\Api\V1\Controllers\SourceController@index');
  $api->get('sources/{id}', 'App\Api\V1\Controllers\SourceController@show');
  $api->post('sources', 'App\Api\V1\Controllers\SourceController@store');
  $api->patch('sources/{id}', 'App\Api\V1\Controllers\SourceController@update');
  $api->delete('sources/{id}', 'App\Api\V1\Controllers\SourceController@destroy');

  $api->get('texta/edit', 'App\Api\V1\Controllers\TextumController@edit');
  $api->get('texta/edit/{id}', 'App\Api\V1\Controllers\TextumController@edit');
  $api->get('texta', 'App\Api\V1\Controllers\TextumController@index');
  $api->get('texta/{id}', 'App\Api\V1\Controllers\TextumController@show');
  
  $api->post('texta', 'App\Api\V1\Controllers\TextumController@store');
  $api->patch('texta/{id}', 'App\Api\V1\Controllers\TextumController@store');
  $api->delete('texta/{id}', 'App\Api\V1\Controllers\TextumController@destroy');

  $api->get('models', 'App\Api\V1\Controllers\ApiController@models' );
  $api->post('mem/push/{id}', 'App\Api\V1\Controllers\ApiController@memPush');
  $api->get('mem/pop/{id}', 'App\Api\V1\Controllers\ApiController@memPop' );
  $api->get('mem/get/{id}', 'App\Api\V1\Controllers\ApiController@memGet' );


  $api->get('linktypes/edit', 'App\Api\V1\Controllers\LinkTypeController@edit');
  $api->get('linktypes/edit/{id}', 'App\Api\V1\Controllers\LinkTypeController@edit');
  $api->get('linktypes', 'App\Api\V1\Controllers\LinkTypeController@index');
  $api->get('linktypes/{id}', 'App\Api\V1\Controllers\LinkTypeController@show');
  $api->post('linktypes', 'App\Api\V1\Controllers\LinkTypeController@store');
  $api->patch('linktypes/{id}', 'App\Api\V1\Controllers\LinkTypeController@update');
  $api->delete('linktypes/{id}', 'App\Api\V1\Controllers\LinkTypeController@destroy');

  $api->get('countries/edit', 'App\Api\V1\Controllers\CountryController@edit');
  $api->get('countries/edit/{id}', 'App\Api\V1\Controllers\CountryController@edit');
  $api->get('countries', 'App\Api\V1\Controllers\CountryController@index');
  $api->get('countries/{id}', 'App\Api\V1\Controllers\CountryController@show');
  $api->post('countries', 'App\Api\V1\Controllers\CountryController@store');
  $api->patch('countries/{id}', 'App\Api\V1\Controllers\CountryController@update');

  $api->get('countries/geo/{operation}/{iso_3166_2}', 'App\Api\V1\Controllers\CountryController@geo');
  
  $api->get('versions/edit', 'App\Api\V1\Controllers\VersionController@edit');
  $api->get('versions/edit/{id}', 'App\Api\V1\Controllers\VersionController@edit');
  $api->get('versions', 'App\Api\V1\Controllers\VersionController@index');
  $api->get('versions/{id}', 'App\Api\V1\Controllers\VersionController@show');
  $api->post('versions', 'App\Api\V1\Controllers\VersionController@store');
  $api->patch('versions/{id}', 'App\Api\V1\Controllers\VersionController@update');
  $api->delete('versions/{id}', 'App\Api\V1\Controllers\VersionController@destroy');
  
  
  $api->get('logs', 'App\Api\V1\Controllers\LogController@index');
  $api->post('logs', 'App\Api\V1\Controllers\LogController@store');
  $api->post('debug', 'App\Api\V1\Controllers\LogController@debug');
  

  $api->get('media', 'App\Api\V1\Controllers\MediaController@index');
  $api->get('media/tiles', 'App\Api\V1\Controllers\MediaController@tiles');

  $api->patch('media/{id}', 'App\Api\V1\Controllers\MediaController@update');
  $api->post('media/cache', 'App\Api\V1\Controllers\MediaController@cache');
  $api->post('media/debug', 'App\Api\V1\Controllers\MediaController@debug');
  $api->post('media/filedrop', 'App\Api\V1\Controllers\MediaController@handleFileDrop');
  
  $api->patch('media/tiles/listpos', 'App\Api\V1\Controllers\MediaController@updateTilesListPositions');

  $api->post('media', 'App\Api\V1\Controllers\MediaController@store');
  $api->delete('media/{id}', 'App\Api\V1\Controllers\MediaController@destroy');

  $api->post('media/operation', 'App\Api\V1\Controllers\MediaController@operation');

  $api->delete('media', 'App\Api\V1\Controllers\MediaController@destroy');

  $api->post('edges', 'App\Api\V1\Controllers\EdgeController@store');
  $api->patch('edges/{id}', 'App\Api\V1\Controllers\EdgeController@update');
  
  $api->patch('edges/people_media/{id}', 'App\Api\V1\Controllers\EdgeController@update');   // <- resolve collection
  $api->patch('edges/organisations_media/{id}', 'App\Api\V1\Controllers\EdgeController@update');   // <- resolve collection
  $api->patch('edges/events_media/{id}', 'App\Api\V1\Controllers\EdgeController@update');   // <- resolve collection

  $api->delete('people_media/{id}', 'App\Api\V1\Controllers\EdgeController@destroy');
  $api->delete('organisations_media/{id}', 'App\Api\V1\Controllers\EdgeController@destroy');
  $api->delete('events_media/{id}', 'App\Api\V1\Controllers\EdgeController@destroy');

  $api->delete('organisations_people/{id}', 'App\Api\V1\Controllers\EdgeController@destroy');
  $api->delete('organisations_events/{id}', 'App\Api\V1\Controllers\EdgeController@destroy');
  $api->delete('people_events/{id}', 'App\Api\V1\Controllers\EdgeController@destroy');


  $api->get('models', 'App\Api\V1\Controllers\ApiController@models' );
  $api->get('models/{model}', 'App\Api\V1\Controllers\ApiController@getModel');

  $api->get('faker', 'App\Api\V1\Controllers\FakeController@getFake');
  $api->post('faker', 'App\Api\V1\Controllers\FakeController@postFake');

  // transformations
  $api->post('transformer/quill/deltas', 'App\Api\V1\Controllers\TransformationsController@postQuillDeltasToRenderable');

  // documentation
  $api->get('docs', 'App\Api\V1\Controllers\PapyrusController@index');
  $api->get('docs/routes/{class}', 'App\Api\V1\Controllers\PapyrusController@getRoutes');
  $api->get('docs/routes/{class}/{version}', 'App\Api\V1\Controllers\PapyrusController@getRoutes');
  
  $api->get('media/image/imaginary', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@index');
  $api->get('media/image/imaginary/info', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@info');
  $api->get('media/image/imaginary/info', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@info');
  $api->post('media/image/imaginary/info', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@info');
  
  $api->get('media/image/imaginary/crop', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@crop');
  $api->post('media/image/imaginary/crop', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@crop');
  
  $api->get('media/image/imaginary/convert', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@convert');
  $api->post('media/image/imaginary/convert', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@convert');
  
  $api->get('media/image/imaginary/smartcrop', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@smartcrop');
  $api->post('media/image/imaginary/smartcrop', 'App\Api\V1\Controllers\Media\Image\Imaginary\ImaginaryController@smartcrop');
  
  
});

$api->version('v2', function ($api) {
 //  $api->get('models', 'App\Api\V2\Controllers\ApiController@models' );

});
