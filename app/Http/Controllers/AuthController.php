<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:api')->only('api/logout');
  }

  public function register(Request $request)
  {
    $this->validate($request, [
      'name' => 'required|max:255',
      'email' => 'required|email|unique:users',
      'password' => 'required|between:6,25|confirmed'
    ]);

    $user = new User($request->all());
    $user->password = bcrypt($request->password);
    $user->save();

    return response()
      ->json([
        'registered' => true
      ]);
  }

  /**
   * POST login handler
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function login(Request $request)
  {

    $this->validate($request, [
      'email' => 'required|email',
      'password' => 'required|between:6,25'
    ]);

    $user = User::where('email', $request->email)
      ->first();

    if ($user && Hash::check($request->password, $user->password)) {
      // generate new api token
      $user->api_token = str_random(60);

      $user->save();

      Auth::login($user, true);

      return response()
        ->json([
          'authenticated' => true,
          'api_token' => $user->api_token,
        ]);

    }
    return response()
      ->json([
        'email' => ['Provided email and password does not match!']
      ], 422);
    }

  /**
   * POST Logout handler
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout(Request $request)
  {
    $user = User::where('api_token', '=', $request->bearerToken())
              ->first();

    $user->api_token = null;
    $user->save();
    
    return response()
      ->json([
        'done' => true
      ]);
  }
}
