<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 23/10/2018
   * Time: 08:38
   */
  
  namespace App\Api\Articulate;
  
  use App\Api\Connection;
  use App\Api\Articulate\Builder as QueryBuilder;
  use ArrayAccess;
  use JsonSerializable;
  use Illuminate\Contracts\Support\Jsonable;
  use Illuminate\Contracts\Support\Arrayable;
  use Illuminate\Database\Eloquent\MassAssignmentException;
  use Illuminate\Database\ConnectionResolverInterface as Resolver;

  abstract class Model implements ArrayAccess, Arrayable, Jsonable, JsonSerializable
  {
    use Concerns\GuardsAttributes;
    
    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [];
  
    /**
     * The model attribute's original state.
     *
     * @var array
     */
    protected $original = [];
  
    /**
     * The changed model attributes.
     *
     * @var array
     */
    protected $changes = [];
    
    /**
     * The connection name for the model.
     *
     * @var Connection
     */
    protected $connection;
  
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $collection;
  
    /**
     * In ArangoDB this takes the form collection/_key
     *
     * @var string
     */
    private $id = '_id';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    private $key = '_key';
  
    /**
     * Revision. In ArangoDB will be something like "_Xn9RD26--_"
     *
     * @var string
     */
    private $rev = '_rev';
    
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    private $keyType = 'string';
  
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;
  
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];
  
    /**
     * The relationship counts that should be eager loaded on every query.
     *
     * @var array
     */
    protected $withCount = [];
  
    /**
     * The connection resolver instance.
     *
     * @var \Illuminate\Database\ConnectionResolverInterface
     */
    protected static $resolver;

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
      // $this->bootIfNotBooted();
    
      // $this->syncOriginal();
    
      $this->fill($attributes);
    }
  
    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
      $totallyGuarded = $this->totallyGuarded();
    
      foreach ($this->fillableFromArray($attributes) as $key => $value) {
        // The developers may choose to place some attributes in the "fillable" array
        // which means only those attributes may be set through mass assignment to
        // the model, and all others will just get ignored for security reasons.
        if ($this->isFillable($key)) {
          $this->setAttribute($key, $value);
        } elseif ($totallyGuarded) {
          throw new MassAssignmentException(sprintf(
            'Add [%s] to fillable property to allow mass assignment on [%s].',
            $key, get_class($this)
          ));
        }
      }
    
      return $this;
    }

  
    /**
     * Set a given attribute on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
      $this->attributes[$key] = $value;
    
      return $this;
    }
    
    /**
     * @param mixed $offset
     * @return bool|void
     */
    public function offsetExists($offset)
    {
    }
  
    /**
     * @param mixed $offset
     * @return mixed|void
     */
    public function offsetGet($offset)
    {
    }
  
    public function offsetSet($offset, $value)
    {
      // TODO: Implement offsetSet() method.
    }
  
    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
      // TODO: Implement offsetUnset() method.
    }
  
    /**
     * @return mixed|void
     */
    public function jsonSerialize()
    {
      // TODO: Implement jsonSerialize() method.
    }
  
    /**
     * @param int $options
     * @return string|void
     */
    public function toJson($options = 0)
    {
      // TODO: Implement toJson() method.
    }
  
    /**
     * @return array|void
     */
    public function toArray()
    {
      // TODO: Implement toArray() method.
    }
  
    /**
     * Create a new instance of the given model.
     *
     * @param  array  $attributes
     * @param  bool  $exists
     * @return static
     */
    public function newInstance($attributes = [], $exists = false)
    {
      // This method just provides a convenient way for us to generate fresh model
      // instances of this current model. It is particularly useful during the
      // hydration of new objects via the Eloquent query builder instances.
      $model = new static((array) $attributes);
    
      $model->exists = $exists;
    
      $model->setConnection(
        $this->getConnectionName()
      );
    
      return $model;
    }
  
    /**
     * Create a new model instance that is existing.
     *
     * @param  array  $attributes
     * @param  string|null  $connection
     * @return static
     */
    public function newFromBuilder($attributes = [], $connection = null)
    {
      $model = $this->newInstance([], true);
    
      $model->setRawAttributes((array) $attributes, true);
    
      $model->setConnection($connection ?: $this->getConnectionName());
    
      // $model->fireModelEvent('retrieved', false);
    
      return $model;
    }
  
    /**
     * Set the array of model attributes. No checking is done.
     *
     * @param  array  $attributes
     * @param  bool  $sync
     * @return $this
     */
    public function setRawAttributes(array $attributes, $sync = false)
    {
      $this->attributes = $attributes;
    
      if ($sync) {
        $this->syncOriginal();
      }
    
      return $this;
    }
  
    /**
     * Sync the original attributes with the current.
     *
     * @return $this
     */
    public function syncOriginal()
    {
      $this->original = $this->attributes;
    
      return $this;
    }
  
    /**
     * Get the database connection for the model.
     *
     * @return \Illuminate\Database\Connection
     */
    public function getConnection()
    {
      return static::resolveConnection($this->getConnectionName());
    }
  
    /**
     * Get the current connection name for the model.
     *
     * @return string
     */
    public function getConnectionName()
    {
      return $this->connection;
    }
  
    /**
     * Set the connection associated with the model.
     *
     * @param  string  $name
     * @return $this
     */
    public function setConnection($name)
    {
      $this->connection = $name;
    
      return $this;
    }
  
    /**
     * Resolve a connection instance.
     *
     * @param  string|null  $connection
     * @return \Illuminate\Database\Connection
     */
    public static function resolveConnection($connection = null)
    {
      return static::$resolver->connection($connection);
    }
  
    /**
     * Get the connection resolver instance.
     *
     * @return \Illuminate\Database\ConnectionResolverInterface
     */
    public static function getConnectionResolver()
    {
      return static::$resolver;
    }
  
    /**
     * Set the connection resolver instance.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface  $resolver
     * @return void
     */
    public static function setConnectionResolver(Resolver $resolver)
    {
      static::$resolver = $resolver;
    }
  
    /**
     * Unset the connection resolver for models.
     *
     * @return void
     */
    public static function unsetConnectionResolver()
    {
      static::$resolver = null;
    }
  
    /**
     * Handle dynamic method calls into the model.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
      // if (in_array($method, ['increment', 'decrement'])) {
      //  return $this->$method(...$parameters);
      // }
      return $this->newQuery()->$method(...$parameters);
    }
  
    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
      return (new static)->$method(...$parameters);
    }
    
    public function newQuery() {
      return $this->newModelQuery()
        ->with($this->with)
        ->withCount($this->withCount);
      
    }
  
    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param \App\Api\Query\Builder
     * @return \App\Api\Articulate\Builder|static
     */
    public function newEloquentBuilder($query)
    {
      return new Builder($query);
    }
  
    /**
     * Get a new query builder instance for the connection.
     *
     * @return \App\Api\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
      $connection = $this->getConnection();
    
      return new QueryBuilder(
        $connection, $connection->getQueryGrammar(), $connection->getPostProcessor()
      );
    }
    
    
    /**
     * Get a new query builder that doesn't have any global scopes or eager loading.
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newModelQuery()
    {
      return $this->newEloquentBuilder(
        $this->newBaseQueryBuilder()
      )->setModel($this);
    }
    
  }
