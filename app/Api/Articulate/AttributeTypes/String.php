<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 24/10/2018
   * Time: 00:40
   */
  
  namespace App\Api\Articulate\AttributeTypes;
  
  
  class AttributeString
  {
    protected $value = '';
    public function __construct($value)
    {
      $this->value = $value;
    }
  }
