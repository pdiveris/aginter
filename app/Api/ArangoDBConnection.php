<?php

namespace App\Api;

use App\Api\Schema\ArangoDBBuilder;
use App\Api\Query\Processors\ArangoDBProcessor;
use App\Api\Query\Grammars\ArangoDBGrammar;

class ArangoDBConnection extends Connection
{
  /**
   * Create a new database connection instance.
   *
   * @param $api
   * @param string $collection
   * @param  string $tablePrefix
   * @param  array $config
   */
  public function __construct($api = '', $collection = '', array $config = [])
  {
    $this->$api = $api;
    
    // First we will setup the default properties. We keep track of the DB
    // name we are connected to since it is needed when some reflective
    // type commands are run such as checking whether a table exists.
    $this->collection = $collection;
    
    $this->config = $config;
    
    // We need to initialize a query grammar and the query post processors
    // which are both very important parts of the database abstractions
    // so we initialize these to their default values while starting.
    $this->useDefaultQueryGrammar();
    
    $this->useDefaultPostProcessor();
  }
  
  /**
   * Set the query grammar to the default implementation.
   *
   * @return void
   */
  public function useDefaultQueryGrammar()
  {
    $this->queryGrammar = $this->getDefaultQueryGrammar();
  }
  
  /**
   * Get the default query grammar instance.
   *
   * @return \App\Api\Query\Grammars\ArangoDBGrammar
   */
  protected function getDefaultQueryGrammar()
  {
    return new  ArangoDBGrammar();
  }
  
  /**
   * Get a schema builder instance for the connection.
   *
   * @return \App\Api\Schema\ArangoDBBuilder
   */
  public function getSchemaBuilder()
  {
    if (is_null($this->schemaGrammar)) {
      $this->useDefaultSchemaGrammar();
    }
    
    return new ArangoDBBuilder($this);
  }
  
  /**
   * Get the default schema grammar instance.
   *
   * @return \App\Api\Query\Grammars\ArangoDBGrammar
   */
  protected function getDefaultSchemaGrammar()
  {
  }
  

  
  /**
   * Get the default post processor instance.
   *
   * @return \App\Api\Query\Processors\ArangoDBProcessor
   */
  protected function getDefaultPostProcessor()
  {
    return new ArangoDBProcessor();
  }
  
  
  /**
   * Bind values to their parameters in the given statement.
   *
   * @param  \PDOStatement $statement
   * @param  array  $bindings
   * @return void
   */
  public function bindValues($statement, $bindings)
  {
    foreach ($bindings as $key => $value) {
      $statement->bindValue(
        is_string($key) ? $key : $key + 1, $value,
        is_int($value) || is_float($value) ? PDO::PARAM_INT : PDO::PARAM_STR
      );
    }
  }

}
