<?php

namespace App\Api\V2\Controllers;

use Illuminate\Http\Request;

/**
 * Class TagController
 *
 * @Resource("Tags", uri="/tags", version="2")
 * @package App\Api\V1\Controllers
 *
 */
class TagController extends ApiController
{
  public function __construct(Request $request)
  {
    parent::__construct($request);
  }


  /**
   * Query Tags
   *
   * Get a JSON representation of all or some actors.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'παπαδοπαίδι'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   *
   * @param Request $request
   * @return array
   */
  public function index(Request $request): array
  {
    if (self::$orderBy == '_id') {
      self::$orderBy = 'tag';
    }

    $direction = (self::$ascending == 1) ? 'ASC' : 'DESC';

    $aql = 'FOR a IN tags FILTER LIKE(a.tag, "%' . self::$query . '%", true) SORT a.' . self::$orderBy . ' ' . $direction . ' LIMIT ' . self::$offset . ',' . self::$limit . ' RETURN a';

    // Create a request with basic Auth
    $response = self::$client->request('POST',
      '_db/piombo/_api/cursor',
      [
        'auth' => self::$auth,
        'json' => [
          'query' => $aql,
          'options' => ['fullCount' => true],
        ]
      ]);

    $body = json_decode($response->getBody());

    $fieldString = $request->input('fields', '');
    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }

        }

        $data[] = $newObject;
      }

    } else {
      $data = $body->result;
    }


    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,

    ];
  }

  /**
   * Get a new blank representation of a tag
   * That is, get an empty model
   *
   * @Versions({"v1","v2"})
   * @return array
   *
   */
  public function create()
  {
    //
    return [];
  }


  /**
   * Return a JSON representation of the Tag resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param  int $id
   * @return array
   */
  public function show($id)
  {
    // Create a request with basic Auth
    $request = self::$client->request('GET',
      '_db/piombo/_api/document/tags/' . $id,
      [
        'auth' => self::$auth
      ]);

    $res = json_decode($request->getBody());

    return (array)$res;

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @Versions({"v1","v2"})
   * @param $id
   * @return array
   */
  public function edit($id)
  {
    //
    return [];
  }

  /**
   * Create a new Tag resource and return the newly created document in JSON`
   *
   * @Post("/")
   * @Versions({"v1","v2"})
   *
   * @param Request $request
   * @param string $collection
   * @return array
   */
  public function store(Request $request, $collection = ''): array
  {
    //
    return parent::store($request);
  }


  /**
   * Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id
   *
   * @Put("/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to update.")
   * })
   *
   * @param Request $request
   * @param $id
   * @return array
   */
  public function update(Request $request, $id): array
  {
    return parent::update($request, $id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @Delete("/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param  int $id
   * @return array
   */
  public function destroy($id): array
  {
    //
    return parent::destroy($id);
  }


}
