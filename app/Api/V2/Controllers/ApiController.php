<?php
namespace App\Api\V2\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

use Illuminate\Http\Request;
use Mockery\Exception;

/**
 * Class ApiController
 *
 * Version 2
 *
 * @package App\Api\V2\Controllers
 */
class ApiController extends Controller
{

  /**
   * Visible to all subclasses and parent
   *
   * @var Client|null
   */
  protected static $client = null;

  /**
   * See .env
   *
   * @var array
   */
  protected static $auth = ['', ''];

  /**
   * @var string
   */
  protected static $query = '';

  /**
   * @var int
   */
  protected static $limit = 25;

  /**
   * orderBy field
   * @var string
   */
  protected static $orderBy = '_id';

  /**
   * ASC or DESC
   *
   * @var int
   */
  protected static $ascending = 1;

  /**
   * Page number
   *
   * @var int
   */
  protected static $page = 1;

  /**
   * Offset calculated from (pageNumber - 1 * limit) + 1...
   * @var int
   */
  protected static $offset = 0;

  /**
   * ?
   * @var int
   */
  protected static $byColumn = 0;

  /**
   * ApiController constructor.
   *
   * Expected parameters from Vue.tables2 are:
   *
   * query
   * hasMore
   * cached
   * extra
   * error
   * code
   * @param Request $request
   */
  public function __construct(Request $request)
  {

    $proto = env('AR_PROTO', 'https');
    $host = env('AR_HOST','');

    self::$auth = [env('AR_USERNAME',''), env('AR_PASSWORD', '')];

    if (null == self::$client) {
      self::$client = new Client(['base_uri' => "$proto://$host"]);
    }

    self::$query = $request->input('q', '');

    self::$page = $request->input('page', 1);

    self::$limit = $request->input('limit', 25);

    if (self::$page > 1) {
      self::$offset = ((self::$page - 1) * self::$limit);
    }

    // sort_by name.asc
    $sortBy = explode('.', $request->input('sort_by', '_id.asc'));

    // TODO: Multiple sorts?
    if (count($sortBy) < 2) {
      // throw an error
      throw new Exception('Bad sort parameters passed', 540);
    }

    self::$ascending = strtolower($sortBy[1] == 'asc');

    self::$orderBy = $sortBy[0];

  }

  /**
   * @return string
   */
  private function getCollection(): string
  {
    $strings = explode('\\', get_class($this));
    $collection = strtolower(str_plural(str_replace('Controller', '', $strings[count($strings) - 1])));

    return $collection;
  }

  /**
   * Create a human readable yet unique key ;o)
   * @param Request $request
   * @param $collection
   * @return string
   */
  public function makeKey(Request $request, $collection): string
  {
    $ret = 'AAAC';
    return $ret;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param string $collection
   * @return array
   */
  public function store(Request $request, $collection = ''): array
  {
    
    // allow for passing collection in order to force edges. just trialing.
    $collection = ($collection == '') ? self::getCollection() : $collection;

    // $key = self::makeKey($request, $collection);

    $body = $request->except(['_key', '_id', '_rev', '_collection']);

    // $body->_key = $key;

    // Create a request with basic Auth

    $response = self::$client->request('POST',
      "_db/piombo/_api/document/{$collection}",
      [
        'auth' => self::$auth,
        'body' => Utils::jsonEncode($body),
      ]);

    $res = json_decode($response->getBody());

    return (array)$res;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param $id
   * @return array
   */
  public function update(Request $request, $id): array
  {

    $collection = self::getCollection();

    $body = $request->except(['_key', '_id', '_rev']);

    // Create a request with basic Auth

    $response = self::$client->request('PATCH',
      "_db/piombo/_api/document/{$collection}/{$id}",
      [
        'auth' => self::$auth,
        'body' => Utils::jsonEncode($body),
      ]);


    $res = json_decode($response->getBody());

    return (array)$res;

  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return array
   */
  public function destroy($id): array
  {
    $collection = self::getCollection();
    $url = "_db/piombo/_api/document/{$collection}/{$id}";

    //
    $response = self::$client->request('DELETE',
      "_db/piombo/_api/document/{$collection}/{$id}",
      [
        'auth' => self::$auth
      ]);

    $res = json_decode($response->getBody());

    return (array)$res;
  }


  /**
   * @param $id
   * @return array
   */
  public function baka($id)
  {

    return [
      'caller' => self::getCollection(),
      'id' => $id,
    ];
  }
  
  /**
   * @param Request $request
   * @return array
   */
  protected function models(Request $request) {
  
    $api = app('Dingo\Api\Routing\Router');
    
    
    $ret['Manifestacja'] = 'Aginter, an atrocity mapper for the 21st century.';
    
    $ret['Version'] = $api->getCurrentRoute()->versions();
  

    
    $ret['Api'] = base_path();
    $ret['Tag'] = [
      'Tag',
    ];
    return $ret;
  }
  
  

}
