<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:15
 */

namespace App\Api\V1\Models;


class AginterModelV2
{
  /**
   * The key, e.g. '10500807'
   * Key is a string that uniquely identifies a document within the collection
   *
   * @see https://docs.arangodb.com/3.0/Manual/Appendix/Glossary.html#document-key Document Key
   * @var string
   */
  public $_key = '';
  
  /**
   * Document handle, e.g. 'tags/10500807'
   * Uniquely identifies a document in the database
   * Consists of the the collection's name and the document key (_key attribute) separated by /
   *
   * @see https://docs.arangodb.com/3.0/Manual/Appendix/Glossary.html#document-handle Document Handle
   * @var string
   */
    public $_id;
  
  /**
   * Revision
   * The MVCC token used to identify a particular revision of a document
   *
   * @see https://docs.arangodb.com/3.0/Manual/Appendix/Glossary.html#document-revision Document Revision
   * @var string
   */
  public $_rev = '';
  
  
}
