<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

class TagV2 extends AginterModel
{
  /**
   * The tag, .e. Aldo Moro
   *
   * @var string
   */
  public $tag = '';
  
  /**
   * Class, e.g. 'politician'
   *
   * @var string
   */
  public $class = '';
  
  /**
   * A list of parents, if any
   *
   * @var array
   */
  public $parents = [];
  
  
}
