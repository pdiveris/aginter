<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 17/10/2018
   * Time: 08:15
   */
  
  namespace App\Api\Query;

  use Closure;
  use Illuminate\Database\Query\Expression;
  use Illuminate\Support\Arr;
  use Illuminate\Support\Str;
  use InvalidArgumentException;
  use Illuminate\Support\Traits\Macroable;
  use Predis\Protocol\Text\Handler\BulkResponse;
  
  use  \Illuminate\Database\ConnectionInterface;
  use App\Api\Connection;
  use App\Api\Query\Grammars\ArangoDBGrammar as Grammar;
  use App\Api\Query\Processors\ArangoDBProcessor as Processor;
  
  class Builder
  {
    use Macroable {
      __call as macroCall;
    }
    /**
     * The database connection instance.
     *
     * @var \App\Api\Connection
     */
    protected $connection;
    
    /**
     * The database query grammar instance.
     *
     * @var \App\Api\Query\Grammars\ArangoDBGrammar
     */
    protected $grammar;
    
    /**
     * The database query post processor instance.
     *
     * @var \Illuminate\Database\Query\Processors\Processor
     */
    protected $processor;
    
    /**
     * The current query value bindings.
     *
     * @var array
     */
    protected $bindings = [
      'select' => [],
      'join'   => [],
      'where'  => [],
      'filter' => [],
      'having' => [],
      'order'  => [],
      'union'  => [],
    ];
    
    /**
     * An aggregate function and column to be run.
     *
     * @var array
     */
    public $aggregate;
  
  
    /**
     * @var
     */
    public $for = 'FOR';
    
    /**
     * The columns that should be returned.
     *
     * @var array
     */
    public $columns;
    
    /**
     * Indicates if the query returns distinct results.
     *
     * @var bool
     */
    public $distinct = false;
    
    /**
     * The table which the query is targeting.
     *
     * @var string
     */
    public $from;
  
    /**
     * The first letter of the collection to be use like: "SORT e.what DESC"
     * @var string
     */
    public $fromAlias;
    
    /**
     * The table joins for the query.
     *
     * @var array
     */
    public $joins;
    
    /**
     * The where constraints for the query.
     *
     * @var array
     */
    public $wheres = [];
    
    /**
     * The groupings for the query.
     *
     * @var array
     */
    public $groups;
    
    /**
     * The having constraints for the query.
     *
     * @var array
     */
    public $havings;
    
    /**
     * The orderings for the query.
     *
     * @var array
     */
    public $orders;
    
    /**
     * The maximum number of records to return.
     *
     * @var int
     */
    public $limit;
    
    /**
     * The number of records to skip.
     *
     * @var int
     */
    public $offset;
    
    /**
     * The query union statements.
     *
     * @var array
     */
    public $unions;
    
    /**
     * The maximum number of union records to return.
     *
     * @var int
     */
    public $unionLimit;
    
    /**
     * The number of union records to skip.
     *
     * @var int
     */
    public $unionOffset;
    
    /**
     * The orderings for the union query.
     *
     * @var array
     */
    public $unionOrders;
    
    /**
     * Indicates whether row locking is being used.
     *
     * @var string|bool
     */
    public $lock;
    
    /**
     * The field backups currently in use.
     *
     * @var array
     */
    protected $backups = [];
    
    /**
     * The binding backups currently in use.
     *
     * @var array
     */
    protected $bindingBackups = [];
    
    /**
     * All of the available clause operators.
     *
     * @var array
     */
    protected $operators = [
      '=', '<', '>', '<=', '>=', '<>', '!=',
      'like', 'like binary', 'not like', 'between', 'ilike',
      '&', '|', '^', '<<', '>>', 'in',
      'rlike', 'regexp', 'not regexp',
      '~', '~*', '!~', '!~*', 'similar to',
      'not similar to', 'not ilike', '~~*', '!~~*',
    ];
  
    /**
     * The components that make up a select clause.
     *
     * @var array
     */
    protected $selectComponents = [
      'aggregate',
      'columns',
      'from',
      'joins',
      'wheres',
      'groups',
      'havings',
      'orders',
      'limit',
      'offset',
      'unions',
      'lock',
    ];
  
  
  
    /**
     * Create a new query builder instance.
     *
     * @param  \Illuminate\Database\ConnectionInterface  $connection
     * @param  Grammars\ArangoDBGrammar $grammar
     * @param  Processors\ArangoDBProcessor $processor
     * @return void
     */
    public function __construct(ConnectionInterface $connection,
                                Grammar $grammar = null,
                                Processor $processor = null)
    {
      $this->connection = $connection;
      $this->grammar = $grammar ?: $connection->getQueryGrammar();
      $this->processor = $processor ?: $connection->getPostProcessor();
    }
/*
    public function __construct()
    {
      // Set our default grammar
      $this->grammar = new Grammars\Grammar();
    }
  */
    /**
     * Set the table which the query is targeting.
     *
     * @param $table
     * @param string $alias
     * @return $this
     */
    public function from($table, $alias = '')
    {
      if ($alias == '') {
        $alias = substr($table,0,1);
      }
      $this->from = $table;
      $this->fromAlias = $alias;
      
      return $this;
    }
  
    public function orders($column, $direction = 'ASC')
    {
 
      $this->orders[] = ['column'=>$column,'direction'=>$direction];
      
    }
    
    /**
     * Get the current query value bindings in a flattened array.
     *
     * @return array
     */
    public function getBindings()
    {
      return Arr::flatten($this->bindings);
    }
    /**
     * Get the raw array of bindings.
     *
     * @return array
     */
    public function getRawBindings()
    {
      return $this->bindings;
    }
  

    /**
     * Compile the components necessary for a select clause.
     *
     * @param Builder $query
     * @return array
     */
    protected function compileComponents(Builder $query)
    {
      $sql = [];
    
      foreach ($this->selectComponents as $component) {
        // To compile the query, we'll spin through each component of the query and
        // see if that component exists. If it does we'll just call the compiler
        // function for the component which is responsible for making the SQL.
        if (isset($query->$component) && ! is_null($query->$component)) {
          $method = 'compile'.ucfirst($component);
        
          $sql[$component] = $this->grammar->$method($query, $query->$component);
        }
      }
      return $sql;
    }

    
    /**
     * Add a basic where clause to the query.
     *
     * @param  string|array|\Closure  $column
     * @param  string  $operator
     * @param  mixed   $value
     * @param  string  $boolean
     * @return $this
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
      
      $operator = strtoupper($operator);
      
      // If the columns is actually a Closure instance, we will assume the developer
      // wants to begin a nested where statement which is wrapped in parenthesis.
      // We'll add that Closure to the query then return back out immediately.
      if ($column instanceof Closure) {
        return $this->whereNested($column, $boolean);
        
      }
      
      // If the value is "null", we will just assume the developer wants to add a
      // where null clause to the query. So, we will allow a short-cut here to
      // that method for convenience so the developer doesn't have to check.
      if (is_null($value)) {
        return $this->whereNull($column, $boolean, $operator != '=');
      }
  
      // If the given operator is not found in the list of valid operators we will
      // assume that the developer is just short-cutting the '=' operators and
      // we will set the operators to '=' and set the values appropriately.
      if (! in_array(strtolower($operator), $this->operators, true) &&
        ! in_array(strtolower($operator), $this->grammar->getOperators(), true)) {
        list($value, $operator) = [$operator, '='];
      }
      
      
      // Now that we are working with just a simple query we can put the elements
      // in our array and add the query binding to our array of bindings that
      // will be bound to each SQL statements when it is finally executed.
      $type = 'Basic';
      if (Str::contains($column, '->') && is_bool($value)) {
        $value = new Expression($value ? 'true' : 'false');
      }
      
      if (null != $this->fromAlias) {
        $column = $this->fromAlias.'.'.$column;
      }
      $this->wheres[] = compact('type', 'column', 'operator', 'value', 'boolean');
      
      if (! $value instanceof Expression) {
        $this->addBinding($value, 'where');
      }
      return $this;
    }
    
    public function getNested(Builder $query)
    {
      
      foreach ($query->wheres as $i=>$where) {
        if ($where['type'] == 'Nested') {
          return $where['query'];
        }
      }
      return null;
    }
  
    /**
     * @param string $column
     * @param Builder $filter
     * @param string $parentAlias
     * @return $this
     */
    public function whereTest(string $column, Builder $filter, string $parentAlias='')
    {
      $type = 'Basic';
      $boolean = 'OR';
      $operator = 'IN';
  
      $aql = "( FOR $filter->fromAlias IN {$filter->from} ";
  
      $builder = $this->getNested($filter);
      
      if (null !== $builder) {
        $aql .= "FILTER $filter->fromAlias._from IN
                    (
                      FOR $builder->fromAlias IN $builder->from
                        FILTER LIKE($builder->fromAlias.name, \"%x%\", true) OR  LIKE($builder->fromAlias.initials, \"%c%\", true)
                        RETURN o._id
                    )
                    RETURN $filter->fromAlias._to
                  )";
        
      }
      $aql .= "";
    
      $value = new Expression($aql);
      
      if ($parentAlias!=='')
        $column = $parentAlias.'.'.$column;
      
      $this->wheres[] = compact('type', 'column', 'operator', 'value', 'boolean');
      
      return $this;
    }
    
    /**
     * Add an "or where" clause to the query.
     *
     * @param  \Closure|string  $column
     * @param  string  $operator
     * @param  mixed   $value
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function orWhere($column, $operator = null, $value = null)
    {
      return $this->where($column, $operator, $value, 'OR');
    }
    
    /**
     * Add a binding to the query.
     *
     * @param  mixed   $value
     * @param  string  $type
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function addBinding($value, $type = 'where')
    {
      if (! array_key_exists($type, $this->bindings)) {
        throw new InvalidArgumentException("Invalid binding type: {$type}.");
      }
      if (is_array($value)) {
        $this->bindings[$type] = array_values(array_merge($this->bindings[$type], $value));
      } else {
        $this->bindings[$type][] = $value;
      }
      return $this;
    }
    
    /**
     * Add a "where null" clause to the query.
     *
     * @param  string  $column
     * @param  string  $boolean
     * @param  bool    $not
     * @return $this
     */
    public function whereNull($column, $boolean = 'and', $not = false)
    {
      $type = $not ? 'NotNull' : 'Null';
      $this->wheres[] = compact('type', 'column', 'boolean');
      return $this;
    }
    
    /**
     * Compile a "where in" clause.
     *
     * @param  array $where
     * @return string
     */
    protected function whereIn($where)
    {
      if (! empty($where['values'])) {
        return $this->wrap($where['column']).' in ('.$this->parameterize($where['values']).')';
      }
    
      return '0 = 1';
    }
    
    /**
     * Compile a where in sub-select clause.
     *
     * @param  Builder $query
     * @param  array $where
     * @return string
     */
    public function whereInSub(Builder $query, $where)
    {
      $select = $query->grammar->compileSelect($query);
      
      return $query->grammar->wrap($where['column']) . ' in (' . $select . ')';
    }
  
  
    /**
     * Set the "limit" value of the query.
     *
     * @param  int  $value
     * @return $this
     */
    public function limit($value)
    {
      $property = $this->unions ? 'unionLimit' : 'limit';
      if ($value >= 0) {
        $this->$property = $value;
      }
      return $this;
    }
  
    /**
     * Add a full sub-select to the query.
     *
     * @param  string   $column
     * @param  string   $operator
     * @param  \Closure $callback
     * @param  string   $boolean
     * @return $this
     */
    protected function whereSub($column, $operator, Closure $callback, $boolean)
    {
      $type = 'Sub';
      
      $query = $this->newQuery();
      
      // Once we have the query instance we can simply execute it so it can add all
      // of the sub-select's conditions to itself, and then we can cache it off
      // in the array of where clauses for the "main" parent query instance.
      call_user_func($callback, $query);
      
      $this->wheres[] = compact('type', 'column', 'operator', 'query', 'boolean');
      
      $this->addBinding($query->getBindings(), 'where');
      
      return $this;
    }
  
    /**
     * Add a nested where statement to the query.
     *
     * @param  \Closure $callback
     * @param  string   $boolean
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function whereNested(Closure $callback, $boolean = 'and')
    {
      $query = $this->forNestedWhere();
      call_user_func($callback, $query);
      return $this->addNestedWhereQuery($query, $boolean);
    }
  
    /**
     * Create a new query instance for nested where condition.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function forNestedWhere()
    {
      $query = $this->newQuery();
      return $query->from($this->from);
    }
    /**
     * Add another query builder as a nested where to the query builder.
     *
     * @param  \Illuminate\Database\Query\Builder|static $query
     * @param  string  $boolean
     * @return $this
     */
    public function addNestedWhereQuery($query, $boolean = 'and')
    {
      if (!null == $this->wheres && count($this->wheres)) {
        $type = 'Nested';
        $this->wheres[] = compact('type', 'query', 'boolean');
        $this->addBinding($this->getBindings(), 'where');
      } else {
      
      }
      
      return $this;
    }
    
    /**
     * Get a new instance of the query builder.
     *
     * @return static
     */
  
    public function newQuery()
    {
      return new static($this->connection, $this->grammar, $this->processor);
    }
  
    /**
     * Get the SQL representation of the query.
     *
     * @return string
     */
    public function toSql()
    {
      return $this->grammar->compileSelect($this);
    }
    
    
    /**
     * Run the query as a "select" statement against the connection.
     *
     * @return array
     */
    protected function runSelect()
    {
      // dump($this->toSql());
      // return $this->connection->select($this->toSql(), $this->getBindings(), ! $this->useWritePdo);
    }
  
    /**
     * Execute the query as a "select" statement.
     *
     * @param  array  $columns
     * @return \Illuminate\Support\Collection
     */
    public function get($columns = null)
    {
      $original = $this->columns;
      if (is_null($original)) {
        $this->columns = $columns;
      }
      
      $this->runSelect();
      //$results = $this->processor->processSelect($this, $this->runSelect());
      //$this->columns = $original;
      //return collect($results);
    }
    
  }
