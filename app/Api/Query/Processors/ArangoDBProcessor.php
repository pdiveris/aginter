<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 20/10/2018
   * Time: 12:23
   */
  
  namespace App\Api\Query\Processors;
  
  use App\Api\Query\Builder;
  use App\Api\Query\Processors\Processor;
  
  class ArangoDBProcessor extends Processor
  {
    /**
     * Process an "insert get ID" query.
     *
     * @param  \App\Api\Query\Builder $query
     * @param  string $sql
     * @param  array $values
     * @param  string $sequence
     * @return string
     */
    public function processInsertGetId(Builder $query, $sql, $values, $sequence = null)
    {
      /*
      $result = $query->getConnection()->selectFromWriteConnection($sql, $values)[0];
      
      $sequence = $sequence ?: 'id';
      
      $id = is_object($result) ? $result->{$sequence} : $result[$sequence];
      
      return is_numeric($id) ? (int)$id : $id;*/
      return md5(rand(123,2938574));
    }
    
    /**
     * Process the results of a column listing query.
     *
     * @param  array $results
     * @return array
     */
    public function processColumnListing($results)
    {
/*      return array_map(function ($result) {
        return ((object)$result)->column_name;
      }, $results);*/
      return [];
    }
  }
