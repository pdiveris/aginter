<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

/**
 * Class LogController
 *
 * @Resource("Logs", uri="/logs")
 * @package App\Api\V1\Controllers
 *
 */
class LogController extends ApiController
{
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['edit', 'store', 'update', 'destroy', 'debug']]);
  }
  
  
  /**
   * Query Log entries
   *
   * Get a JSON representation of all or some Log entries.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. alert", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by, and the direction e.g. name.asc.")
   * })
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {
    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';

    $aql = 'FOR l IN logs FILTER LIKE(l.evt, "%' . self::$query . '%", true) SORT l.' . self::$orderBy . ' ' . $direction . ' LIMIT ' . self::$offset . ',' . self::$limit . ' RETURN l';

    // Create a request with basic Auth
    $response = self::$client->request(
        'POST',
        '_db/piombo/_api/cursor',
        [
          'auth' => self::$auth,
          'json' => [
            'query' => $aql,
            'options' => ['fullCount' => true],
          ]
        ]
    );

    $body = json_decode($response->getBody());

    $fieldString = $request->input('fields', '');

    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }
        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }

    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,

    ];

  }

  /**
   * Return a JSON representation of the Log resource with the specified ID
   *
   * @GET("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param  int $id
   * @return array
   *
   */
  public function show($id): array
  {

    $res = [
      'Log' => 'show',
      'id' => $id,
    ];

    return (array)$res;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return array
   *
   */
  public function edit($id): array
  {
    //
    $res = [
      'Log' => 'edit',
      'id' => $id,
    ];

    return (array)$res;
  }
  
  /**
   * Create a new Log resource and return the newly created document in JSON
   *
   * @Post("/")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    return parent::store($request);
  }


  /**
   * Update the specified resource in storage.
   * Return the updated fields in JSON along with _key, _revision and _id
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to update.")
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    if (1==1);
  }


  /**
   * @param Request $request
   * @return array|string
   */
  public function debug(Request $request)
  {

    return $request->input();
  }

    /**
     * Remove the specified resource from storage.
     *
     * @Delete("/{?id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="The ID of the resource to show..")
     * })
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse  {
    //
    return parent::destroy($request, $id);
  }
}
