<?php
  
  namespace App\Api\V1\Controllers;
  
  use Illuminate\Http\Request;

  /**
   * Class EventController
   *
   * @Resource("Edges", uri="/edges")
   * @package App\Api\V1\Controllers
   */
  class EdgeController extends ApiController
  {
    /**
     * EdgeController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
      parent::__construct($request);
      
      $this->middleware('api.auth', ['only' => ['edit', 'store', 'update', 'destroy']]);
    }
    
    /**
     * Query Edges
     *
     * Get a JSON representation of all or some edges.
     *
     * @Get("/{?query,page,limit,sort_by}")
     *
     * @param Request $request
     * @return array
     *
     */
    public function index(Request $request): array
    {
      return [];
    }
    
    
    /**
     * Create a new Edge resource and return the newly created document in JSON
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request("_key=foo&listPos=bar", contentType="application/json")
     * @Response(200, body={"_key": 10, "listPos": "1"})
     *
     * @param Request $request
     * @param string $collection
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
    {
      
      $collection = $request->input('_collection');
      // unset($request->input('_collection'));
      return parent::store($request, $collection);
    }
    
    
    /**
     * Update the specified resource in storage. Return _id, _key, _rev and _oldRev in JSON
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
     * })
     * @Request("listPos=1", contentType="application/json")
     * @Response(200, body={"_key": 17045706, "_rev": "_WxoiF7G--_", "_from": "people/LIGE", "_to": "events/17045702", "listPos": "1"})
     *
     * @param Request $request
     * @param string $id
     * @param string $collection
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     */
    public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
    {
      
      if ($request->input('_collection', '') !== '') {
        $collection = $request->input('_collection');
      }
      
      return parent::update($request, $id, $collection);
    }

      /**
       * Remove the specified resource from storage.
       *
       * @Delete("/{?id}")
       * @Versions({"v1"})
       * @Parameters({
       *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
       * })
       * @Response(200, body={"response": "Response text"})
       *
       * @param Request $request
       * @param int $id
       * @return \Illuminate\Http\JsonResponse
       * @throws \GuzzleHttp\Exception\GuzzleException
       */
    public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
      
      $collection = $request->segment(2);
      
      $url = "_db/piombo/_api/document/{$collection}/{$id}";
      
      //
      $response = self::$client->request(
          'DELETE',
          $url,
          [
            'auth' => self::$auth
          ]
      );
      
      $res = json_decode($response->getBody());
      
      return response()->json(
          $res,
          $response->getStatusCode(),
          [
            'X-Status' => 'Destroy successful',
            'User' => $request->server('user')
          ]
      );
    }
  }
