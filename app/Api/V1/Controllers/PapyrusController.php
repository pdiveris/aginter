<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;


/**
 * Class PapyrusController
 *
 * This is the Documentation Controller.
 *
 * My ambition is to have it respond to a a series of endpoints with
 *  - Detailed description of services available
 *  - The various data formats expected and returned
 *  - Anything else in lieu of **live** supporting documentation
 *
 * @Resource("Docs", uri="/docs")
 * @package App\Api\V1\Controllers
 *
 */
class PapyrusController extends ApiController
{
  /**
   * The input interface implementation.
   *
   * @var \Symfony\Component\Console\Input\InputInterface
   */
  protected $input;
  
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => []]);
  }
  
  
  /**
   * Query Docs
   *
   * Get a JSON representation of all or some actors.
   *
   * @Get("/{?what}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("what", description="Which set of documents", default="Table of contents"),
   * })
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {
    $docs = [
      'routes'=>[
        'API'=>[
          'V1'=>'/api/docs/routes/api/v1',
          'V2'=>'/api/docs/routes/api/v2',

        ],
        'WEB'=>[
          'current'=>'/api/docs/routes/web/current'
        ],
      ]
    ];

    return [
      'data' => $docs,
      'count' => count($docs),

    ];
  }
  
  /**
   * Get the various Routes available
   *
   * @param string $section
   * @return array
   *
   */
  public function getRoutes($section = 'api', $version = null)
  {
    // 'API' => Dingo\Api\Facade\API::class,
    // 'APIRoute'=> Dingo\Api\Facade\Route::class,
    
    $ret = [];
    switch ($section) {
      case 'api':
        $api = app('api.router');
        
        $colRet = $api->getRoutes($version);
        
        if (is_array($colRet)) {
          $collections = $colRet;
        } else {
          $collections[] = $colRet;
        }
        
        $routes = [];
        
        foreach ($collections as $collection) {
          foreach ($collection->getRoutes() as $route) {
            $routes[] = [
              'host' => $route->domain(),
              'method' => implode('|', $route->methods()),
              'uri' => $route->uri(),
              'name' => $route->getName(),
              'action' => $route->getActionName(),
              'protected' => $route->isProtected() ? 'Yes' : 'No',
              'versions' => $route->versions(),
              'scopes' => implode(', ', $route->scopes()),
              'rate' => $this->routeRateLimit($route),
            ];
          }
        }
        $ret = $routes;
    }
    return $ret;
  }
  
  /**
   * Filter the route by URI, Version, Scopes and / or name.
   *
   * @param array $route
   *
   * @return array|null
   *
   */
  protected function filterRoute(array $route)
  {
    $filters = ['name', 'path', 'protected', 'unprotected', 'versions', 'scopes'];
    /*
    foreach ($filters as $filter) {
      if ($this->option($filter) && ! $this->{'filterBy'.ucfirst($filter)}($route)) {
        return;
      }
    }
    */
    return $route;
  }
  
  /**
   * Display the routes rate limiting requests per second. This takes the limit
   * and divides it by the expiration time in seconds to give you a rough
   * idea of how many requests you'd be able to fire off per second
   * on the route.
   *
   * @param \Dingo\Api\Routing\Route $route
   *
   * @return null|string
   *
   */
  protected function routeRateLimit($route)
  {
    list($limit, $expires) = [$route->getRateLimit(), $route->getRateLimitExpiration()];
    
    if ($limit && $expires) {
      return sprintf('%s req/s', round($limit / ($expires * 60), 2));
    }
  }
  
  /**
   * Get the value of a command option.
   *
   * @param  string  $key
   * @return string|array
   *
   */
  public function option($key = null)
  {
    if (is_null($key)) {
      return $this->input->getOptions();
    }
    
    return $this->input->getOption($key);
  }
  
  
  /**
   * Return a JSON representation of the Tag resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param  int $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function show($id)
  {
    // Create a request with basic Auth
    $request = self::$client->request(
        'GET',
        '_db/piombo/_api/document/tags/' . $id,
        [
          'auth' => self::$auth
        ]
    );

    $res = json_decode($request->getBody());

    return (array)$res;
  }
  


}
