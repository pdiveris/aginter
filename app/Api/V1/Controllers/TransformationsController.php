<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Utils;

/**
 * Class TransformationsController
 *
 * @Resource("Fake", uri="/fakes")
 * @package App\Api\V1\Controllers
 *
 */
class TransformationsController extends ApiController
{
  public function __construct(Request $request)
  {
    parent::__construct($request);

    // Only apply to a subset of methods.
    // $this->middleware('api.auth', ['only' => ['postFake', 'getFake']]);
  }

  /**
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function index(Request $request)
  {
    return response()->json(['index'=>'Index, not auth']);
  }
  
  /**
   * Quill Ops ("Delta") to "renderable" stream transformer
   *
   * In this instance we are looking at old, limited, very limited, HTML
   * Stepping stone for some grand ideas which might never be.
   *
   * @Post("/{?id}")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   *
   */
  public function postQuillDeltasToRenderable(Request $request): \Illuminate\Http\JsonResponse
  {
    $deltas = $request->input('delta');
    $format = strtoupper($request->input('to'));
  
    \Cache::store('redis')->put('operation', Utils::jsonEncode($deltas), 10);
    
    return response()->json('');
  }

  /* The Quill Transformer section */
  
  /**
   * Take the Quill nonsense and chop it nicely
   *
   * @param $input
   * @param int $index
   * @param array $tagSoup
   * @return array
   *
   */
  private static function chopTheVeg($input, &$index = 0, &$tagSoup = [])
  {
    
    $op = $input[$index];
    if (property_exists($op, 'insert')) {
      if (null !== $op->insert) {
        $tagSoup[] = $op;
      } else {  // it's null
        $j = count($tagSoup)-1;
        
        $attrs = property_exists($tagSoup[$j],'attributes') ? (array)$tagSoup[$j]->attributes : [];
        $tagSoup[$j]->attributes = (object) array_merge((array)$op->attributes, $attrs);
      }
    }
    $index++;
    
    if ($index < count($input)) {
      self::chopTheVeg($input, $index, $tagSoup);
    }
    return $tagSoup;
  }
  
  public static function makeSoup($entries)
  {
  }
  
  /**
   * The Parser and Renderer testbeds
   *
   * @param Request $request
   *
   */
  public function quill(Request $request)
  {
    echo "<h2>Take the veg from Quill and chop it (the Parser)</h2>";
  
    $string = '{"ops":[{"insert":"Theresa May"},{"attributes":{"align":"justify","header":4},"insert":null}, {"insert":"As you almost certainly know by now, Theresa May announced in early April 2017 a surprise general election. The accepted wisdom is that with this election"},{"attributes":{"align":"justify"},"insert":null}]}';
    /*
    $delta_bold = '{"ops":[{"insert":"Lorem ipsum dolor sit amet "},{"attributes":{"bold":true},"insert":"sollicitudin"},{"insert":" quam, nec auctor eros felis elementum quam. Fusce vel mollis enim."}]}';
    $delta_italic = '{"ops":[{"insert":"Lorem ipsum dolor sit amet "},{"attributes":{"italic":true},"insert":"sollicitudin"},{"insert":" quam, nec auctor eros felis elementum quam. Fusce vel mollis enim."}]}';
    $delta_link = '{"ops":[{"insert":"Lorem ipsum dolor sit amet, "},{"attributes":{"link":"http://www.example.com"},"insert":"consectetur"},{"insert":" adipiscing elit. In sed efficitur enim. Suspendisse mattis purus id odio varius suscipit. Nunc posuere fermentum blandit. \nIn vitae eros nec mauris dignissim porttitor. Morbi a tempus tellus. Mauris quis velit sapien. "},{"attributes":{"link":"http://www.example.com"},"insert":"Etiam "},{"insert":"sit amet enim venenatis, eleifend lectus ac, ultricies orci. Sed tristique laoreet mi nec imperdiet. Vivamus non dui diam. Aliquam erat eros, dignissim in quam id.\n"}]}';
    $delta_strike = '{"ops":[{"insert":"Lorem ipsum dolor sit amet "},{"attributes":{"strike":true},"insert":"sollicitudin"},{"insert":" quam, nec auctor eros felis elementum quam. Fusce vel mollis enim."}]}';
    $delta_subscript = '{"ops":[{"insert":"Lorem ipsum dolor sit"},{"attributes":{"script":"sub"},"insert":"x"},{"insert":" amet, consectetur adipiscing elit. Pellentesque at elit dapibus risus molestie rhoncus dapibus eu nulla. Vestibulum at eros id augue cursus egestas."}]}';
    $delta_superscript = '{"ops":[{"insert":"Lorem ipsum dolor sit"},{"attributes":{"script":"super"},"insert":"x"},{"insert":" amet, consectetur adipiscing elit. Pellentesque at elit dapibus risus molestie rhoncus dapibus eu nulla. Vestibulum at eros id augue cursus egestas."}]}';
    $delta_underline = '{"ops":[{"insert":"Lorem ipsum dolor sit amet "},{"attributes":{"underline":true},"insert":"sollicitudin"},{"insert":" quam, nec auctor eros felis elementum quam. Fusce vel mollis enim."}]}';
    */
    
    $obj = json_decode($string);
  
    echo '<pre>';
    echo $string;
  
    $x = $obj->ops;
    $blanket = self::chopTheVeg($x);
    print_r($blanket);
    echo '</pre>';
    
    echo "<h2>Take the chopped veg and make the soup (the Renderer)</h2>";
  
    self::makeSoup($blanket);
  }
}
