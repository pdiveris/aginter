<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 27/05/2018
 * Time: 12:17
 */

namespace App\Api\V1\Controllers\Media\Image\Imaginary;

use App\Api\V1\Controllers\MediaController;
use App\Helpers\Utils;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Routing\Controller;

/**
 * Class ImaginaryController
 *
 * Bridge to the Imaginary golang microservice
 *
 * @see https://github.com/h2non/imaginary#get-
 *
 *
 */
class ImaginaryController extends Controller
{
  /**
   * @var string
   */
  private $baseUrl = '';
  
  /**
   * Placeholder (.env)
   *
   * @var array
   */
  protected static $auth = ['', ''];
  
  /**
   * Visible to all subclasses and parent
   *
   * @var Client|null
   */
  protected static $client = null;
  
  /**
   * ImaginaryController constructor.
   *
   * @param Request $request
   */
  public function __construct(Request $request)
  {
    $this->baseUrl = env('IMAGE_FACTORY', 'http://www.aginter.eu/factory');
  
    self::$auth = [env('AR_USERNAME',''), env('AR_PASSWORD', '')];
  
    if (null == self::$client) {
      self::$client = new Client(['base_uri' => $this->baseUrl]);
    }

    $this->middleware('api.auth', ['only' => ['convert', 'crop', 'update', 'destroy']]);
  }
  
  /**
   * Imaginary service API root
   *
   * @Get("/")
   * @Versions({"v1","v2"})
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function index(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  
  /**
   * Get health status
   *
   * @Get("/health")
   * @Versions({"v1","v2"})
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function health(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Get service info
   *
   * @Get("/info")
   * @Post("/info")
   * @Versions({"v1","v2"})
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function info(Request $request)
  {
    /**
     * @Get("/info")
     * @Post("/info")
     * @Versions({"v1","v2"})
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    
    $metadata = [
      "width"=>550,
      "height"=> 740,
      "type"=> "jpeg",
      "space"=> "srgb",
      "hasAlpha"=> false,
      "hasProfile"=> true,
      "channels"=> 3,
      "orientation"=> 1
    ];
    return response()->json($metadata, 200, []);
  }
  
  /**
   * Crop an image
   *
   * @Get("/crop")
   * @Post("/crop")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("gravity", type="string", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function crop(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Smart crop an image
   *
   * @Get("/smartcrop")
   * @Post("/smartcrop")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("gravity", type="string", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function smartcrop(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Resize an image
   *
   * @Get("/resize")
   * @Post("/resize")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function resize(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Enlarge an image
   *
   * @Get("/enlarge")
   * @Post("/enlarge")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function enlarge(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Extract an image
   *
   * @Get("/extract")
   * @Post("/extract")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("top", required="true", type="integer", description="top"),
   *      @Parameter("left", type="integer", description="left"),
   *      @Parameter("areawidth", required="true", type="integer", description="area width"),
   *      @Parameter("areaheight", required="false". type="integer", description="areaheight"),
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function extract(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Zoom an image
   *
   * @Get("/zoom")
   * @Post("/zoom")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("factor", type="integer", required="true", description="Zoom factor"),
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function zoom(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Create a thumbnail from an image
   *
   * @Get("/thumbnail")
   * @Post("/thumbnail")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function thumbnail(Request $request)
  {
    return response()->json([], 200, []);
  }

  /**
   * Squeeze an image
   *
   * @Get("/fit")
   * @Post("/fit")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function fit(Request $request)
  {
    return response()->json([], 200, []);
  }

  /**
   * Rotate an image
   *
   * @Get("/rotate")
   * @Post("/rotate")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("rotate", required="true", type="integer", description="rotate"),
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function rotate(Request $request)
  {
    return response()->json([], 200, []);
  }

  /**
   * Flip an image
   *
   * @Get("/flip")
   * @Post("/flip")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function flip(Request $request)
  {
    return response()->json([], 200, []);
  }

  /**
   * Flop an image
   *
   * @Get("/flop")
   * @Post("/flop")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("width", type="integer", description="width"),
   *      @Parameter("height", type="integer", description="height"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("type", type="string", description="type")
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function flop(Request $request)
  {
    return response()->json([], 200, []);
  }
  
  /**
   * Convert an image
   *
   * @Get("/convert")
   * @Post("/convert")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("type", type="string", , required="true", description="type"),
   *      @Parameter("quality", type="integer", description="quality (JPEG only)"),
   *      @Parameter("compression", type="integer", description="compression (JPEG only)"),
   *      @Parameter("file", type="string", description="Only GET method and if the -mount flag is present")
   *      @Parameter("url", type="string", description="Only GET method and if the -enable-url-source flag is present")
   *      @Parameter("embed", type="boolean", description="embed")
   *      @Parameter("force", type="boolean", description="force")
   *      @Parameter("rotate", type="boolean", description="rotate")
   *      @Parameter("norotation", type="boolean", description="")
   *      @Parameter("noprofile", type="boolean", description="")
   *      @Parameter("stripmeta", type="boolean", description="")
   *      @Parameter("flip", type="boolean", description="")
   *      @Parameter("flop", type="boolean", description="")
   *      @Parameter("extend", type="string", description="")
   *      @Parameter("background", type="string", description="Example: ?background=250,20,10")
   *      @Parameter("colorspace", type="string", description="Example: ?colorspace=rgb")
   *      @Parameter("sigma", type="float", description="")
   *      @Parameter("minampl", type="float", description="")
   *      @Parameter("field",type="string", description="Field - Only POST and multipart/form payloads")
   * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function convert(Request $request)
  {
    // http://www.aginter.eu/factory/convert?colorspace=bw&type=jpeg&file=images/organisations/16461758/15213032633_9d2de71adc_k.jpg

    $url = $this->baseUrl . '/';
    $url .=  'convert';

    // iterate through the params and build the quesry string...?
    $type = $request->input('type', 'jpeg');
    $colorpsace = $request->input('colorspace', 'bw');
    $file = $request->input('file', '');

    if ($file == '') {
     // throw exception
      return response()->json([], 422, []);
    }

    $response = self::$client->request(
        'GET',
        $url,
        [
          'auth' => self::$auth,
          'query' => ['file' => $file, 'type'=>$type, 'colorspace'=>$colorpsace]
        ]
    );

   /// $body = $response->getBody()->getContents();


    return response($response->getBody());
  }
  
  /**
   * https://github.com/h2non/imaginary#get-
   */
  
  /**
  * Accepts: image/*, multipart/form-data. Content-Type: image/*
  *
  * This endpoint allow the user to declare a pipeline of multiple independent image transformation operations all in a single HTTP request.
  *
  * Note: a maximum of 10 independent operations are current allowed within the same HTTP request.
  *
  * Internally, it operates pretty much as a sequential reducer pattern chain, where given an input image and a set of operations, for each independent image operation iteration, the output result image will be passed to the next one, as the accumulated result, until finishing all the operations.
  *
  * In imperative programming, this would be pretty much analog to the following code:
  *
  * var image
  * for operation in operations {
  *  image = operation.Run(image, operation.Options)
  * }
  * Allowed params
  * operations json required - URL safe encoded JSON with a list of operations. See below for interface details.
  * file string - Only GET method and if the -mount flag is present
  * url string - Only GET method and if the -enable-url-source flag is present
  * Operations JSON specification
  * Self-documented JSON operation schema:
  *
  * [
  *  {
  *    "operation": string, // Operation name identifier. Required.
  *    "ignore_failure": boolean, // Ignore error in case of failure and continue with the next operation. Optional.
  *    "params": map[string]mixed, // Object defining operation specific image transformation params, same as supported URL query params per each endpoint.
  *  }
  * ]
  */
  
  public function pipeline(Request $request)
  {
    return response()->json([], 200, []);
  }
}
