<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\PeopleMedia;
use Illuminate\Http\Request;
use App\Helpers\Utils;

/**
 * Class PeopleController
 *
 * @Resource("People",  uri="/people")
 * @package App\Api\V1\Controllers
 */
class PeopleController extends ApiController
{
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['store', 'update', 'destroy']]);
  }
  
  
  /**
   * Query People
   *
   * Get a JSON representation of all or some people.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'Borghese'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   * @Response(200, body={"data": { {"_key": "ALFR", "_id": "people/ALFR", "_rev": "_WxcbrDC--b", "name": "Alberto Franceschini",
   *      "country": "IT", "orgs": "BR", "tags": {{"_key": "10500807","tag": "convicted"}},
   *      "introduction": {"html": "", "node": {} },"profile": "L", "aliases": "", "title": ""}
   *    },
   *    "count": 133, "totalCount": 133,
   *    "aql": "FOR a IN people FILTER LIKE(a.name, '%%', true) SORT a.name ASC LIMIT 1,1 RETURN a ",
   *    "fields": {}
   *   } )
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {

    if (self::$orderBy == '_id') {
      self::$orderBy = 'name';
    }

    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';
    
    $aql = 'FOR p IN people
              FILTER (p.deleted == null) OR (p.deleted == \'\')
              FILTER
                LIKE(p.name, "%'.self::$query.'%", true)
              OR
                LIKE(p.aliases, "%'.self::$query.'%", true)
              OR p._id IN
                  (
                  FOR pe IN people_events FILTER pe._to IN
                      (
                          FOR e IN events
                              FILTER LIKE(e.what, "%'.self::$query.'%", true) RETURN e._id
                      )
                  RETURN pe._from
                  )
              OR p._id IN
                  (
                  FOR op IN organisations_people
                      FILTER op._from IN
                      (
                          FOR o IN organisations FILTER
                            LIKE(o.name, "%'.self::$query.'%", true)
                            OR
                            LIKE(o.initials, "%'.self::$query.'%", true)
                            RETURN o._id
                            
                      )
                      RETURN op._to
                  )
          SORT p.'.self::$orderBy.' '.$direction.'
          LIMIT '.self::$offset.','.self::$limit.'
          RETURN p
    ';

    // Create a request with basic Auth
    $response = self::$client->request(
        'POST',
        '_db/piombo/_api/cursor',
        [
          'auth' => self::$auth,
          'json' => [
            'query' => $aql,
            'options' => ['fullCount' => true],
          ]
        ]
    );

    $body = json_decode($response->getBody());

    $fieldString = $request->input('fields', '');
    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }
        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }


    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,

    ];

  }
  
  /**
   * Return a JSON representation of the People resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", required=true, description="The ID of the resource to show..")
   * })
   * @Transaction({
   *    @Request({}),
   *    @Response(200, body={ "person": {"_key": "ALGI","_id": "people/ALGI","_rev": "_WxcbrC2--J","name": "Αλέξανδρος Γιωτόπουλος","country": "GR","orgs": "17N",
   *      "aliases": null,"profile": "L","tags": {},"introduction": {"html": "<p><strong>Alexandros Giotopoulos</strong></p>","node": {}},"title": ""},
   *      "media": {"images": {{"_id": "people_media/13813798","_key": "13813798","_rev": "_WzpJf5u--J","addedBy": "Petros Diveris",
   *      "description": { "html": "", "node": { "content": {}, "type": "doc"} },"file": "0f7b183a5da5e0a90bdd12c9eb1f73e4a244800f.jpg",
   *      "license": "","mime": "image/jpeg","mountPoint": "people/ALGI","size": 3637,
   *      "source": "http://www.bbc.co.uk/greek/local/030328_17nspecial3.shtml",
   *      "_to": "media/13813796","listPos": 0,"_from": "people/ALGI","type": "image"}},"videos": {},"texts": {},"sources": {}},"country": {"_key": "GR","_id":
   *      "countries/GR","_rev": "_WlS28SC--_","name": "Greece","alpha3": "GRC", "countryCode": "300", "iso_3166_2": "ISO 3166-2:GR", "region": "Europe", "subRegion": "Southern Europe",
   *      "regionCode": "150", "subRegionCode": "039", "alpha2": "GR", "geo": {"centre": {"latitude": 39.169082, "longitude": 21.897994,"zoom": 6}}}  }
   *    ),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   *
   * @param int $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function show($id): array
  {
    $aql = "
      FOR p IN people
        FILTER p._key == '{$id}'
          RETURN {
            person: p,
              media: {
                images: (FOR v, e IN OUTBOUND p people_media FILTER e.type=='image' SORT e.listPos RETURN MERGE(v,e) ),
                videos: (FOR v, e IN OUTBOUND p people_media FILTER e.type=='video' SORT e.listPos  RETURN MERGE(v,e) ),
                texts: (FOR v, e IN OUTBOUND p people_media FILTER e.type=='text' SORT e.listPos RETURN MERGE(v,e) ),
                sources: (FOR v, e IN OUTBOUND p people_media FILTER e.type=='source' RETURN MERGE(v,e) )
              }
        
      }";

    $body = [
      'query'=>$aql,
      'options'=>[
        'fullCount'=>true
      ]
    ];

    $response = self::$client->request(
        'POST',
        "_db/piombo/_api/cursor",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
    );

    $ret = null;

    if ($response->getStatusCode()==200 || $response->getStatusCode()==201) {
      $res = json_decode($response->getBody());

      if (is_array($res->result) && count($res->result) > 0)
        $ret = (array)$res->result[0];
        $people = (array)$ret['person'];

        // add country
        $dispatcher = app('\Dingo\Api\Dispatcher');

        $countryRes = $dispatcher->version('v1')->get('/api/countries/'.$people['country']);
        $ret['country'] = (array)$countryRes;
    } else {
    }
    return $ret;
  }
  
  /**
   * Get a suitable structure for editing an existing or adding a new People resource
   *
   * @Get("/{?id}")
   * @Parameters({
   *      @Parameter("id", type="string", description="The ID of the resource to get an edit structure for.")
   * })
   * @Versions({"v1","v2"})
   * @Transaction({
   *    @Request({}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"}),
   *    @Response(200, body={"person": {"name": "","title": "","aliases": "","profile": "","country": "","from": "","to": "","category": "","source": "",
   *   "introduction": {"html": "","markdown": "","deltas": "","node": ""},"tags": {},"library": ""},
   *   "media": {"images": {},"videos": {},"texts": {},"sources": {}},"country": {},
   *   "relations": {"actors": {}} }
   *   )
   * })
   *
   * @param Request $request
   * @param string $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function edit(Request $request, $id = ''): \Illuminate\Http\JsonResponse
  {
    //
    if ($id == 'undefined' || $id == '') {
      $ret = new PeopleMedia;
    } else {
      $ret = self::show($id);

      if (null==$ret || $ret==[]) {
        $ret = new PeopleMedia;
      }
    }

    return response()->json(
        $ret,
        200,
        [
          'X-Status'=>'Got form'
        ]
    );
  }
  
  /**
   * Create document in People collection and return _id, _key and _rev
   *
   * @Post("/")
   * @Versions({"v1"})
   *
   * @Transaction({
   *    @Request({"name": "Petros Diveris", "country": "IT", "profile": "L",  "tags": "[]", "introduction": "{}"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "people/PEDI", "_key": "17502424", "_rev": "_W0AAF5m--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"})
   * })
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    try {
      $request->validate(
          [
            'name' => 'required|max:255',
            'country' => 'required',
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          ['errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed']
      );
    }

    // bypass parent to use the foxx People code key generation API
    // return parent::store($request);

    $body = $request->except(['_key', '_id', '_rev', '_collection']);

    // Create a request with basic Auth
    // db/piombo/aginter/names

    $response = self::$client->request(
        'POST',
        "_db/piombo/aginter/people",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
    );

    $res = json_decode($response->getBody());

    return response()->json(
        $res,
        $response->getStatusCode(),
        [
          'X-Status'=>'Store successful',
          'User' => $request->server('user')
        ]
      );

  }
  
  /**
   * Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
   * })
   * @Transaction({
   *    @Request({"name": "Petros Diveris", "title": "Chief Dev"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "people/PEDI", "_key": "PEDI", "_rev": "_W0_3n1O--_", "_oldRev": "_W0_3Iyq--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    try {
      $request->validate([
          'name' => 'required|max:255',
          'country' => 'required',
        ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(['errors'=>$e->errors()],
        422,
        [
          'X-Status-Reason'=>'Validation failed',
          'User' => $request->server('user')
        ]
      );
    }

    return parent::update($request, $id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @Delete("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
   * })*
   * @Transaction({
   *    @Request("", contentType="application/javascript", headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "people/PEDI","_key": "PEDI","_rev": "_W0AAF5m--_"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse  {
    //
    return parent::destroy($request, $id);
  }


}
