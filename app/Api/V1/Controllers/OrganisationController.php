<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\OrganisationMedia;
use Illuminate\Http\Request;
use App\Helpers\Utils;


/**
 * Class OrganisationController
 *
 * Organisation resource representation
 *
 * @Resource("Organisations", uri="/organisations")
 * @package App\Api\V1\Controllers
 *
 */
class OrganisationController extends ApiController
{
  /**
   * OrganisationController constructor.
   *
   * @param Request $request
   */
  public function __construct(Request $request)
  {
    parent::__construct($request);
    $this->middleware('api.auth', ['only' => ['store', 'update', 'destroy']]);
  }
  
  /**
   * Query Organisations
   *
   * Get a JSON representation of all or some organisations.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'Banda'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   * @Response(200, body={"data": { {"_key": "16461758","_id": "organisations/16461758","_rev": "_WzNbSjy--_","name": "ABBA","initials": "ABBA","aliases": "","profile": "N","country": "SE","from": "","to": "","category": "","source": "","introduction": {"html": "", "node": {} },"tags": {},"library": {} }},
   *      "count": 175,
   *      "totalCount": 175,
   *      "aql": "FOR o IN organisations FILTER LIKE(o.name, '%%', true) SORT o.name ASC LIMIT 0,5 RETURN o",
   *      "fields": {}
   *   }
   * )
   *
   * @Request({}, headers={"X-Custom": "aginter"})
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function index(Request $request): array
  {
    if (self::$orderBy == '_id') {
      self::$orderBy = 'name';
    }
    
    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';
    
    $aql = '
        FOR o IN organisations
            FILTER (o.deleted == null) OR (o.deleted == \'\')
            FILTER LIKE(o.name, "%' . self::$query . '%", true) OR LIKE(o.initials, "%' . self::$query . '%", true)
            OR o._id IN 
                (
                    FOR oe IN organisations_events 
                        FILTER oe._to IN 
                            (
                                FOR e IN events 
                                    FILTER LIKE(e.what, "%' . self::$query . '%", true)
                                RETURN e._id    
                            )
                    
                    RETURN oe._from
                )
            OR o._id IN 
                (
                    FOR op IN organisations_people 
                        FILTER op._to IN 
                            (
                                FOR p IN people 
                                    FILTER LIKE(p.name, "%' . self::$query . '%", true)
                                RETURN p._id    
                            )
                    
                    RETURN op._from
                )
          SORT o.' . self::$orderBy . ' ' . $direction . '
          LIMIT ' . self::$offset . ',' . self::$limit . '
          RETURN o
    ';
    
    
    // Create a request with basic Auth
    $response = self::$client->request(
      'POST',
      '_db/piombo/_api/cursor',
      [
        'auth' => self::$auth,
        'json' => [
          'query' => $aql,
          'options' => ['fullCount' => true],
        ]
      ]
    );
    
    $body = json_decode($response->getBody());
    
    $fieldString = $request->input('fields', '');
    $fields = [];
    
    $data = [];
    
    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);
      
      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();
        
        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }
        
        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }
    
    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,
    ];
  }
  
  /**
   * Return a JSON representation of the Organisation resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to show..")
   * })
   * @Transaction({
   *    @Request({}),
   *    @Response(200, body={"_key": "16461758", "_id": "organisations/16461758", "_rev": "_WzNbSjy--_", "name": "ABBA",
   *      "initials": "ABBA", "aliases": null, "profile": "N", "country": "SE", "from": "", "to": "", "category": "",
   *      "source": "", "introduction": {}, "media": {"images": {}, "videos": {}, "texts": {}, "sources": {}},
   *      "relations": {"actors": {} }, "country": {} }),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param string $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function show($id)
  {
    
    $aql = "
      FOR o IN organisations
        FILTER o._key == '{$id}'
          RETURN {
            organisation: o,
              media: {
                images: (FOR v, e IN OUTBOUND o organisations_media FILTER e.type=='image' SORT e.listPos  RETURN MERGE(v,e) ),
                videos: (FOR v, e IN OUTBOUND o organisations_media FILTER e.type=='video' SORT e.listPos  RETURN MERGE(v,e) ),
                texts: (FOR v, e IN OUTBOUND o organisations_media FILTER e.type=='text' SORT e.listPos  RETURN MERGE(v,e) ),
                sources: (FOR v, e IN OUTBOUND o organisations_media FILTER e.type=='source' SORT e.listPos  RETURN MERGE(v,e) )
              },
              relations: { 
                actors: (FOR v, e IN OUTBOUND o organisations_people FILTER e.relationship!='victim' RETURN MERGE(v,e) )  
              }              
          }";
    
    $body = [
      'query' => $aql,
      'options' => [
        'fullCount' => true
      ]
    ];
    
    $response = self::$client->request(
      'POST',
      "_db/piombo/_api/cursor",
      [
        'auth' => self::$auth,
        'body' => Utils::jsonEncode($body),
      ]
    );
    
    $ret = null;
    
    if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
      $res = json_decode($response->getBody());
      
      if (is_array($res->result) && count($res->result) > 0)
        $ret = (array)$res->result[0];
      
      $org = (array)$ret['organisation'];
      
      // add country
      $dispatcher = app('\Dingo\Api\Dispatcher');
      
      $countryRes = $dispatcher->version('v1')->get('/api/countries/' . $org['country']);
      
      $ret['country'] = (array)$countryRes;
      // GR_00138
    } else {
    }
    
    return $ret;
  }
  
  /**
   * Get a suitable structure for editing an existing or adding a new organisation resource
   * Essentially it presents a new or existing resource with sane default values where they do not exist
   * and with the associated structures (media, other reosurces) which might be linked to it, also with sane defaults
   *
   * @Get("edit/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", type="string", description="The ID of the resource to get an edit structure for.")
   * })
   * @Transaction({
   *    @Request({}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"}),
   *    @Response(200, body={"organisation": {"name": "","initials": "","aliases": "","profile": "","country": "",
   *      "from": "","to": "","category": "","source": "","introduction": {"html": "","markdown": "","deltas": "","node": ""},
   *      "tags": {},"library": ""},"media": {"images": {},"videos": {},"texts": {},"sources": {}},"country": {},
   *      "relations": {"actors": {}} }
   *     )
   *
   * })
   *
   * @param Request $request
   * @param string $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function edit(Request $request, $id = ''): \Illuminate\Http\JsonResponse
  {
    //
    if ($id == 'undefined' || $id == '') {
      $ret = new OrganisationMedia();
    } else {
      $ret = self::show($id);
      
      if (null == $ret || $ret == []) {
        $ret = new OrganisationMedia;
      }
    }
    
    return response()->json(
      $ret,
      200,
      [
        'X-Status' => 'Get edit successful',
        'User' => $request->server('user')
      ]
    );
  }
  
  /**
   * Add new Organisation document
   *
   * Create a new Organisation resource and return _id, _key and _rev
   *
   * @Post("/")
   * @Versions({"v1","v2"})
   *
   * @Transaction({
   *    @Request({"name": "Test test", "country": "MV", "category": "TER", "initials": "TT", "source": "Book", "tags": "[]", "introduction": "{}"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "organisations/17502424", "_key": "17502424", "_rev": "_W0AAF5m--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"})
   * })
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    try {
      $request->validate(
        [
          'name' => 'required|max:255',
          'country' => 'required',
        ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
        [
          'errors' => $e->errors()],
        422,
        [
          'X-Status-Reason' => 'Validation failed',
          'User' => $request->server('user')
        ]
      );
    }
    // bypass parent to use the foxx People code key generation API
    // return parent::store($request);
    
    $body = $request->except(['_key', '_id', '_rev', '_collection']);
    
    // Create a request with basic Auth
    $response = self::$client->request(
      'POST',
      "_db/piombo/aginter/organisations",
      [
        'auth' => self::$auth,
        'body' => Utils::jsonEncode($body),
      ]
    );
    
    $res = json_decode($response->getBody());
    
    return response()->json(
      $res,
      $response->getStatusCode(),
      [
        'X-Status' => 'Store successful',
        'User' => $request->server('user')
      ]
    );
    
    // return parent::store($request);
  }
  
  /**
   * Update the specified resource in storage. Return _id, _key, _rev and _oldRev in JSON
   *
   * @Put("/{id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
   * })
   * @Transaction({
   *    @Request({"name": "17 Νοέμβρη", "profile": "L"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "organisations/IT_00001", "_key": "IT_00001", "_rev": "_W0_3n1O--_", "_oldRev": "_W0_3Iyq--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    try {
      $request->validate(
        [
          'name' => 'required|max:255',
          'country' => 'required',
        ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
        ['errors' => $e->errors()],
        422,
        [
          'X-Status-Reason' => 'Validation failed',
          'User' => $request->server('user')
        ]
      );
    }
    
    return parent::update($request, $id);
  }


  /**
   * Remove the specified resource from storage.
   *
   * @Delete("/{?id}")
   * @Versions({"v1"})
   * @Transaction({
   *    @Request("", contentType="application/javascript", headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "organisations/17502424","_key": "17502424","_rev": "_W0AAF5m--_"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse
  {
    // headers={"X-Custom": "FooBar"}
    return parent::destroy($request, $id);
  }
}
