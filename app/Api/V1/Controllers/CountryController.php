<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Country;
use Illuminate\Http\Request;

/**
 * Class CountryController
 *
 * @Resource("Countries", uri="/countries")
 * @package App\Api\V1\Controllers
 */
class CountryController extends ApiController
{
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['edit', 'store', 'update', 'destroy']]);
  }


  /**
   * Query Countries
   *
   * Get a JSON representation of all or some countries.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'Greece'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {
    if (self::$orderBy == '_id') {
      self::$orderBy = 'name';
    }

    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';

    $aql = 'FOR c IN countries FILTER LIKE(c.name, "%' . self::$query . '%", true) SORT c.' . self::$orderBy . ' ' . $direction . ' LIMIT ' . self::$offset . ',' . self::$limit . ' RETURN c';

    // Create a request with basic Auth
    $response = self::$client->request(
        'POST',
        '_db/piombo/_api/cursor',
        [
          'auth' => self::$auth,
          'json' => [
            'query' => $aql,
            'options' => ['fullCount' => true],
          ]
        ]
    );


    $body = json_decode($response->getBody());

    $fieldString = $request->input('fields', '');
    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }

        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }

    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,

    ];
  }

  /**
   * Return a JSON representation of the Country resource with the specified ID
   *
   * @GET("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   * @Response(200, body={"_id": "AF", "name": "Afghanistan", "alpha3": "AFG", "countryCode": "004", "iso_3166_2": "ISO 3166-2:AF", "region": "Asia", "subRegion": "Southern Asia", "regionCode": "142", "subRegionCode": "034", "alpha2": "AF"})
   *
   *
   * @param  int $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function show($id)
  {
    // Create a request with basic Auth
    $request = self::$client->request(
        'GET',
        '_db/piombo/_api/document/countries/' . $id,
        [
          'auth' => self::$auth
        ]
    );

    $res = json_decode($request->getBody());

    return (array)$res;
  }

  /**
   * Get a suitable structure for editing an existing or adding a new country resource
   *
   * @Get("/{?id}")
   * @Versions({"v1","v2"})
   * @Response(200, body={"_id": "AF", "name": "Afghanistan", "alpha3": "AFG", "countryCode": "004", "iso_3166_2": "ISO 3166-2:AF", "region": "Asia", "subRegion": "Southern Asia", "regionCode": "142", "subRegionCode": "034", "alpha2": "AF"})
   *
   * @param Request $request
   * @param $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function edit(Request $request, $id): \Illuminate\Http\JsonResponse
  {
    //
    //
    if ($id == 'undefined' || $id == '') {
      $ret = new Country();
    } else {
      $ret = self::show($id);

      if (null==$ret || $ret==[]) {
        $ret = new Country;
      }
    }

    return response()->json(
        $ret,
        200,
        [
          'X-Status'=>'Got form',
          'User' => $request->server('user')
        ]
    );
  }

  /**
   * Create a new Country resource and return the newly created document in JSON
   *
   * @Post("/")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    try {
      $request->validate(
          [
            'name' => 'required|max:255',
            "alpha2" => "required",
            'alpha3' => 'required',
            "countryCode" => "required"
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          ['errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed',
            'User' => $request->server('user')
          ]
      );
    }
    return parent::store($request);
  }


  /**
   * Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to update.")
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    try {
      $request->validate(
          [
            'name' => 'required|max:255',
            'alpha3' => 'required',
            "alpha2" => "required",
            "countryCode" => "required",
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          ['errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed',
            'User' => $request->server('user')
          ]
      );
    }
    return parent::update($request, $id);
  }

    /**
     * Remove the specified resource from storage.
     *
     * @Delete("/{?id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="The ID of the resource to show..")
     * })
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse  {
    //
    return parent::destroy($request, $id);
  }

  /**
   * Get [lat, long] coordinates corresponding to a map drawing centre
   * for a country specified by its iso_3166_2 code, e.g. RO for Romania
   *
   * Europa: 48.1324482,4.1733136
   *
   * @param Request $request
   * @param string $operation
   * @param string $countryCode
   * @return \Illuminate\Http\JsonResponse
   *
   */
  public function geo(Request $request, $operation = '', $countryCode = '') {
    switch ($operation) {
      case 'centre':
        $ret = ['centre', $countryCode];
    }
    return response()->json(
        $ret,
        200,
        [
          'X-Status'=>'Got from',
          'User' => $request->server('user')
        ]
    );
  }
}
