<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Helpers\Utils;

/**
 * Class VersionController
 *
 * @Resource("Versions", uri="/versions")
 * @package App\Api\V1\Controllers
 *
 */
class VersionController extends ApiController
{
  /**
   * VersionController constructor.
   * @param Request $request
   */
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['edit', 'store', 'update', 'destroy']]);
  }
  
  /**
   * Query Versions
   *
   * Get a JSON representation of all or some versions.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'παπαδοπαίδι'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   * @Response(200, body={"data": {{"_key": "17036398","_id": "versions/17036398","_rev": "_WxlnePe--_", "version": "17N", "class": null, "parent": null }},
   *       "count": 42
   *     }
   * )
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {
    if (self::$orderBy == '_id') {
      self::$orderBy = '_id';
    }

    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';

    $aql = 'FOR a IN versions FILTER LIKE(a.type, "%' . self::$query . '%", true) SORT a.' . self::$orderBy . ' ' . $direction . ' LIMIT ' . self::$offset . ',' . self::$limit . ' RETURN a';

    // Create a request with basic Auth
    $response = self::$client->request(
        'POST',
        '_db/piombo/_api/cursor',
        [
          'auth' => self::$auth,
          'json' => [
            'query' => $aql,
            'options' => ['fullCount' => true],
          ]
        ]
    );

    $body = json_decode($response->getBody());

    $fieldString = $request->input('fields', '');
    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }

        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }

    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,

    ];
  }
  
  
  /**
   * Return a JSON representation of the Version resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   * @Transaction({
   *    @Request({}),
   *    @Response(200, body={"_key": "17036398", "_id": "versions/17036398", "_rev": "_WxlnePe--_", "version": "17N", "class": "", "parent": "" }),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param  int $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function show($id)
  {
    // Create a request with basic Auth
    $request = self::$client->request(
        'GET',
        '_db/piombo/_api/document/versions/' . $id,
        [
          'auth' => self::$auth
        ]
    );

    $res = json_decode($request->getBody());

    return (array)$res;
  }
  
  /**
   * Get a suitable structure for editing an existing or adding a new organisation resource
   * Essentially it presents a new or existing resource with sane default values where they do not exist
   * and with the associated structures (media, other reosurces) which might be linked to it, also with sane defaults
   *
   * @Get("edit/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", type="string", description="The ID of the resource to get an edit structure for.")
   * })
   * @Transaction({
   *    @Request({}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"}),
   *    @Response(200, body={"_key": "17036398", "_id": "versions/17036398", "_rev": "_WxlnePe--_", "version": "17N", "class": "", "parent": ""}
   *     )
   * })
   *
   * @param Request $request
   * @param  int $id
   * @return JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function edit(Request $request, $id): JsonResponse
  {
    //
    //
    if ($id == 'undefined' || $id == '') {
      $ret = new Version();
    } else {
      $ret = self::show($id);

      if (null==$ret || $ret==[]) {
        $ret = new Version;
      }
    }

    return response()->json(
        $ret,
        200,
        [
          'X-Status'=>'Got form', 
          'User' => $request->server('user')
        ]
    );
  }
  
  /**
   * Create a new Version resource and return _id, _key and _rev
   *
   * @Post("/")
   * @Versions({"v1","v2"})
   *
   * @Transaction({
   *    @Request({"name": "DIGI", "class": "State", "parent": "IT"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "versions/18037398", "_key": "18037398", "_rev": "_W0ABF5m--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"})
   * })
   *
   * @param Request $request
   * @param string $collection
   * @return JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): JsonResponse
  {
    try {
      $request->validate(
          [
            'version' => 'required|max:255'
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          ['errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed',
            'User' => $request->server('user')
          ]
      );
    }

    return parent::store($request);
  }
  
  
  /**
   * Update the specified Version resource in the collection.<br/>Return _id, _key, _rev and _oldRev in JSON
   *
   * @Put("/{id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
   * })
   * @Transaction({
   *    @Request({"version": "DIGI", "class": "Secret Service", "parent": "P2"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "versions/18037398", "_key": "18037398", "_rev": "_W0ABF5n--_", "_oldRev": "_W0ABF5m--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function update(Request $request, string $id, $collection = ''): JsonResponse
  {
    try {
      $request->validate(
          [
            'content' => 'required'
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          $e->errors(),
          422,
          [
            'X-Status-Reason'=>'Validation failed',
            'User' => $request->server('user')
          ]
      );
    }

    return parent::update($request, $id);
  }
}
