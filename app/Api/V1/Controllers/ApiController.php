<?php
namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Helpers\Utils;
/**
 * Class ApiController
 *
 * Version 1
 *
 * @package App\Api\V1\Controllers
 */
class ApiController extends Controller
{
  /**
   * Visible to all subclasses and parent
   *
   * @var Client|null
   */
  protected static ?Client $client = null;

  /**
   * Placeholder (.env)
   *
   * @var array
   */
  protected static array $auth = ['', ''];

  /**
   * @var string
   */
  protected static $query = '';

  /**
   * @var int
   */
  protected static $limit = 25;

  /**
   * orderBy field
   * @var string
   */
    protected static $orderBy = '_id';

  /**
   * ASC or DESC
   *
   * @var int
   */
  protected static $ascending = 1;

  /**
   * Page number
   *
   * @var int
   */
  protected static $page = 1;

  /**
   * Offset calculated from (pageNumber - 1 * limit) + 1...
   * @var int
   */
  protected static $offset = 0;

  /**
   * ?
   * @var int
   */
  protected static int $byColumn = 0;

  /**
   * ApiController constructor.
   *
   * Expected parameters from Vue.tables2 are:
   *
   * query
   * hasMore
   * cached
   * extra
   * error
   * code
   * @param Request $request
   */
  public function __construct(Request $request)
  {

    $proto = env('AR_PROTO', 'https');

    $host = env('AR_HOST','');

    self::$auth = [env('AR_USERNAME',''), env('AR_PASSWORD', '')];

    if (null === self::$client) {
      self::$client = new Client(
        [
          'base_uri' => "$proto://$host",
          'verify' => false,
        ]
      );
    }
  
    self::$limit = $request->input('limit', 25);
    self::$page = $request->input('page', 1);
    self::$query = $request->input('q', '');


    if (self::$page > 1) {
      self::$offset = ((self::$page - 1) * self::$limit);
    }

    // sort_by name.asc
    $sortBy = explode('.', $request->input('sort_by', '_id.asc'));

    // TODO: Multiple sorts?
    if (count($sortBy) < 2) {
      // throw an error
      throw new Exception('Bad sort parameters passed', 540);
    }


    self::$ascending = strtolower($sortBy[1]) === 'asc';

    self::$orderBy = $sortBy[0];
  }

  /**
   * @return string
   */
  private function getCollection(): string
  {
    $strings = explode('\\', get_class($this));

    return strtolower(str_plural(str_replace('Controller', '', $strings[count($strings) - 1])));
  }

  /**
   * Create a human readable yet unique key ;o)
   * @param Request $request
   * @param $collection
   * @return string
   */
  public function makeKey(Request $request, $collection): string
  {
    return 'AAAC';
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param string $collection
   * @return JsonResponse
   * @throws GuzzleException
   */
  public function store(Request $request, $collection = ''): JsonResponse
  {
    
    // allow for passing collection in order to force edges. just trialing.
    $collection = ($collection === '') ? self::getCollection() : $collection;

    // $key = self::makeKey($request, $collection);

    $body = $request->except(['_key', '_id', '_rev', '_collection']);

    // $body->_key = $key;

    // Create a request with basic Auth

    $response = self::$client->request(
        'POST',
        "_db/piombo/_api/document/{$collection}",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),

        ]
    );

    $res = json_decode($response->getBody());

    return response()->json(
        $res,
        $response->getStatusCode(),
        [
            'X-Status'=>'Store successful',
            'User' => $request->server('user')
        ]
    );
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return JsonResponse
   * @throws GuzzleException
   */
  public function update(Request $request, string $id, $collection = ''): JsonResponse
  {
    // allow for passing collection in order to force edges. just trialing.
    $collection = ($collection === '') ? self::getCollection() : $collection;
    
    $body = $request->except(['_key', '_id', '_rev']);

    // Create a request with basic Auth
    
    
    $response = self::$client->request(
        'PATCH',
        "_db/piombo/_api/document/{$collection}/{$id}",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
    );

    $res = json_decode($response->getBody());

    return response()->json(
        $res,
        $response->getStatusCode(),
        [
          'X-Status'=>'Update successful',
          'User' => $request->server('user')
        ]
    );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Request $request
   * @param int $id
   * @return JsonResponse
   * @throws GuzzleException
   */
  public function destroy(Request $request, int $id): JsonResponse
  {
    $collection = $this->getCollection();
    $url = "_db/piombo/_api/document/{$collection}/{$id}";

    //
    $response = self::$client->request(
        'DELETE',
        $url,
        ['auth' => self::$auth]
    );

    $res = json_decode($response->getBody());

    return response()->json(
        $res,
        $response->getStatusCode(),
        [
          'X-Status'=>'Destroy successful',
          'User' => $request->server('user')
        ]
    );
  }
  
  /**
   * Get an array with all models available
   *
   * @param Request $request
   * @return array
   */
  protected function models(Request $request): array
  {
  
    $api = app('Dingo\Api\Routing\Router');

    $ret = array();

    $ret['Manifestacja'] = 'Aginter, an atrocity mapper for the 21st century.';

    // $v =  $api->getCurrentRoute()->versions();
    $v =  'V1';
    
    $ret['Version'] = 'V1';

    $ret['Api'] = base_path();

    $scan = scandir(base_path('/app/Api/V1/Models/'));
    
    foreach ($scan as $item) {
      if ($item !== '.' && $item !== '..' && $item !== 'AginterModel.php') {
        $class = str_replace('.php', '', $item);
        $ret['Models'][$class] = "Api/$v/Models/$class";
      }
    }
    return $ret;
  }
  
  /**
   * Return an instance of the requsted model
   *
   * @param Request $request
   * @param string $model
   * @return object|string
   */
  protected function getModel(Request $request, $model = '') {
    
    $model = ucfirst($model);
    
    $models = $this->models($request);
    
    if (array_key_exists($model, $models['Models'])) {
    
    } else {
      abort(542, 'No such model in this API and/or version');
    }
    
    $model = "\App\Api\V1\Models\\$model";
    
    $instance = new $model;
    
    return \Response::json($instance);
  }
}
