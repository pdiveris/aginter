<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

/**
 * Class FakeController
 *
 * Used as debugging bench. Nothing to see here.
 *
 * @Resource("Fake", uri="/fakes")
 * @package App\Api\V1\Controllers
 *
 */
class FakeController extends ApiController
{
  /**
   * FakeController constructor.
   * @param Request $request
   */
  public function __construct(Request $request)
  {
    parent::__construct($request);

    // Only apply to a subset of methods.
    $this->middleware('api.auth', ['only' => ['postFake', 'getFake']]);
  }

  /**
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function index(Request $request)
  {
    return response()->json(['index'=>'Index, no auth']);
  }


  /**
   * Faker!
   *
   * @Get("/{?id}")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function getFake(Request $request): \Illuminate\Http\JsonResponse
  {
    return response()->json(['faker'=>'I was here!']);
  }

  /**
   * Faker!
   *
   * @Post("/{?id}")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function postFake(Request $request): \Illuminate\Http\JsonResponse
  {
    return response()->json(['faker'=>'I was here!']);
  }
}
