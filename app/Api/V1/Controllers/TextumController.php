<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Textum;
use App\Helpers\Utils;
use Dingo\Blueprint\Annotation\Method\Delete;
use Dingo\Blueprint\Annotation\Method\Get;
use Dingo\Blueprint\Annotation\Method\Post;
use Dingo\Blueprint\Annotation\Method\Put;
use Dingo\Blueprint\Annotation\Parameters;
use Dingo\Blueprint\Annotation\Resource;
use Dingo\Blueprint\Annotation\Versions;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class TextumController
 *
 * textum, texta [n.] O
 * (1) woven fabric (2) cloth (3) framework
 *
 * Please note tha form of singular (textum) and plular (text) and therefore the need for manual
 * resolution of the route(s) to the controller
 *
 * @Resource("texta", uri="/texta")
 * @package App\Api\V1\Controllers
 *
 */
class TextumController extends ApiController
{
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware(
      'api.auth',
      [
        'only' => ['edit', 'store', 'update', 'destroy']
      ]
    );
  }

  /**
   * Query Texta
   *
   * Get a JSON representation of all or some actors.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'παπαδοπαίδι'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   *
   * @param Request $request
   * @return array
   * @throws GuzzleException
   * @throws \JsonException
   *
   */
  public function index(Request $request): array
  {
    if (self::$orderBy === '_id') {
      self::$orderBy = 'head';
    }

    $direction = (self::$ascending !== true) ? 'DESC' : 'ASC';

    $aql = 'FOR t IN texta FILTER LIKE(t.head, "%' .
      self::$query . '%", true) OR  LIKE(t.body, "%' .
      self::$query . '%", true) SORT t.' . self::$orderBy . ' ' .
      $direction . ' LIMIT ' .
      self::$offset . ',' . self::$limit . ' RETURN t';

    $response = self::$client->request(
      'POST',
      '_db/piombo/_api/cursor',
      [
        'auth' => self::$auth,
        'json' => [
          'query' => $aql,
          'options' => ['fullCount' => true],
        ]
      ]
    );

    $body = json_decode(
      $response->getBody(),
      false,
      512,
      JSON_THROW_ON_ERROR
    );

    $fieldString = $request->input('fields', '');
    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }

        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }

    //
    // clean up the results (here for now, move to a view later.)
    //
    foreach ($data as $datum) {
      if (property_exists($datum, 'body')) {
        $pattern = '/.*Επα.*ΡΗ\s.*\s.*\s/mu';
        // $pattern = '/Επαναστατική Οργάνωση 17 ΝΟΕΜΒΡΗ/mu';
        $replacement = '';

        $datum->body->text = preg_replace($pattern, $replacement, $datum->body->text);
      }
    }

    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,
      'req' => $header = $request->header('Authorization', '')
    ];
  }

  /**
   * Return a JSON representation of the Textum resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param int $id
   * @return array
   * @throws GuzzleException
   */
  public function show(int $id): ?array
  {
    $aql = "
      FOR t IN texta
        FILTER t._key == '{$id}'
          RETURN {
            textum: t
      
      }";

    $body = [
      'query' => $aql,
      'options' => [
        'fullCount' => true
      ]
    ];

    $response = self::$client->request(
      'POST',
      "_db/piombo/_api/cursor",
      [
        'auth' => self::$auth,
        'body' => Utils::jsonEncode($body),
      ]
    );

    $ret = null;

    if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
      $res = json_decode($response->getBody());

      if (is_array($res->result) && count($res->result) > 0)
        $ret = (array)$res->result[0];
    }
    return $ret;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param Request $request
   * @param int $id
   * @return JsonResponse
   * @throws GuzzleException
   *
   */
  public function edit(Request $request, $id): JsonResponse
  {
    //
    //
    if ($id === 'undefined' || $id === '') {
      $ret = new Textum();
    } else {
      $ret = $this->show($id);

      if (null === $ret || $ret === []) {
        $ret = new Textum;
      }
    }

    return response()->json(
      $ret,
      200,
      [
        'X-Status' => 'Got form',
        'User' => $request->server('user')
      ]
    );
  }

  /**
   * Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to update.")
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return JsonResponse
   * @throws GuzzleException
   */
  public function update(Request $request, string $id, $collection = ''): JsonResponse
  {
    try {
      $request->validate(
        [
          'head' => 'required|max:255'
        ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
        $e->errors(),
        422,
        [
          'X-Status-Reason' => 'Validation failed',
          'User' => $request->server('user')
        ]
      );
    }

    return parent::update($request, $id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @Delete("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param Request $request
   * @param int $id
   * @return JsonResponse
   * @throws GuzzleException
   */
  public function destroy(Request $request, int $id): JsonResponse
  {
    //
    return parent::destroy($request, $id);
  }

  /**
   * Create a new Texta resource and return the newly created document in JSON
   *
   * @Post("/")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @param string $collection
   * @return JsonResponse
   * @throws GuzzleException
   */
  public function store(Request $request, $collection = ''): JsonResponse
  {
    try {
      $request->validate(
        [
          'thread' => 'required|max:255'
        ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
        [
          'errors' => $e->errors()],
        422,
        [
          'X-Status-Reason' => 'Validation failed',
          'User' => $request->server('user')
        ]
      );
    }

    return parent::store($request);
  }

  /**
   * @param Request $request
   * @param array $body
   * @return JsonResponse
   * @throws \JsonException
   */
  public function localStore(Request $request, $body = []): JsonResponse
  {
    // allow for passing collection in order to force edges. just trialing.
    $collection = "texta";

    // $key = self::makeKey($request, $collection);


    try {
      $response = self::$client->request(
        'POST',
        "_db/piombo/_api/document/{$collection}",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
      );
    } catch (GuzzleException $e) {
    }

    $res = json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
    return response()->json(
      $res,
      $response->getStatusCode(),
      [
        'X-Status' => 'Store successful',
        'User' => $request->server('user')
      ]
    );
  }

  /**
   * @param Request $request
   * @return string
   * @throws GuzzleException
   */
  public function addTokens(Request $request): string
  {

    $direction =  'ASC';

    $aql = 'FOR t IN texta FILTER LIKE(t.head, "%%", true)  SORT t.' . self::$orderBy . ' ' .
      $direction . ' RETURN t';


    $response = self::$client->request(
      'POST',
      '_db/piombo/_api/cursor',
      [
        'auth' => self::$auth,
        'json' => [
          'query' => $aql,
          'options' => ['fullCount' => true],
        ]
      ]
    );

    try {
      $body = json_decode(
        $response->getBody(),
        false,
        512,
        JSON_THROW_ON_ERROR
      );
    } catch (\JsonException $e) {
    }

    dump($body);

    return '';
  }

  /**
   * @param Request $request
   */
  public function addTexts(Request $request): void
  {
    $dir = base_path('media/texts/KOUFO');

    $files = scandir($dir);

    foreach ($files as $file) {
      if (strpos($file, '.txt') !== false) {
        $content = file_get_contents($dir . '/' . $file);
        echo "$file (" . strlen($content) . ")<br/>";

        $this->localStore(
          $request,
          [
            'thread' => "Προκηρύξεις 17Ν",
            'blurb' => substr($content, 0, strpos($content, "\n")),
            'body' => [
              'text' => $content,
              'normalized' => mb_convert_case(Utils::aTonikon($content), MB_CASE_UPPER, "UTF-8")
            ],
            "type" => "text/plain",
            "class" => [
              "manifesto"
            ],
            "language" => "gr",
            "origin" => "KOUFO",
            "createdAt" => "2018-06-14 13:28:00"
          ]
        );
      }
    }
  }

}
