<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\LinkType;
use Illuminate\Http\Request;
use App\Helpers\Utils;

/**
 * Class Controller
 *
 * @Resource("LinkTypes", uri="/linktypes")
 * @package App\Api\V1\Controllers
 *
 */
class LinkTypeController extends ApiController
{
  /**
   * LinkTypeController constructor.
   *
   * @param Request $request
   *
   */
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['edit', 'store', 'update', 'destroy']]);

  }
  
  
  /**
   * Query LinkTypes
   *
   * Get a JSON representation of all or some actors.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'παπαδοπαίδι'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {
    if (self::$orderBy == '_id') {
      self::$orderBy = 'tag';
    }

    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';

    $aql = 'FOR l IN linktypes FILTER LIKE(l.label, "%' . self::$query . '%", true) SORT l.' . self::$orderBy . ' ' . $direction . ' LIMIT ' . self::$offset . ',' . self::$limit . ' RETURN l';

    // Create a request with basic Auth
    $response = self::$client->request(
        'POST',
        '_db/piombo/_api/cursor',
        [
          'auth' => self::$auth,
          'json' => [
            'query' => $aql,
            'options' => ['fullCount' => true],
          ]
        ]
    );

    $body = json_decode($response->getBody());

    $fieldString = $request->input('fields', '');
    $fields = [];

    $data = [];

    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);

      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();

        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        }

        $data[] = $newObject;
      }
    } else {
      $data = $body->result;
    }


    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'fields' => $fields,

    ];
  }
  
  /**
   * Return a JSON representation of the LinkType resource with the specified ID
   *
   * @Get("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to show..")
   * })
   *
   * @param  int $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function show($id)
  {
    // Create a request with basic Auth
    $request = self::$client->request(
        'GET',
        '_db/piombo/_api/document/linktypes/' . $id,
        [
          'auth' => self::$auth
        ]
    );

    $res = json_decode($request->getBody());

    return (array)$res;
  }
  
  /**
   * Show the form for editing the specified resource.
   *
   * @param Request $request
   * @param  int $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function edit(Request $request, $id): \Illuminate\Http\JsonResponse
  {
    //
    //
    if ($id == 'undefined' || $id == '') {
      $ret = new LinkType();
    } else {
      $ret = self::show($id);

      if (null==$ret || $ret==[]) {
        $ret = new LinkType();
      }
    }

    return response()->json(
        $ret,
        200,
        [
          'X-Status'=>'Got form',
          'User' => $request->server('user')]
    );
  }
  
  /**
   * Create a new LinkType resource and return the newly created document in JSON`
   *
   * @Post("/")
   * @Versions({"v1"})
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    try {
      $request->validate(
          [
            'kind' => 'required|max:255',
            'weight' => 'numeric',
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          ['errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed',
            'User' => $request->server('user')
          ]
      );
    }

    // bypass parent to use the foxx actor code key generation API
    // return parent::store($request);

    $body = $request->except(['_key', '_id', '_rev', '_collection']);

    // Create a request with basic Auth
    // db/piombo/aginter/names

    $response = self::$client->request(
        'POST',
        "_db/piombo/aginter/actors",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
    );

    $res = json_decode($response->getBody());

    return response()->json(
        $res,
        $response->getStatusCode(),
        [
          'X-Status'=>'Store successful',
          'User' => $request->server('user')
        ]
    );
  }
  
  
  /**
   * Update the specified resource in storage. Return the updated fields in JSON along with _key, _revision and _id
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to update.")
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    try {
      $request->validate([
          'kind' => 'required|max:255',
          'weight' => 'numeric',
        ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(['errors'=>$e->errors()],
        422,
        [
          'X-Status-Reason'=>'Validation failed',
          'User' => $request->server('user')
        ]
      );
    }

    return parent::update($request, $id);
  }

    /**
     * Remove the specified resource from storage.
     *
     * @Delete("/{?id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="The ID of the resource to show..")
     * })
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse  {
    //
    return parent::destroy($request, $id);
  }


}
