<?php

namespace App\Api\V1\Controllers;

use App\Helpers\Utils;
use function GuzzleHttp\Psr7\mimetype_from_extension;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Class MediaController
 *
 * @Resource("Media",  uri="/media")
 * @package App\Api\V1\Controllers
 */
class MediaController extends ApiController
{
  /**
   * MediaController constructor.
   *
   * @param Request $request
   *
   */
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['edit', 'operation']]);

  }
  
  /**
   * Create a new Media resource and return _id, _key and _rev
   *
   * @Post("/")
   * @Versions({"v1"})
   * @Transaction({
   *    @Request({"title": " Βόλος", "author": "Petros Diveris", "copyright": "Petros Diveris", "file": "DSC_7342-1dla.jpg", "mime": "image/jpeg",
   *   "description": {"html": "","node": {"type": "doc","content": {{"type": "paragraph","content": {{"type": "text","text": "Βόλος"}}}}}},
   *   "size": 3654583,"mountPoint": "people/AAAA","addedBy": "Petros Diveris","source": "Nikon","license": "CC"},
   *    headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "media/17421467", "_key": "17421467", "_rev": "_WzlHcNS---"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"})
   * })
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
  
    //
    try {
      $request->validate(
          [
          // 'title' => 'required|max:255',
          'file' => 'required|max:255',
          // 'mime' => 'required|max:255',
          // 'mountPoint' => 'required|max:255',
          // 'size' => 'required|integer',
          // 'license' => 'required|max:200',
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          [
          'errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed']
      );
    }
    
    //
    return parent::store($request);
  }
  
  /**
   * Update the specified resource in storage.
   * Return _key, _revision and _id in JSON
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to update.")
   * })
   * @Transaction({
   *    @Request({"title": " Βόλος", "author": "Petros Diveris", "copyright": "Petros Diveris", "file": "DSC_7342-1dla.jpg", "mime": "image/jpeg",
   *      "description": {"html": "","node": {"type": "doc","content": {{"type": "paragraph","content": {{"type": "text","text": "Βόλος"}}}}}},
   *      "size": 3654583,"mountPoint": "people/AAAA","addedBy": "Petros Diveris","source": "Nikon","license": "CC"},
   *        headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "media/17421467", "_key": "17421467", "_rev": "_WzlHcNS---"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function update(Request $request, string $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    return parent::update($request, $id);
  }

  /**
   * Update the listPost of the resources posted to reflect the position within the array
   *
   * @Put("/tiles/listpos")
   * @Parameters({
   *      @Parameter("token", required=true, type="string", description="API auth token")
   * })
   * @Request("edges={}", contentType="application/json")
   * @Transaction({
   *    @Request({0: "people_media/17422118",1: "people_media/17421857",2: "people_media/11530782",3: "people_media/17421469",4:
   *        "people_media/17422932",5: "people_media/17422818"},
   *        headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"updated": "6 edges"}),
   *    @Response(404, body={"status_code": "404 Edge not found"}),
   *    @Response(404, body={"status_code": "414 Collection not found"}),
   *    @Response(422, body={"status_code": "415 Some other error (to be expanded)"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"})
      * })
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function updateTilesListPositions(Request $request): \Illuminate\Http\JsonResponse
  {
  
  
    $dispatcher = app('\Dingo\Api\Dispatcher');
    /*
  
  
    $resp = $dispatcher->header('AUTHORIZATION', "Bearer {$token}")
      ->version('v1')
      ->json($data)
      ->post('/api/edges');
    */
    
    $edges = $request->input('edges');
    $token = $request->query('token', '');
    
    $responses = [];
    
    if (count($edges)>0) {
      $collection = substr($edges[0], 0, strpos($edges[0],'/'));

      foreach ($edges as $index => $edge) {
        $data = [
          '_collection' => $collection,
          'listPos' => $index
        ];
        
        $resp = $dispatcher->header('AUTHORIZATION', "Bearer {$token}")
          ->version('v1')
          ->json($data)
          ->patch('/api/edges/'.$edge);
        
        $responses[] = $resp;
        // $responses[] = [$edge=>$token, '/api/edges/'.$edge];
      }
    } else {
      $collection = '';
    }
    return response()->json(
        [
          'input'=>$request->input('edges'),
          'responses'=>$responses,
          'collection'=>$collection
        ],
        200,
        [
          'X-Status'=>'Store successful'
        ]
    );
  }
  
  /**
   * Perform image transformations and return a file structure
   *
   * @Post("/transformer")
   *
   * @param Request $request
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function operation(Request $request)
  {
    // get resource (image)
    $resource = $request->input('resource');

    // get pipeline
    $pipeline = $request->input('pipeline');
    $imageLocation = 'images/' . $resource['mountPoint'].'/'.$resource['file'];
    $dispatcher = app('\Dingo\Api\Dispatcher');
    $val = $pipeline['value'];
    $imgType = 'jpeg';

    $res = [];

    $token = $request->query('token', $request->server('user'));
    
    // do something
    switch ($pipeline['operation']) {
      case 'color':
        $type = str_plural($resource['type']);

        $uri = '/api/media/image/imaginary/convert?file='.$imageLocation.'&colorspace='.$val.'&type='.$imgType;
        
      
        $res = $dispatcher->version('v1')->get($uri);  

        $size =  strlen($res);
        $mountPoint = $resource['mountPoint'];
        $file =  $resource['file'];
        $mnt = base_path() . "/media/$type/$mountPoint/";
        $name = Utils::uniqueFileName($mnt, $file);

        // store the file on filesystem!
        $copyRes = Utils::filePutContents($mnt.$name, $res);
        $msg = 'Added '.$mnt.$name;
        
        $persisted = self::persistAsset($name, $mountPoint, $size);
        
        $linked = null;

        if (is_array($persisted) && array_key_exists('_id', $persisted)) {
          // 3. create link to collection if necessary (actor, org, event)

          $linkTo = str_replace('/',',',$mountPoint);
          if ($linkTo!=='') {
            // actors,AAAA   media/32974324   image

            $linked = self::linkMediaAssetToDocument($mountPoint, $persisted['_id'], 'image', $token);
          }
        }
            return $persisted;
            // http://www.aginter.eu/factory/convert?colorspace=bw&type=jpeg&file=images/organisations/16461758/15213032633_9d2de71adc_k.jpg
            break;
      case 'crop':
            break;
      case 'thumbnail':
            break;
    }
    return [];
  }

  public function tranformWas(Request $request) {
    // http://www.aginter.eu/factory/fit?colorspace=bw&type=png&width=800&height=800&file=images/actors/demo/fiat.jpg
  
    $factory = "http://www.aginter.eu/factory";
  
    $type = str_plural($request->input('type', 'image'));
  
    $mountPoint = $request->input('mountPoint');
  
    $file = "$type/".
      $request->input('mountPoint').'/'.
      $request->input('file');
  
  
    $earl = "$factory/fit?colorspace=bw&type=png&width=800&height=800&file=";
    $earl .= $file;
  
    $client = new \GuzzleHttp\Client();
  
    $res = $client->request('GET', $earl, [
      'auth' => ['user', 'pass']
    ]);
  
    $msg = '';
  
    if ($res->getStatusCode()==200) {
      $mnt = base_path() . "/media/$type/" . $mountPoint . '/';
      $name = $request->input('file');
      $name = Utils::uniqueFileName($mnt, $name);
      $status = Utils::filePutContents($mnt . $name, $res->getBody());
      $msg = 'Added '.$mnt.$name;
    
      $persisted = self::persistAsset($name, $mountPoint, count($res->getBody()));
      $linked = null;
    
      if (is_array($persisted) && array_key_exists('_id', $persisted)) {
        // 3. create link to collection if necessary (actor, org, event)
      
        $linkTo = str_replace('/',',',$mountPoint);
        if ($linkTo!=='') {
          // actors,AAAA   media/32974324   image
        
          $linked = self::linkMediaAssetToDocument($mountPoint, $persisted['_id'], $type);
        }
      }
    }
  
    return [
      'status'=> $res->getStatusCode(),
      'content-type'=> $res->getHeader('content-type'),
      'persisted' => $persisted,
      'message' => $msg,
      'link' => $linked
    ];
  
  
  }
  
  /**
   * Upload a file from filedropjs
   *
   * @Post("/filedrop")
   * @Versions({"v1"})
   *
   * Get the context (actors, orgs, media lib)
   * Depending on the context set mountPoint
   * Beware that paths passed in the query string are sperated by commas e.g. actors,AAAA->actors/AAAA
   * Generate a unique filename for that particular context. Use Helpers generateUniqueFilename(name, destination)
   * Write file
   * Persist
   *
   * @param Request $request
   * @return string
   *
   */
  public function handleFileDrop(Request $request)
  {
    $server = $request->server();

    $mountPoints = $request->query('mount_points','');
    
    $type = $request->query('asset_type','');
    
    if ($type !== '')
      $mountPoints = str_plural($type) . ',' . $mountPoints;
    
    $linkTo = $request->query('link_to', '');
    
    if ($mountPoints !== '')
      $mountPoints .= ',';

    $mnt = base_path() . '/media/' . str_replace(',', '/', $mountPoints);

    $data = $request->getContent();

    $name = urldecode(@$server['HTTP_X_FILE_NAME']);

    $name = Utils::uniqueFileName($mnt, $name);
    
    // 1. store on filesystem
    // $res = @file_put_contents($mnt.$name, $data);
    
    $res = Utils::filePutContents($mnt.$name, $data);
    
    // 2. create entry in media collection

    $persisted = self::persistAsset($name, $request->query('mount_points',''), $res);

    $token = $request->query('token', '');

    if (is_array($persisted) && array_key_exists('_id', $persisted)) {
      // 3. create link to collection if necessary (actor, org, event)
      if ($linkTo!=='') {
        // actors,AAAA   media/32974324   image
        $linked = self::linkMediaAssetToDocument(
            str_replace(',', '/', $linkTo),
            $persisted['_id'],
            $type,
            $token
        );
      }
    }


    if ($res) {
      $output = sprintf("Wrote %s bytes to %s", $res, $mnt.$name);
    } else {
      $output = sprintf("Error writing to %s", $mnt.$name);
    }
    return $persisted;
  }
  
  /**
   * @param string $from
   * @param string $to
   * @param string $type
   * @param string $token
   * @return bool
   */
  public static function linkMediaAssetToDocument($from = '', $to = '', $type = '', $token = '') {

    $lefts = explode('/', $from);
    if (count($lefts) < 2) {
      // throw exception
    }

    $rights = explode('/', $to);
    if (count($rights) < 2) {
      // throw exception
    }
    
    $edges = $lefts[0] . '_' .$rights[0];    // i.e. actors_media

    $dispatcher = app('\Dingo\Api\Dispatcher');

    $data = [
        '_collection' => $edges,
        '_from' => $from,
        '_to' => $to,
        'type' => str_singular($type)
      ];


    $resp = $dispatcher->header('AUTHORIZATION', "Bearer {$token}")
              ->version('v1')
              ->json($data)
              ->post('/api/edges');

    return $resp;
  }

  /**
   * Persist the newly added file in the Datastore
   *
   * @param string $file
   * @param string $mnt
   * @param int $size
   * @return bool
   */
  public static function persistAsset($file = '', $mnt = '', $size = 0) {
    $dispatcher = app('\Dingo\Api\Dispatcher');
    
    $mnt = str_replace(',', '/', $mnt);

    $chunks = explode('.', $file);
    
    $ext = '';
    
    if (count($chunks)>1)
      $ext = $chunks[1];
    
    $data = [
      'file' => "$file",
      'mime' => mimetype_from_extension($ext),
      'description' => ['html'=>'', 'markdown'=>'', 'node'=>null ],
      'size' => $size,
      'mountPoint' => "$mnt",
      'addedBy' => 'Petros Diveris'

    ];
  
    $resp = $dispatcher->version('v1')
      ->json($data)
      ->post('/api/media');
    
    return $resp;
  }

    /**
     * Remove the specified resource from storage.
     *
     * @Delete("/{?id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="The ID of the resource to show.."),
     * })
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse  {
    //
    return parent::destroy($request, $id);
  }

  /**
   * Return some demo data for MediaManager (tiles) in JSON
   * @Get("/tiles")
   * @Versions({"v1"})
   *
   * @param Request $request
   */
  
  public function tiles(Request $request)
  {
    if (!$request) { // cheat code inspection
    
    }
    
    $json = '
{
	"media": {
		"images": [{
				"_id": "demo/00000001",
				"_key": "00000001",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "16835939_1268079313260314_6772288626647710389_o.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 79173,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "41 Bus",
				"type": "image"
			},
			{
				"_id": "demo/00000002",
				"_key": "00000002",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "25360745358_dfffdca65a_k.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 484802,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Αυτοπορτρέτο",
				"type": "image"
			},
			{
				"_id": "demo/00000003",
				"_key": "00000003",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "25573186544_6456fb1ba6_o.png",
				"mime": "image/png",
				"mountPoint": "actors/demo",
				"size": 1404984,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Cooperativa Booze",
				"type": "image"
			},
			{
				"_id": "demo/00000004",
				"_key": "00000004",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "26891028361_d7b051b3b2_k.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 441348,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Milverton horror",
				"type": "image"
			},
			{
				"_id": "demo/00000005",
				"_key": "00000005",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "26892010962_291f2bb8e3_k.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 896276,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Jaguar",
				"type": "image"
			},
			{
				"_id": "demo/00000006",
				"_key": "00000006",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "27646056732_9990ba8f93_k.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 722219,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Oh brother Nuradin",
				"type": "image"
			},
			{
				"_id": "demo/00000007",
				"_key": "00000007",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "37597595791_34f979478c_k.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 431767,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Beatle, Athens",
				"type": "image"
			},
			{
				"_id": "demo/00000008",
				"_key": "00000008",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "bike_1366x2048.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 779099,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "A bile in Bruseels",
				"type": "image"
			},
			{
				"_id": "demo/00000009",
				"_key": "00000009",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "pedro_400x400.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 19593,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Self portrait",
				"type": "image"
			},
			{
				"_id": "demo/00000010",
				"_key": "00000010",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "sharon.jpg",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 632016,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "Sharon",
				"type": "image"
			},
			{
				"_id": "demo/00000010",
				"_key": "00000010",
				"_rev": "_WV92mK---_",
				"addedBy": "Petros Diveris",
				"author": "Petros Diveris",
				"copyright": "Petros Diveris",
				"file": "taratata.png",
				"mime": "image/jpeg",
				"mountPoint": "actors/demo",
				"size": 1404984,
				"source": "",
				"sourceExtras": [],
				"timestamp": "unknown",
				"title": "More Booze",
				"type": "image"
			}
		]
	}
}
';
    
    header('Content-Type: application/json');
    echo($json);
    return;
  }
}
