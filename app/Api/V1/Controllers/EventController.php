<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Models\Event;
use App\Api\Versionable;
use App\Api\V1\Models\EventMedia;
use Illuminate\Http\Request;
use App\Helpers\Utils;

/**
 * Class EventController
 *
 * @Resource("Events", uri="/events")
 * @package App\Api\V1\Controllers
 */
class EventController extends ApiController
{
  /**
   * EventController constructor.
   *
   * @param Request $request
   */
  public function __construct(Request $request)
  {
    parent::__construct($request);

    $this->middleware('api.auth', ['only' => ['store', 'update', 'destroy']]);
  }
  
  /**
   * Query Events
   *
   * Get a JSON representation of all or some events.
   *
   * @Get("/{?query,page,limit,sort_by}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("query", description="A term to search for e.g. 'massacre'", default=""),
   *      @Parameter("page", description="The page of results to view.", default=1),
   *      @Parameter("limit", description="The amount of results per page.", default=25),
   *      @Parameter("sort_by", description="The field to sort by , and the direction e.g. name.asc.")
   * })
   * @Response(200, body={"data": {"_key": "13692140","_id": "events/13692140","_rev": "_WzNbnV6--_","when": "1969-03-30T23:00:00.000Z",
   *   "what": "Birth of Petros Diveris","who": "Toula Sieti, Kostas Diveris","country": "DE","location": "Weimar", "tags": {{"_key":"14395610", "tag":"DDR" }},"eventDate": "","labels": "","library": "",
   *   "latitude_deg": 50.9712734,"longitude_deg": 11.3291517, "map_center": {"latitude_deg": "50.9712734", "longitude_deg": "11.3291517", "zoom": "13"},
   *   "introduction": { "html": "The <strong>most</strong> important event that took place on March 31, 1969 in Weimar, DDR. ",
   *    "node": { "content": {{"type": "paragraph","content": {{"type": "text","text": "The important event that took place on March 31, 1969 in Weimar, DDR. "}}}  }, "type": "doc"}
   *   },
   *   "count": 131,
   *   "totalCount": 131,
   *   "aql": "FOR e IN events FILTER LIKE(e.what, '%%', true)  SORT e.when ASC LIMIT 0,5 RETURN e",
   *   "fields": {} }
   *   } )
   *
   * @param Request $request
   * @return array
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function index(Request $request): array
  {
    if (self::$orderBy == '_id') {
      self::$orderBy = 'when';
    }
    
    // test use of Symfony's Dump Server with the Laravel "adapter
    // dump($request->all());
    
    $direction = (self::$ascending == true) ? 'ASC' : 'DESC';
    
    $aql = '
        FOR e IN events
            FILTER (e.deleted == null) OR (e.deleted == \'\')
            FILTER LIKE(e.what, "%'.self::$query.'%", true) OR LIKE(e.where, "%'.self::$query.'%", true) OR LIKE (e.when, "%'.self::$query.'%", true)
            OR e._id IN
                (
                    FOR oe IN organisations_events
                        FILTER oe._from IN
                            (
                                FOR o IN organisations
                                    FILTER LIKE(o.name, "%'.self::$query.'%", true) OR  LIKE(o.initials, "%'.self::$query.'%", true)
                                RETURN o._id
                            )
                    RETURN oe._to
                )
            OR e._id IN
                (
                    FOR pe IN people_events
                        FILTER pe._from IN
                            (
                                FOR p IN people
                                    FILTER LIKE(p.name, "%'.self::$query.'%", true)
                                RETURN p._id
                            )
                    RETURN pe._to
                )
    ';

    $aql .= 'SORT e.' . self::$orderBy . ' ' . $direction . ' LIMIT ' . self::$offset . ',' . self::$limit . ' RETURN e';
    
    
    // Create a request with basic Auth
    $response = self::$client->request(
        'POST',
        '_db/piombo/_api/cursor',
        [
        'auth' => self::$auth,
        'json' => [
          'query' => $aql,
          'options' => ['fullCount' => true],
          ]
        ]
    );
    
    $body = json_decode($response->getBody());
    
    $fieldString = $request->input('fields', '');
    $fields = [];
    
    $data = [];
    
    if ($fieldString !== '') {
      $fields = explode(',', $fieldString);
      
      // $response->result
      foreach ($body->result as $object) {
        $arr = (array)$object;
        $newObject = new \stdClass();
        
        foreach ($fields as $field) {
          if (array_key_exists($field, $arr)) {
            $newObject->$field = $arr[$field];
          }
        };
        $data[] = $newObject;
      };
    } else {
      $data = $body->result;
    }
    
    return [
      'data' => $data,
      'count' => (int)$body->extra->stats->fullCount,
      'totalCount' => (int)$body->extra->stats->scannedFull,
      'aql' => $aql,
      'direction'=>$direction,
      'fields' => $fields,
    ];
  }
  
  /**
   * Return JSON representation of Event resource with the identified by ID
   *
   * @Get("/{?id}")
   * @Versions({"v1","v2"})
   * @Parameters({
   *      @Parameter("id", required=true, type="string", description="The ID of the resource to show..")
   * })
   * @Transaction({
   *    @Request({}),
   *    @Response(200, body={"event": {"_key": "800802-00009","_id": "events/800802-00009","_rev": "_WxoUvVS--H","when": "1980-08-02T00:00:00.000Z","what": "Bologna Massacre","who": "NAR","labels": "fascist","country": "IT","location": "Bologna","persons": "Francesca Mambro, Valerio Fioravanti","comments": "85 dead, 200 injured, Terza Posizione","tags": {},"latitude_deg": 44.5160084,"longitude_deg": 11.2820739,"map_center": {"latitude_deg":"44.5160084", "longitude_deg": "11.2820739", "zoom": "15"},"introduction": {"html": "<p><br></p>","markdown": "","node": ""}},"media": {"images": {{"_id": "events_media/15277095","_key": "15277095","_rev": "_WzpKDb6--_","addedBy": "Petros Diveris","author": "Beppe Briguglio, Patrizia Pulga, Medardo Pedrini, Marco Vaccari","copyright": "www.stragi.it","description": {"deltas": {},"html": "By Beppe Briguglio, Patrizia Pulga, Medardo Pedrini, Marco Vaccari - www.stragi.it/, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=301978","markdown": "","node": {"content": {{"type": "paragraph","content": {{"type": "text","text": "By Beppe Briguglio, Patrizia Pulga, Medardo Pedrini, Marco Vaccari - www.stragi.it/, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=301978"}}}},"type": "doc"}},"file": "Stragedibologna-2.jpg","license": "CC BY-SA 3.0","mime": "image/jpeg","mountPoint": "events/800802-00009","size": 32396,"source": "Wikipedia - www.stragi.it","title": "Rescue teams making their way through the rubble.","_to": "media/15277093","listPos": 0,"_from": "events/800802-00009","type": "image"},{"_id": "events_media/15277418","_key": "15277418","_rev": "_WzpKDbu--I","addedBy": "Petros Diveris","author": "Zweifel","copyright": "Zweifel","description": {"deltas": {},"html": "A picture of the plaque at Bologna Centrale. I took this picture this past summer.","markdown": "","node": {"content": {{"type": "paragraph","content": {{"type": "text","text": "A picture of the plaque at Bologna Centrale. I took this picture this past summer."}}}},"type": "doc"}},"file": "Bologna_massacre_memorial.jpg","license": "Public domain","mime": "image/jpeg","mountPoint": "events/800802-00009","size": 72461,"source": "Wikipedia","title": "Memorial","_to": "media/15277416","listPos": 0,"_from": "events/800802-00009","type": "image"}},"videos": {},"texts": {},"sources": {}},"relations": {"people": {},"organisations": {{"_id": "organisations_events/15277374","_key": "15277374","_rev": "_Woal4PS--_","category": "TER","comments": "Strage di Bologna","country": "IT","from": 1977,"initials": "NAR","introduction": {"html": "<p>The <strong>Nuclei Armati Rivoluzionari..","node": {"content": {{"type": "paragraph","content": {{"type": "text","text": "The Nuclei Armati Rivoluzionari"}}}},"type": "doc"}},"name": "Nuclei Armati Rivoluzionari","profile": "F","tags": {},"to": 1981,"victims": "33,85","_to": "events/800802-00009","_from": "organisations/IT_00079"}}},"country": {"_key": "IT","_id": "countries/IT","_rev": "_WlSsK9K--_","name": "Italy","alpha3": "ITA","countryCode": "380","iso_3166_2": "ISO 3166-2:IT","region": "Europe","subRegion": "Southern Europe","regionCode": "150","subRegionCode": "039","alpha2": "IT","geo": {"centre": {"latitude": 41.71944,"longitude": 13.629067,"zoom": 6}}}}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param  int $id
   * @return array
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function show($id)
  {
    $aql = "
      FOR ev IN events
        FILTER ev._key == '{$id}'
          RETURN {
            event: ev,
              media: {
                images: (FOR v, e IN OUTBOUND ev events_media FILTER e.type=='image' SORT e.listPos  RETURN MERGE(v,e) ),
                videos: (FOR v, e IN OUTBOUND ev events_media FILTER e.type=='video' SORT e.listPos RETURN MERGE(v,e) ),
                texts: (FOR v, e IN OUTBOUND ev events_media FILTER e.type=='text' SORT e.listPos  RETURN MERGE(v,e) ),
                sources: (FOR v, e IN OUTBOUND ev events_media FILTER e.type=='source' SORT e.listPos  RETURN MERGE(v,e) )
              },
              relations: {
                people: (FOR v, e IN INBOUND ev people_events RETURN MERGE(v,e) ),
                organisations: (FOR v, e IN INBOUND ev organisations_events RETURN MERGE(v,e) )
              }
          }";


    $body = [
      'query' => $aql,
      'options' => [
        'fullCount' => true
      ]
    ];
    
    $response = self::$client->request(
        'POST',
        "_db/piombo/_api/cursor",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
    );


    $ret = null;

    if ($response->getStatusCode()==200 || $response->getStatusCode()==201) {
      $res = json_decode($response->getBody());

      if (is_array($res->result) && count($res->result) > 0)
        $ret = (array)$res->result[0];

      $event = (array)$ret['event'];

      // add country
      $dispatcher = app('\Dingo\Api\Dispatcher');

      $countryRes = $dispatcher->version('v1')->get('/api/countries/'.$event['country']);
      $ret['country'] = (array)$countryRes;
    } else {
      $ret = [];
    }

    return $ret;
  }
  
  /**
   * Get a suitable structure for editing an existing or adding a new event resource
   *
   * @Get("/{?id}")
   * @Versions({"v1","v2"})
   * @Transaction({
   *    @Request({}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"}),
   *    @Response(200, body={"event": {"_key": "13692140","_id": "events/13692140","_rev": "_WzNbnV6--_","when": "1969-03-30T23:00:00.000Z","what": "Birth of Petros Diveris",
   *      "who": "Toula Sieti, Kostas Diveris","country": "DE","location": "Weimar","tags": {{"_key": "14395610","tag": "DDR"}},
   *      "eventDate": null,"labels": null,"library": null,"latitude_deg": 50.9712734,"longitude_deg": 11.3291517,
   *      "map_center": {"latitude_deg":"50.9712734", "longitude_deg": "11.3291517", "zoom": "13"},
   *      "introduction": {"html": "The <strong>most</strong> important event that took place on March 31, 1969 in Weimar, DDR. ",
   *      "markdown": null,"node": {"content": {{"type": "paragraph","content": {{"type": "text","text": "The "},{"type": "text",
   *        "marks": {{"type": "strong"}},"text": "most "},{"type": "text","text": "important event that took place on March 31, 1969 in Weimar, DDR. "}}}},"type": "doc"}}},
   *      "media": {"images": {{"_id": "events_media/13731988",}},"videos": {},"texts": {},"sources": {}},
   *      "relations": {"people": {{"_id": "people_events/14228315","_to": "events/13692140","_from": "people/KODI"},{"_id": "people_events/14228290","_to": "events/13692140","_from": "people/TUSI"}},
   *        "organisations": {{"_id": "organisations_events/14462667","_to": "events/13692140","_from": "organisations/14395628"}}},
   *      "country": {"_key": "DE","name": "Germany","geo": {"centre": {"latitude": 50.048132,"longitude": 10.619596}}}}
   *     )
   *
   * })
   *
   * @param Request $request
   * @param string $id
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function edit(Request $request, $id = ''): \Illuminate\Http\JsonResponse
  {
    //
    if ($id == 'undefined' || $id == '') {
      $ret = new EventMedia();
    } else {
      $ret = self::show($id);

      if (null==$ret || $ret==[]) {
        $ret = new EventMedia;
      }
    }

    return response()->json(
        $ret,
        200,
        [
          'X-Status'=>'Got form',
          'User' => $request->server('user')
        ]
    );
  }
  
  /**
   * Create a new Event resource and return _id, _key and _rev
   *
   * @Post("/")
   * @Versions({"v1"})
   *
   * @Transaction({
   *    @Request({"what": "Επίθεση με ρουκέτες κατά της τράπεζας Barclays", "country": "GR", "when": "1992-04-12"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "events/17617990","_key": "17617990","_rev": "_W0mqZDi--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"})
   * })
   *
   * @param Request $request
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function store(Request $request, $collection = ''): \Illuminate\Http\JsonResponse
  {
    //
    try {
      $request->validate(
          [
          'what' => 'required|max:255',
          'when' => 'required|max:255',
          'country' => 'required',
          ]
      );
    } catch (\Illuminate\Validation\ValidationException $e) {
      return response()->json(
          ['errors'=>$e->errors()],
          422,
          [
            'X-Status-Reason'=>'Validation failed',
            'User' => $request->server('user')
          ]
      );
    }
  
    // bypass parent to use the foxx People code key generation API
    // return parent::store($request);
  
    $body = $request->except(['_key', '_id', '_rev', '_collection']);
  
    // Create a request with basic Auth
    // db/piombo/aginter/names
  
    $response = self::$client->request(
        'POST',
        "_db/piombo/aginter/events",
        [
          'auth' => self::$auth,
          'body' => Utils::jsonEncode($body),
        ]
    );
  
    $res = json_decode($response->getBody());
  
    return response()->json(
        $res,
        $response->getStatusCode(),
        [
          'X-Status'=>'Store successful',
          'User' => $request->server('user')
        ]
    );
   // return parent::store($request);
  }
  
  /**
   * Update the specified resource in storage. Return _id, _key, _rev and _oldRev in JSON
   *
   * @Put("/{?id}")
   * @Versions({"v1"})
   * @Parameters({
   *      @Parameter("id", description="The ID of the resource to update.")
   * })
   * @Transaction({
   *    @Request({"what": "Επίθεση στα ματ με πολυβόλο"}, headers={"Authorization": "Bearer AbCdEf123456"}),
   *    @Response(200, body={"_id": "events/17617990", "_key": "17617990", "_rev": "_W0mtUG--_", "_oldRev": "_W0mqZDi--_"}),
   *    @Response(422, body={"status_code": "422 Validation failed"}),
   *    @Response(401, body={"status_code": "401 Unauthorized"}),
   *    @Response(404, body={"status_code": "404 Not found"})
   * })
   *
   * @param Request $request
   * @param string $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   */
  public function update(Request $request, $id, $collection = ''): \Illuminate\Http\JsonResponse
  {
    $event = new Event();
    return $event->update($id);
    //
    //    try {
    //      $request->validate([
    //          'what' => 'required|max:255',
    //          'when' => 'required|max:255',
    //          'country' => 'required',
    //        ]
    //      );
    //    } catch (\Illuminate\Validation\ValidationException $e) {
    //      return response()->json(['errors'=>$e->errors()],
    //        422,
    //          [
    //            'X-Status-Reason'=>'Validation failed',
    //            'User' => $request->server('user')
    //          ]
    //        );
    //    }
    //
    //    return parent::update($request, $id);
  }

    /**
     * Remove the specified resource from storage.
     *
     * @Delete("/{?id}")
     * @Versions({"v1"})
     * @Transaction({
     *    @Request("", contentType="application/javascript", headers={"Authorization": "Bearer AbCdEf123456"}),
     *    @Response(200, body={"_id": "events/17617990","_key": "17617990","_rev": "_W0mtUG---_"}),
     *    @Response(401, body={"status_code": "401 Unauthorized"}),
     *    @Response(404, body={"status_code": "404 Not found"})
     * })
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
  public function destroy(Request $request, int $id): \Illuminate\Http\JsonResponse  {
    //
    return parent::destroy($request, $id);
  }
}
