<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

/**
 * Class EventMedia
 * @package App\Api\V1\Models
 */
class EventMedia
{
  /**
   * The event
   *
   * @var Event
   */
  public $event;

  /**
   * The media object
   *
   * @var Media
   */
  public $media;

  /**
   * Country
   *
   * @var
   */
  public $country;

  /**
   * Relations
   *
   * @var array
   */
  public $relations = [
    'people' => [],
    'organisations' => [],
  ];


  /**
   * PeopleMedia constructor.
   */
  public function __construct()
  {
    $this->event = new Event();

    $this->media = new Media();

    $this->country = new Country();
  }
}
