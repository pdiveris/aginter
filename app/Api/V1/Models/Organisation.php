<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

class Organisation extends AginterModel
{
  /**
   * Name, e.g. Brigate Rosse
   *
   * @var string
   */
  public $name = '';

  /**
   * Initials e.g. BR
   *
   * @var string
   */
  public $initials = '';

  /**
   * Aliases e.g. Ταινίας
   *
   * @var string
   */
  public $aliases = '';


  /**
   * Profile L (left), F (fascist) etc.
   *
   * @var string
   */
  public $profile = '';

  /**
   * Country, e.g. 'Italy'
   *
   * @var object
   */
  public $country;

  /**
   * Year active from e.g. 1977
   *
   * @var string
   *
   */
  public $from = '';

  /**
   * Year active to, e.g. 1986
   *
   * @var string
   */
  public $to = '';

  /**
   * What is it e.g. 'TER' for terrorist, 'MIL' for military etc.
   *
   * @var string
   */
  public $category = '';

  /**
   * Where this info comes from e.g. "book - Political Terrorism"
   * This is a temporary field to get us started, it will be pushed to the "sources" in Media
   * @var string
   */
  public $source = '';

  /**
   * Introduction, e.g. "Pier Paolo Pasolini was an Italian film director, poet, writer, and intellectual. "
   *
   * @var array
   */
  public $introduction = ['html'=>'', 'markdown'=>'', 'deltas'=>'', 'node'=>null];
  
  /**
   * A list of tags, if any
   *
   * @var array
   */
  public $tags = [];

  /**
   * Documents, images, video etc
   *
   * @var object
   */
  public $library;
}
