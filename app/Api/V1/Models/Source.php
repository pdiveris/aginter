<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

class Source extends AginterModel
{
  /**
   * Title, e.g. "Political Terrorism : A New Guide to Actors, Authors, Concepts, Data Bases, Theories, and Literature"
   *
   * @var string
   */
  public $title = '';

  /**
   * Authors e.g. ["A.J. Jongman"]
   *
   * @var string
   */
  public $authors = '';

  /**
   * compactAuthors
   *
   * @var string e.g. "KARA"
   */
  public $compactAuthors = '';

  /**
   * Publisher e.g. "Οδυσσέας"
   *
   * @var string
   */
  public $publisher = '';


  /**
   * Location, e.g. 'Athens'
   *
   * @var string
   */
  public $location;

  /**
   * Country, e.g. 'IT'
   *
   * @var string
   */

  public $country;

  /**
   * Year, e.g. '1988'
   *
   * @var string
   */
  public $year;

  /**
   * Pages, e.g. 143
   *
   * @var integer
   */
  public $pages;

  
  /**
   * Introduction
   * e.g. "Pier Paolo Pasolini was an Italian film director, poet, writer, and intellectual."
   *
   * @var array
   */
  public $notes = ['html'=>'', 'markdown'=>'', 'node'=>null];
  
  /**
   * A list of tags, if any
   *
   * @var array
   */
  public $tags = [];

  /**
   * Documents, images, video etc
   *
   * OBSOLETED
   *
   * @var object
   */
  public $library;
}
