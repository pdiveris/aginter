<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

/**
 * Class Country
 * @package App\Api\V1\Models
 */
class Country extends AginterModel
{

  /**
   * Key.
   * Exists in AginterModel but we want it public here
   *
   * @var string
   */
  public $_key = '';

  /**
   * Name, e.g. Afghanistan
   *
   * @var string
   */
  public $name = '';

  /**
   * Alpha3 code e.g. "AFG"
   *
   * @var string
   */
  public $alpha3 = '';

  /**
   * Country code e.g. "004"
   *
   * @var string
   */
  public $countryCode = '';

  /**
   * ISO_3166-2 e.g. "ISO 3166-2:AF"
   *
   * @var string
   */
  public $iso_3166_2 = '';

  /**
   * Region e.g. "Asia"
   *
   * @var string
   */
  public $region = '';

  /**
   * Sub-region e.g. "Southern Asia"
   *
   * @var string
   */
  public $subRegion = '';

  /**
   * Region code e.g. 142
   *
   * @var string
   */
  public $regionCode = '';

  /**
   * Sub-region code 034
   *
   * @var string
   */
  public $subRegionCode = '';

  /**
   * Alpha 2 e.g. "AG"
   * @var string
   */
  public $alpha2 = '';
  
  /**
   * @var array
   */
  public $geo = [];
}
