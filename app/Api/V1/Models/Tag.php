<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

class Tag extends AginterModel
{
  /**
   * The tag, e.g. Aldo Moro
   *
   * @var string
   */
  public $tag = '';
  
  /**
   * Class, e.g. 'politician'
   *
   * @var string
   */
  public $class = '';

  /**
   * Spectrun from -1 to +1 (left to right)
   *
   * @var float
   */
  public $spectrum = 0.5;
  /**
   * A list of parents, if any
   *
   * @var array
   */
  public $parents = [];
}
