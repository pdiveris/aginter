<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

class People extends AginterModel
{
  /**
   * Name, e.g. Pier Paolo Pasolini
   *
   * @var string
   */
  public $name = '';

  /**
   * Title (office)
   *
   * @var string
   */
  public $title = '';

  /**
   * Aliases
   *
   * @var string
   */
  public $aliases = '';

  /**
   * Profile L, F, U
   *
   * @var string
   */
  public $profile = '';

  /**
   * Country, e.g. 'IT'
   *
   * @var string
   */
  public $country;

  /**
   * Introduction
   * e.g. "Pier Paolo Pasolini was an Italian film director, poet, writer, and intellectual."
   *
   * @var array
   */
  public $introduction = ['html'=>'', 'markdown'=>'', 'deltas'=>'', 'node'=>null];
  
  /**
   * A list of tags, if any
   *
   * @var array
   */
  public $tags = [];

  /**
   * Documents, images, video etc
   *
   * OBSOLETED
   *
   * @var object
   */
  public $library;
}
