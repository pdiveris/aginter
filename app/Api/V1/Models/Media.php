<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 04/03/2018
 * Time: 12:26
 */

namespace App\Api\V1\Models;

/**
 * Class Media
 * @package App\Api\V1\Models
 */
class Media
{
  /**
   * @var array
   */
  public $images = [];

  /**
   * @var array
   */
  public $videos = [];

  /**
   * @var array
   */
  public $texts = [];

  /**
   * @var array
   */
  public $sources = [];
}
