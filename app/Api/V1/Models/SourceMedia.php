<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 04/07/2018
 * Time: 11:55
 */

namespace App\Api\V1\Models;

/**
 * Class SourceMedia
 * @package App\Api\V1\Models
 */
class SourceMedia
{
  /**
   * Source
   *
   * @var Source
   */
  public $source;

  /**
   * The media object
   *
   * @var Media
   */
  public $media;

  /**
   * Country
   *
   * @var
   */
  public $country;

  /**
   * SourceMedia constructor.
   *
   */
  public function __construct()
  {
    $this->source = new Source();

    $this->media = new Media();

    $this->country = new Country();
  }
}
