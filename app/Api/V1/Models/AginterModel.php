<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:15
 */

namespace App\Api\V1\Models;

use App\Api\ApiClient;
use App\Api\ArangoDBConnection;
use App\Api\Query\Builder;

use App\Api\Query\Processors\ArangoDBProcessor as Processor;
use App\Api\Query\Grammars\ArangoDBGrammar as Grammar;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Expression;

/**
 * Class AginterModel
 * @package App\Api\V1\Models
 */
class AginterModel
{
  /**
   * The key, e.g. '10500807'
   * Key is a string that uniquely identifies a document within the collection
   *
   * @see https://docs.arangodb.com/3.0/Manual/Appendix/Glossary.html#document-key Document Key
   * @var string
   */
  protected $_key = '';
  
  /**
   * Document handle, e.g. 'tags/10500807'
   * Uniquely identifies a document in the database
   * Consists of the the collection's name and the document key (_key attribute) separated by /
   *
   * @see https://docs.arangodb.com/3.0/Manual/Appendix/Glossary.html#document-handle Document Handle
   * @var string
   */
  protected $_id;
  
  /**
   * Revision
   * The MVCC token used to identify a particular revision of a document
   *
   * @see https://docs.arangodb.com/3.0/Manual/Appendix/Glossary.html#document-revision Document Revision
   * @var string
   */
  protected $_rev = '';
  
  /**
   * Fields to build the update request
   *
   * @var array
   */
  private $_fields = [];
  
  /**
   * Do not include those fields in update request body
   *
   * @var array
   */
  protected static $excludedFields = ['_id', '_rev', '_key', '_fields', '_validationRules'];
  
  /**
   * The connection name for the model.
   *
   * @var string
   */
  protected $connection;
  
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table;
  
  /**
   * The primary key for the model.
   *
   * @var string
   */
  protected $primaryKey = '_key';
  
  
  
  /**
   * AginterModel constructor.
   *
   * @param null $data
   */
  public function __construct($data = null)
  {
    if (null == $data) {
      $data = request()->all();
    }
    
    if (empty($data)) { // new model
      foreach (get_object_vars($this) as $property => $value) {
        if (!in_array($property, self::$excludedFields)) {
          $this->_fields[] = $property;
        }
      }
    } else {
      foreach ($data as $property => $value) {
        if (property_exists($this, $property)) {
          if (!in_array($property, self::$excludedFields))
            $this->_fields[] = $property;
      
          $this->$property = $value;
        }
      }
    }
  }
  
  
  /**
   * Return the collection name
   *
   * @return string
   */
  private static function getCollection(): string
  {
    $strings = explode('\\', get_called_class());
  
    $collection = strtolower(str_plural(str_replace('Model', '', $strings[count($strings) - 1])));
    
    return $collection;
  }
  
  /**
   * Update existing record
   *
   * @param $id
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse|static
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function update($id, $collection = '')
  {
    // allow for passing collection in order to force edges. just trialing.
    $collection = ($collection == '') ? self::getCollection() : $collection;
    
    $body = [];
    foreach ($this->_fields as $field) {
      $body[$field] = $this->$field;
    }

    $response = ApiClient::instance()->request('PATCH', "_db/piombo/_api/document/{$collection}/{$id}", $body);
  
    $res = json_decode($response->getBody());
    if (request()->isJson()) {
      return response()->json(
          $res,
          $response->getStatusCode(),
          [
            'X-Status'=>'Update successful',
            'User' => request()->server('user')
          ]
      );
    } else {
      return $this->hydrate($res);
    }
  }
  
  /**
   * Refresh data from operation result
   * You could check old and new value and raise warning here..
   *
   * @param array $data
   * @return static|null
   */
  public function hydrate($data = [])
  {
    foreach ($data as $property => $value) {
      if (property_exists($this, $property)) {
        $this->$property = $value;
      }
    }
    return $this;
  }
  
  /**
   * Crate a new record and save
   s*
   * @param string $collection
   * @return \Illuminate\Http\JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function store($collection = '')
  {
  
    // allow for passing collection in order to force edges. just trialing.
    $collection = ($collection == '') ? self::getCollection() : $collection;
  
    $body = [];
    foreach ($this->_fields as $field) {
      $body[$field] = $this->$field;
    }
    
    $response = ApiClient::instance()->request('POST', "_db/piombo/_api/document/{$collection}", $body);
  
    $res = json_decode($response->getBody());
    if (request()->isJson()) {
      return response()->json(
          $res,
          $response->getStatusCode(),
          [
            'X-Status' => 'Stored successful',
            'User' => request()->server('user')
          ]
      );
    } else {
      return response()-json($this->hydrate($res));
    }
  }
  
  public function destroy($id = '')
  {
  }
  
  public function where()
  {
    ini_set('xdebug.var_display_max_depth',10);
    
    
    $events = new Builder(new ArangoDBConnection(), new Grammar(), new Processor());
    
    $events->where('what', 'like', new Expression('"%ass%"'));
    $events->orWhere('when', 'like', new Expression('"%198%"'));
    $events->orWhere('where', 'like', new Expression('"%It%"'));
  
    $events->from('events', 'e');
    
    $events->limit(30);
    $events->orders('what', 'asc');
    $events->orders('when', 'desc');
    
    $oe = new Builder(new ArangoDBConnection(), new Grammar(), new Processor());
    $oe->columns = ['_to'];
    $oe->from('organisations_events', 'oe');
    $oe->where('_from', 'in', new Expression(''));
    
    $oe->whereNested(function($query) {
        $query->fromAlias = 'o';
        $query->from = 'organisations';
        $query->columns = ['_id'];
       
        $query->where('name', 'like', new Expression("%P%"));
        $query->orWhere('initials', 'like', new Expression("%P%"));
      },
      ''
    );
    
    $events->whereTest('_id', $oe, 'e');
    
    $op = new Builder(new ArangoDBConnection(), new Grammar(), new Processor());
    $op->columns = ['_to'];
    $op->from('people_events', 'pe');
    $op->where('_from', 'in', new Expression(''));
  
    $op->whereNested(function($query) {
        $query->fromAlias = 'o';
        $query->from = 'organisations';
        $query->columns = ['_id'];
        $query->where('name', 'like', new Expression("%P%"));

      },
      ''
    );
    
    $events->whereTest('_id', $op, 'e');
    
    $events->get();
    
    echo '<pre><font color="#cc0000">'.$events->toSql().'</font></pre>';
    
    //var_dump($events->toSql());
    die;
  }
  
  /**
   * Find a model by its primary key.
   *
   * @param  mixed $id
   * @param  array $columns
   * @return static|null
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public static function find($id, $columns = ['*'])
  {
    
    $collection = self::getCollection();

    $model = 'App\\Api\\V1\\Models\\'.ucfirst(str_singular($collection));
    
    $response = ApiClient::instance()
      ->request('GET', "_db/piombo/_api/document/{$collection}/$id");
    
    $body = json_decode($response->getBody());
    $concreteModel = new $model($body);
    
    return $concreteModel;
  }
  
  /**
   * @return Collection
   */
  public static function all(): Collection
  {
  }
  
  /**
   * Save
   *
   * Store/update depending on state of model
   */
  public function save()
  {
    if (null !== $this->_key && $this->_key !== '') {
      return $this->update($this->_key);
    } else {
      return $this->store();
    }
  }
  
  /**
   *
   */
  public function delete($id = '')
  {
  }
  
  // Eloquent style
  
  /**
   * Get the database connection for the model.
   *
   * @return \Illuminate\Database\Connection
   */
  public function getConnection()
  {
    return static::resolveConnection($this->getConnectionName());
  }
  
  /**
   * Get the current connection name for the model.
   *
   * @return string
   */
  public function getConnectionName()
  {
    return $this->connection;
  }
  
  /**
   * Set the connection associated with the model.
   *
   * @param  string  $name
   * @return $this
   */
  public function setConnection($name)
  {
    $this->connection = $name;
    
    return $this;
  }
  
  /**
   * Resolve a connection instance.
   *
   * @param  string|null  $connection
   * @return \Illuminate\Database\Connection
   */
  public static function resolveConnection($connection = null)
  {
    return static::$resolver->connection($connection);
  }
  
  /**
   * Get the connection resolver instance.
   *
   * @return \Illuminate\Database\ConnectionResolverInterface
   */
  public static function getConnectionResolver()
  {
    return static::$resolver;
  }
  
  /**
   * Set the connection resolver instance.
   *
   * @param  \Illuminate\Database\ConnectionResolverInterface  $resolver
   * @return void
   */
  public static function setConnectionResolver(Resolver $resolver)
  {
    static::$resolver = $resolver;
  }
  
  /**
   * Unset the connection resolver for models.
   *
   * @return void
   */
  public static function unsetConnectionResolver()
  {
    static::$resolver = null;
  }
  
  /**
   * Get the table associated with the model.
   *
   * @return string
   */
  public function getTable()
  {
    if (! isset($this->table)) {
      return str_replace(
        '\\', '', Str::snake(Str::plural(class_basename($this)))
      );
    }
    
    return $this->table;
  }
  
  /**
   * Set the table associated with the model.
   *
   * @param  string  $table
   * @return $this
   */
  public function setTable($table)
  {
    $this->table = $table;
    
    return $this;
  }
  
  
  
  
}
