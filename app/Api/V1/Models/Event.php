<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

class Event extends AginterModel
{
  /**
   * When
   *
   * @var string
   */
  public $when = '1948-01-01';
  
  /**
   * What
   *
   * @var string
   */
  public $what;

  /**
   * Who
   *
   * @var string
   */
  public $who;

  /**
   * Country
   *
   * @var string
   */
  public $country = '';

  /**
   * Location
   *
   * @var string
   */
  public $location = '';

  /**
   * Introduction, e.g. "Pier Paolo Pasolini was an Italian film director, poet, writer, and intellectual. "
   *
   * @var array
   */
  public $introduction = ['html'=>'', 'markdown'=>'', 'deltas'=>'', 'node'=>null];

  /**
   * A list of tags, if any
   *
   * @var array
   */
  public $tags = [];

  /**
   * EventDate. Obsoleted
   *
   * @var string
   */
  public $eventDate = '';

  /**
   * Labels. Temporary field
   *
   * @var string
   */
  public $labels = '';

  /**
   * Documents, images, video etc
   * OBSOLETED
   *
   * @var object
   */
  public $library;

  /**
   * Centering on Europe
   *
   * @var string
   */
  public $map_center = "{\"latitude_deg\":\"45.6523748\", \"longitude_deg\": \"9.3011539\", \"zoom\": \"6\"}";
  
  /**
   * Marker position - this can perhaps also be the centre of the map
   * @var string
   */
  public $marker = "{}";
  
  protected static $validationRules = [
          'what' => 'required|max:255',
          'when' => 'required|max:255',
          'country' => 'required',
        ];
}
