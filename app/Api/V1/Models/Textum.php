<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 04/03/2018
 * Time: 12:26
 */

namespace App\Api\V1\Models;

/**
 * Class LinkType
 * @package App\Api\V1\Models
 */
class Textum
{
  /**
   * Head
   *
   * @var string
   */
  public $thread = '';

  /**
   * Blurb
   *
   * @var string
   */
  public $blurb = '';

  /**
   * Class
   *
   * e.g. ["definition", "citation"]
   *
   * @var array
   */
  public $class = [];

  /**
   * Spectrum
   *
   * -100 to +100
   *
   * @var string
   */
  public $spectrum = 0;

  /**
   * Body
   *
   * @var array
   */
  public $body = ['node'=>'', 'text'=>''];
  
  /**
   * Type
   *
   * @var string
   */
  public $type = '';  // ISO, PDF etc
  
  /**
   * Language
   * e.g. eg Shqip, ALbanian.
   *
   * @var string
   */
  public $language = '';

  /**
   * Encoding
   * e.g. eg UTF-8
   *
   * @var string
   */
  public $encoding = '';

  /**
   * Origin
   *
   * @var string
   */
  public $origin = "";

  /**
   * createdAt - time entry was created
   *
   * @var string
   */
  public $createdAt = '';

  /**
   * updateAt - time entry was updated
   *
   * @var string
   */
  public $updateAt = '';
}
