<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 16/10/2018
 * Time: 08:23
 */

namespace App\Api\V1\Models;

class Version extends AginterModel
{
  /**
   * The collection, e.g. events
   *
   * @var string
   */
  public $collection = '';
  
  /**
   * Type, e.g. 'document:node'
   *
   * @var string
   */
  public $type = '';

  /**
   * Revision e.g. _Xlm4n6q--_
   *
   * @var float
   */
  public $revision = '';
  
  /**
   * Content e.g. the node (in JSON)
   *
   * @var array
   */
  public $content ='';
}
