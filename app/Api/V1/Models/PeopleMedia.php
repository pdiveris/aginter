<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

/**
 * Class PeopleMedia
 * @package App\Api\V1\Models
 */
class PeopleMedia
{
  /**
   * The actor
   *
   * @var People
   */
  public $person;

  /**
   * The media object
   *
   * @var Media
   */
  public $media;

  /**
   * Country
   *
   * @var
   */
  public $country;

  /**
   * PeopleMedia constructor.
   */
  public function __construct()
  {
    $this->person = new People();

    $this->media = new Media();

    $this->country = new Country();
  }
}
