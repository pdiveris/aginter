<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 04/03/2018
 * Time: 12:26
 */

namespace App\Api\V1\Models;

/**
 * Class LinkType
 * @package App\Api\V1\Models
 */
class LinkType
{
  /**
   * Kind
   *
   * @var string
   */
  public $kind = '';

  /**
   * Weight
   *
   * @var integer
   */
  public $weight = 0;

  /**
   * Comments
   *
   * @var string
   */
  public $string = '';
}
