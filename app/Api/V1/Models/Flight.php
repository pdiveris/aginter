<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 23/10/2018
   * Time: 20:32
   */
  
  namespace App\Api\V1\Models;

  use App\Api\Articulate\AttributeTypes\AttributeString;
  use App\Api\Articulate\Model;
  use App\Api\Articulate\AttributeTypes\AttributeStringAttributeString;

  /**
   * Class Flight
   *
   * @package App\Api\V1\Models
   *
   * @property string $_key
   * @property string $_id
   * @property string $_rev
   * @property int $Year;
   * @property int $Month;
   * @property int $DayofMonth;
   * @property int $DayOfWeek;
   * @property string $DepTime;
   * @property string $ArrTime;
   * @property string $DepTimeUTC;
   * @property string $ArrTimeUTC;
   * @property string $UniqueCarrier;
   * @property string $FlightNum;
   * @property string $TailNum;
   * @property string $Distance;
   *
   */
  class Flight extends Model
  {
    /**
     * We use collections not tables.
     *
     * @var string
     */
    protected $collection = 'flights';
  
    /**
     * @var array
     */
    protected $fillable = ['_from','_to','Year','Month','DayofMonth','DayOfWeek','DepTime','ArrTime','DepTimeUTC','ArrTimeUTC','UniqueCarrier','FlightNum','TailNum','Distance'];
    
    
  }
  
