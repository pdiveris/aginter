<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 24/02/2018
 * Time: 17:24
 */

namespace App\Api\V1\Models;

/**
 * Class OrganisationMedia
 * @package App\Api\V1\Models
 */
class OrganisationMedia
{
  /**
   * The organisation
   *
   * @var Organisation
   */
  public $organisation;

  /**
   * The media object
   *
   * @var Media
   */
  public $media;

  /**
   * Country
   *
   * @var
   */
  public $country;

  /**
   * Relations
   *
   * @var array
   */
  public $relations = [
    'actors' => [],
  ];

  /**
   * OrganisationMedia constructor.
   *
   */
  public function __construct()
  {
    $this->organisation = new Organisation();

    $this->media = new Media();

    $this->country = new Country();
  }
}
