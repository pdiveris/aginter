<?php

namespace App\Api\Schema;

class ArangoDBBuilder extends Builder
{
    /**
     * Determine if the given table exists.
     *
     * @param  string  $table
     * @return bool
     */
    public function hasTable($table)
    {
        return false;
    }

    /**
     * Drop all tables from the database.
     *
     * @return void
     */
    public function dropAllTables()
    {

    }

    /**
     * Drop all views from the database.
     *
     * @return void
     */
    public function dropAllViews()
    {

    }

    /**
     * Get all of the table names for the database.
     *
     * @return array
     */
    protected function getAllTables()
    {
        return [];
    }

    /**
     * Get all of the view names for the database.
     *
     * @return array
     */
    protected function getAllViews()
    {
        return [];
    }

    /**
     * Get the column listing for a given table.
     *
     * @param  string  $table
     * @return array
     */
    public function getColumnListing($table)
    {
        return [];
    }

    /**
     * Parse the table name and extract the schema and table.
     *
     * @param  string  $table
     * @return array
     */
    protected function parseSchemaAndTable($table)
    {
        return [];
    }
}
