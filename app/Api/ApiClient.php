<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 16/10/2018
   * Time: 12:51
   */
  
  namespace App\Api;
  
  use GuzzleHttp\Client as GuzzleClient;
  use App\Helpers\Utils;
  
  /**
   * Class Client
   *
   * @package App\Api
   */
  class ApiClient
  {
    /**
     * Visible to all subclasses and parent
     *
     * @var ApiClient|null
     */
    protected static $client = null;
  
    /**
     * Placeholder (.env)
     *
     * @var array
     */
    protected static $auth = ['', ''];
  
    /**
     * @var null
     */
    protected static $me = null;
    
    public static $request;

    public function __construct(\Illuminate\Http\Request $request)
    {
      self::$request = $request;
      
      $proto = env('AR_PROTO', 'https');
  
      $host = env('AR_HOST','');
  
      self::$auth = [env('AR_USERNAME',''), env('AR_PASSWORD', '')];
  
      if (null == self::$client) {
        self::$client = new GuzzleClient(['base_uri' => "$proto://$host"]);
      }
    }
  
    /**
     * Get an instance of the client
     *
     * @return ApiClient
     */
    public static function instance() {
      if (null == self::$me)
      {
        return new ApiClient(request());
      }
    }
  
    /**
     * Perform a request and return response
     *
     * @param string $verb
     * @param $uri
     * @param string $body
     * @param null $auth
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($verb = 'GET', $uri, $body = '', $auth = null)
    {
      if (null == $auth)
      {
        $auth = self::$auth;
      }
      
      $response = self::$client->request($verb, $uri, [ 'auth' => $auth,  'body' => Utils::jsonEncode($body) ]);

      return $response;
    }
    
  }
