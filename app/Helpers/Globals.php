<?php
/**
 * Created by PhpStorm.
 * User: "Petros Diveris"
 * Date: 15/02/2018
 * Time: 12:50
 */

namespace App\Helpers;

/**
 * Class Globals
 * @package App\Helpers
 */
class Globals
{
  public static function prefix(): string
  {
    if (isset($_SERVER['AGINTER_PREFIX'])) {
      return '/'.$_SERVER['AGINTER_PREFIX'];
    }
    return '';

  }
}