<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 16/02/2018
 * Time: 17:54
 */

namespace App\Helpers;

/**
 * Class Utils
 * @package App\Helpers
 */
class Utils
{
  /**
   * Produce a unique file name for a folder and a name
   * That is, return the fileName passed if no other file exists with the same name,
   * or return a fileName with enough uniqueness appended to it..
   *
   * * do: write tests
   *
   * @param string $folder
   * @param string $fileName
   * @return string
   */
  public static function uniqueFileName($folder = '', $fileName = '' ): string
  {
    
    
    if ($fileName == '') {
      // throw exception
    }
    
    $bits = explode('.', $fileName);
    $name = $bits[0];
    $ext = '';
    
    if (count($bits)>1) {
      $ext = $bits[1];
    }
    
    while (file_exists($folder.'/'.$fileName)) {
      /*
      /Users/pedro/test/
      test.jpg
      */
      $fileName = $name . '-';
      $fileName .= self::randomString(4);
      $fileName .= '.';
      $fileName .= $ext;
    }
    
    return $fileName;
  }
  
  /**
   * Return a pseudorandom sequence of characters and digits of length $len
   *
   * do: write test
   *
   * @param int $len
   * @return string
   */
  public static  function randomString($len = 5): string
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    
    $result = '';
    
    for ($i = 0; $i < $len; $i++)
      $result .= $characters[mt_rand(0, 35)];
    
    return $result;
  }
  
  /**
   * Wrapper for file_put_conetnts
   * Makes sure that paths get created if they don't exist
   *
   * @param string $dir
   * @param string $contents
   * @return bool|int
   */
  public static function filePutContents($dir, $contents) {
    $parts = explode('/', $dir);
    $file = array_pop($parts);
    $dir = '';
    foreach($parts as $part)
      if(!is_dir($dir .= "/$part")) mkdir($dir);
    
    return @file_put_contents("$dir/$file", $contents);
  }

  /**
   * Remove greek accents
   *
   * @param string $str
   * @return string
   */
  public static function aTonikon($str = '') {
    $ret = $str;

    $ret = str_replace("ά", "α", $ret);
    $ret = str_replace("έ", "ε", $ret);
    $ret = str_replace("ή", "η", $ret);
    $ret = str_replace("ί", "ι", $ret);
    $ret = str_replace("ύ", "υ", $ret);
    $ret = str_replace("ό", "ο", $ret);
    $ret = str_replace("ώ", "ω", $ret);

    $ret = str_replace("Ά", "Α", $ret);
    $ret = str_replace("Έ", "Ε", $ret);
    $ret = str_replace("Ή", "Η", $ret);
    $ret = str_replace("Ί", "Ι", $ret);
    $ret = str_replace("Ύ", "Υ", $ret);
    $ret = str_replace("Ό", "Ο", $ret);
    $ret = str_replace("Ώ", "Ω", $ret);

    return $ret;
  }
  
  /**
   * @param object|array $data
   * @return false|string
   */
  public static function jsonEncode($data) {
    return json_encode($data);
  }

}
