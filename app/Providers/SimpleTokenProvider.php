<?php
/**
 * Created by Petros Diveris.
 * Created with PhpStorm.
 *
 * Date: 18/03/2018
 * Time: 12:38
 */

namespace App\Providers;

use App\User;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Route;
use Dingo\Api\Auth\Provider\Authorization;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Support\Str;

/**
 * Class SimpleTokenProvider
 *
 * Authenticate a user based on whether the supplied token exists in the DB
 * and links to a valid User object
 *
 * @package App\Providers
 */
class SimpleTokenProvider extends  Authorization
{
  /**
   * Autenticate
   *
   * Implement Dingo API's authenticate as per docs
   * re: custom authentication
   *
   * @param Request $request
   * @param Route $route
   * @return mixed
   */
  public function authenticate(Request $request, Route $route)
  {
    // Logic to authenticate the request.

    $header = $request->header('Authorization', '');

    if ($header =='') {
      throw new BadRequestHttpException('Request must include AUTH');
    }

    if (Str::startsWith($header, 'Bearer ')) {
      $token =  Str::substr($header, 7);

      $user = User::where('api_token', $token)
        ->first();

      if ($user) {
        $request->server->add(['user'=>$user]);
        return $user;
      }

    }

    throw new UnauthorizedHttpException('Unable to authenticate with supplied username and password.',
      'Unable to authenticate with supplied username and password.');
  }

  public function getAuthorizationMethod()
  {
    return 'mac';
  }

  public function boot() {

  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton('simple', function ($app) {
      return new SimpleTokenProvider($app);
    });


  }

}
