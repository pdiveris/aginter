<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableAddBob extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      User::create([
        'name' => "Bob Myers",
        'email' => 'bob@diveris.org',
        'password' => bcrypt('kasbah!!'),
        'api_token' => str_random(60)
      ]);
      
    }
}
