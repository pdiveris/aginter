var elixir = require('laravel-elixir');

// require('laravel-elixir-rollup');

elixir(function(mix) {
  mix.rollup(
    './resources/assets/js/app.js',
    './public/dist'
  );
});
